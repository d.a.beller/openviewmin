#!/usr/bin/env python3

""" Executable "front-end" file """

import sys
import glob
import open_viewmin as ovm
from open_viewmin.filter_tree_plot.utilities.ovm_utilities import sort_filenames_by_timestamp

# TODO save, load state
# TODO Separate documentation for FilterTreePlot, ViewMinPlot, and NematicPlot
# TODO Qtensor ellipsoids?

plotter_class = ovm.NematicPlot

if __name__ == '__main__':
    filenames = []
    kwargs = {}
    if len(sys.argv) > 1:
        skip_next = False
        for argi in range(1, len(sys.argv)):
            if skip_next:
                skip_next = False
                continue
            else:
                arg = sys.argv[argi]
                if arg[:2] == '--':
                    kwargs[arg[2:]] = eval(sys.argv[argi + 1])
                    skip_next = True
                else:
                    filenames += glob.glob(arg)
    if len(filenames) > 0:
        filenames = sort_filenames_by_timestamp(filenames)
        print('Found these files (and importing in this order):')
        for filename in filenames:
            print(filename)
        my_viewmin_plot = plotter_class(filenames, **kwargs)
    else:
        my_viewmin_plot = plotter_class(**kwargs)
    input("\nPress Enter in this window to exit or return control to interpreter.\n")
