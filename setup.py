from setuptools import find_packages, setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='open_viewmin-dbeller',
    version="0.1.0",
    author="Daniel Beller",
    author_email="d.a.beller@jhu.edu",
    description="A PyVista extension for 3D nematic liquid crystals visualization",
    long_description=long_description,
    url="https://gitlab.com/d.a.beller/openviewmin/",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    python_requires=">=3.7, <3.10",
    install_requires=[
        'pandas',
        'PyQt5',
        'pyvistaqt',
        'tqdm',
        'imageio-ffmpeg',
        'meshio',
        'scipy',
        'ipython',
        'ipywidgets',
        'pythreejs',
        'jupyterlab'
    ]
)
