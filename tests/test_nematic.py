import unittest
from open_viewmin import NematicPlot

class TestNematic(unittest.TestCase):

    def setUp(self):
        self.plot = NematicPlot('../open-Qmin/data/many_colloids_demo_t3000_x0y0z0.txt')

    def test_plot_type(self):
        self.assertIsInstance(self.plot, NematicPlot)


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestNematic('test_plot_type'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(test_suite())