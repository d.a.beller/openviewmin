:py:mod:`open_viewmin.nematic`
==============================

.. py:module:: open_viewmin.nematic


Submodules
----------
.. toctree::
   :titlesonly:
   :maxdepth: 1

   jones/index.rst
   nematic/index.rst


