:py:mod:`open_viewmin.filter_tree_plot.utilities.mpi_stitch`
============================================================

.. py:module:: open_viewmin.filter_tree_plot.utilities.mpi_stitch

.. autoapi-nested-parse::

   Utility to merge data from separate files for a single scene, such as from
   an MPI run



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.utilities.mpi_stitch.mpi_group
   open_viewmin.filter_tree_plot.utilities.mpi_stitch.mpi_stitch



.. py:function:: mpi_group(filenames)

   Check whether some filenames come from the same MPI run
   (formatted as same_prefix_x#y#z#.txt) and if so, stitch them together
   using mpi_stitch()



.. py:function:: mpi_stitch(in_filenames, out_filename=None)

   Stitches together a set of data text files from an MPI run into a single
   file with the proper ordering of lines for use by openViewMin. First three
   columns must be x y z coords.



