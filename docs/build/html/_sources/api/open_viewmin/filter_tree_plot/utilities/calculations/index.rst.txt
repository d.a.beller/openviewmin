:py:mod:`open_viewmin.filter_tree_plot.utilities.calculations`
==============================================================

.. py:module:: open_viewmin.filter_tree_plot.utilities.calculations

.. autoapi-nested-parse::

   General calculation utilities.



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.utilities.calculations.calculate_from_scalar_field
   open_viewmin.filter_tree_plot.utilities.calculations.calculate_from_tensor_field
   open_viewmin.filter_tree_plot.utilities.calculations.calculate_from_vector_field
   open_viewmin.filter_tree_plot.utilities.calculations.di
   open_viewmin.filter_tree_plot.utilities.calculations.dij
   open_viewmin.filter_tree_plot.utilities.calculations.einstein_sum
   open_viewmin.filter_tree_plot.utilities.calculations.list_to_xyzmat
   open_viewmin.filter_tree_plot.utilities.calculations.matrix_times_transpose
   open_viewmin.filter_tree_plot.utilities.calculations.safe_inverse
   open_viewmin.filter_tree_plot.utilities.calculations.transpose_times_matrix
   open_viewmin.filter_tree_plot.utilities.calculations.xyzmat_to_list



Attributes
~~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.utilities.calculations.levi_civita


.. py:data:: levi_civita
   

   

.. py:function:: calculate_from_scalar_field(plotter, scalar_field_name, operation_string, mesh_name='fullmesh')


.. py:function:: calculate_from_tensor_field(plotter, tensor_field_name, operation_string, mesh_name='fullmesh')


.. py:function:: calculate_from_vector_field(plotter, vector_field_name, operation_string, mesh_name='fullmesh')


.. py:function:: di(arr, array_dims, flatten=False, component=None, data_stride=1)

   Gradient


.. py:function:: dij(arr, array_dims, flatten=False, data_stride=1)

   Hessian


.. py:function:: einstein_sum(sum_str, *arrays, reshape=True)

   Increase flexibility of numpy.einsum, for convenience in calculating
   datasets



.. py:function:: list_to_xyzmat(arr, dims)


.. py:function:: matrix_times_transpose(arr)


.. py:function:: safe_inverse(arr)

   take inverse x->1/x but with 0->0


.. py:function:: transpose_times_matrix(arr)


.. py:function:: xyzmat_to_list(arr)


