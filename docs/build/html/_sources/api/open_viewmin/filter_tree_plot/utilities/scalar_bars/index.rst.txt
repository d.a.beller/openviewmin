:py:mod:`open_viewmin.filter_tree_plot.utilities.scalar_bars`
=============================================================

.. py:module:: open_viewmin.filter_tree_plot.utilities.scalar_bars

.. autoapi-nested-parse::

   Utilities for customizing and auto-updating scalar bars (colorbars)



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.utilities.scalar_bars.copy_scalar_bar_properties
   open_viewmin.filter_tree_plot.utilities.scalar_bars.move_scalar_bar_to_previous_position
   open_viewmin.filter_tree_plot.utilities.scalar_bars.rename_scalar_bar_actor_to_title
   open_viewmin.filter_tree_plot.utilities.scalar_bars.standardize_scalar_bar
   open_viewmin.filter_tree_plot.utilities.scalar_bars.update_scalar_bar



.. py:function:: copy_scalar_bar_properties(old_scalar_bar, scalar_bar_args)


.. py:function:: move_scalar_bar_to_previous_position(plotter, this_scalar_bar, scalar_bar_args)


.. py:function:: rename_scalar_bar_actor_to_title(plotter, scalar_bar_actor)


.. py:function:: standardize_scalar_bar(plotter, scalar_bar)

   Workaround for bugs in PyVista's display of scalar bars


.. py:function:: update_scalar_bar(plotter, actor_name, this_scalar_bar)


