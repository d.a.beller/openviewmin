:py:mod:`open_viewmin.filter_tree_plot.filters.isosurfaces`
===========================================================

.. py:module:: open_viewmin.filter_tree_plot.filters.isosurfaces

.. autoapi-nested-parse::

   Utilities for creating single-value contour surfaces filtered from a 3D mesh



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.filters.isosurfaces.calculate_PT_surface
   open_viewmin.filter_tree_plot.filters.isosurfaces.make_smooth_contour_func
   open_viewmin.filter_tree_plot.filters.isosurfaces.update_isosurface



.. py:function:: calculate_PT_surface(mesh, vectors_name, normal_dir=None, theta_key=None, phi_key=None)

   Calculate angles for Pontryagin-Thom construction


   For vector array named `vectors_name` defined on `mesh`, calculates angle
   data for the vectors relative to a reference direction `normal_dir`, for
   use in making a Pontryagin-Thom surface.

   :param mesh: PyVista mesh containing the orientation field vectors.
   :type mesh: pyvista.DataSet
   :param vectors_name: Name of vector array in `mesh`.
   :type vectors_name: str
   :param normal_dir: Normal direction in orientation-space.
   :type normal_dir: [int, int, int], optional
   :param theta_key: Name to give calculated array of polar angles between the orientation
                     field and `normal_dir`.
   :type theta_key: str, optional
   :param phi_key: Name to give calculated array of azimuthal angles of the orientation
                   field about the `normal_dir` axis, with an arbitrary zero.
   :type phi_key: str, optional

   :returns: Keys for scalar arrays containing the calculated polar and azimuthal
             angles, respectively, of the orientation field relative to the given
             normal direction.
   :rtype: str, str


.. py:function:: make_smooth_contour_func(mesh, n_iter)

   Use PyVista's smoothing function for contour surfaces


.. py:function:: update_isosurface(plotter, mesh_name, dataset_name=None, contour_values=None, **contour_kwargs)


