:py:mod:`open_viewmin.filter_tree_plot.widgets.plane_widgets`
=============================================================

.. py:module:: open_viewmin.filter_tree_plot.widgets.plane_widgets

.. autoapi-nested-parse::

   Utilities for working with PyVista plane widgets



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot.widgets.plane_widgets.add_PT_widget
   open_viewmin.filter_tree_plot.widgets.plane_widgets.add_filter_widget_clip_plane
   open_viewmin.filter_tree_plot.widgets.plane_widgets.add_slice
   open_viewmin.filter_tree_plot.widgets.plane_widgets.add_slice_aux
   open_viewmin.filter_tree_plot.widgets.plane_widgets.add_slice_widget_to_slice_mesh
   open_viewmin.filter_tree_plot.widgets.plane_widgets.alter_plane_widget
   open_viewmin.filter_tree_plot.widgets.plane_widgets.configure_plane_widget
   open_viewmin.filter_tree_plot.widgets.plane_widgets.default_slice_kwargs
   open_viewmin.filter_tree_plot.widgets.plane_widgets.new_slice_mesh
   open_viewmin.filter_tree_plot.widgets.plane_widgets.setup_slice_widget
   open_viewmin.filter_tree_plot.widgets.plane_widgets.slice_widget_callback



.. py:function:: add_PT_widget(plotter, vectors_name, apolar=True, name=None)

   Add a Pontryagin-Thom surface.

   Finds the surface(s) of points where the orientation lies in the plane
   of directions
   perpendicular to `normal_dir`, and colors the surface by angle in this
   plane. For uses of the Pontryagin-Thom construction in nematic liquid
   crystals, see:
   Chen, Ackerman, Alexander, Kamien, and Smalyukh (2013),
   Generating the Hopf fibration experimentally in nematic liquid crystals.
   Phys. Rev. Lett. 110, 237801.
   https://doi.org/10.1103/PhysRevLett.110.237801

   Čopar, Porenta and Žumer (2013),
   Visualisation methods for complex nematic fields,
   Liquid Crystals, 40:12, 1759-1768.
   https://doi.org/110.1080/02678292.2013.853109.

   Machon and Alexander (2014),
   Knotted Defects in Nematic Liquid Crystals,
   Phys. Rev. Lett. 113, 027801.
   https://doi.org/110.1103/PhysRevLett.113.027801



.. py:function:: add_filter_widget_clip_plane(plotter, mesh_name)


.. py:function:: add_slice(plotter, scalars_name=None, slice_name=None, widget_name=None, mesh_to_slice_name='fullmesh')


.. py:function:: add_slice_aux(plotter, scalars_name=None)


.. py:function:: add_slice_widget_to_slice_mesh(plotter, slice_name=None, widget_name=None)


.. py:function:: alter_plane_widget(widget_formula, theta=None, phi=None, origin=None, dtheta=0, dphi=0, d_origin=(0, 0, 0))

   Change normal and/or origin of a plane widget,
   and update associated mesh (and possibly actor(s)) accordingly


.. py:function:: configure_plane_widget(plotter)


.. py:function:: default_slice_kwargs(plotter)


.. py:function:: new_slice_mesh(plotter, slice_name=None, mesh_to_slice_name='fullmesh', normal=(1.0, 0.0, 0.0), origin=None, **mesh_kwargs)


.. py:function:: setup_slice_widget(plotter, widget_name=None, slice_name=None, mesh_to_slice_name='fullmesh', **mesh_kwargs)


.. py:function:: slice_widget_callback(plotter, slice_name)


