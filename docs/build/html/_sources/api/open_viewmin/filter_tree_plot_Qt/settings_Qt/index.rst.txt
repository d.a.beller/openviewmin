:py:mod:`open_viewmin.filter_tree_plot_Qt.settings_Qt`
======================================================

.. py:module:: open_viewmin.filter_tree_plot_Qt.settings_Qt

.. autoapi-nested-parse::

   Additional default values specific to the Qt GUI



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot_Qt.settings_Qt.actor_label_style
   open_viewmin.filter_tree_plot_Qt.settings_Qt.mesh_label_style



Attributes
~~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot_Qt.settings_Qt.settings_Qt_dict


.. py:data:: settings_Qt_dict
   

   

.. py:function:: actor_label_style(actor_name)


.. py:function:: mesh_label_style(mesh_name)


