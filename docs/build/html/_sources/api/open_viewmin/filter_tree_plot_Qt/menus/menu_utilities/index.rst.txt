:py:mod:`open_viewmin.filter_tree_plot_Qt.menus.menu_utilities`
===============================================================

.. py:module:: open_viewmin.filter_tree_plot_Qt.menus.menu_utilities

.. autoapi-nested-parse::

   Utilities for using and editing Qt menus



Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   open_viewmin.filter_tree_plot_Qt.menus.menu_utilities.get_menu
   open_viewmin.filter_tree_plot_Qt.menus.menu_utilities.get_menu_action
   open_viewmin.filter_tree_plot_Qt.menus.menu_utilities.get_menus
   open_viewmin.filter_tree_plot_Qt.menus.menu_utilities.remove_menu_action



.. py:function:: get_menu(parent, title)


.. py:function:: get_menu_action(parent, label)


.. py:function:: get_menus(plotter)


.. py:function:: remove_menu_action(parent, label)


