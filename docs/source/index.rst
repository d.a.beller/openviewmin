open-ViewMin documentation
========================================

.. root: Overview <readme>

.. toctree::
   :maxdepth: 5

   Overview <readme>
   Filters tree <filters-tree>
   Generalized usage <generalized-usage>
   Calculations <calculations>
   GUI Reference <GUI-reference>
   License <LICENSE>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
