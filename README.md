# open-ViewMin

Nematic liquid crystal tensorial data visualization environment

*Human usin' openQmin, you can zoom in!*

 <!-- <iframe src="../../../examples/results/html/many_colloids_demo_t3000.html" title="Demo scene" width="700" height="500" style="border:none;" ></iframe> -->

## Description

*open-ViewMin* is a visualization environment for three-dimensional orientation field datasets. It is primarily designed as a visualization companion to the nematic liquid crystal modeling package [*open-Qmin*](https://github.com/sussmanLab/open-Qmin). 


The goal of *open-ViewMin* is to provide an environment similar to
[*ParaView*](https://www.paraview.org/) where 3D visualization is fast (ideally, GPU-accelerated) and reasonably good-looking, but that also

  1. has a Python interface with *NumPy* compatibility so that users can visualize calculation results on the fly, and

  2. comes pre-loaded with many commonly-used analysis methods for nematic LCs,
  such as defect identification and Landau-de Gennes or Frank free energy components.

To do this, *open-ViewMin* extends [*PyVistaQt*](https://github.com/pyvista/pyvistaqt), a Python package that uses the *Qt* application-building system as the backend for [*PyVista*](https://docs.pyvista.org/), which in turn is a Python interface to the [*Visualization Toolkit (VTK)*](https://vtk.org/). 

In addition to details specific to nematic liquid crystals, *open-ViewMin* extends *PyVista* with `FilterFormula` objects, which apply *PyVista* filters and automatically update them when a parent mesh has changed.


## Requirements

You'll need a Python 3.7, 3.8, or 3.9 installation. Python 3.9 is recommended as it is used for development of *open-ViewMin*. This package will **not** work with Python 3.10. Earlier versions than 3.7 haven't been tested. 

## Installation

1. Download the source code using

   ```
   git clone https://gitlab.com/d.a.beller/openviewmin/
   cd openviewmin
   ```

2. It is recommended to create a virtual environment.

   ```
   python3 -m venv ovm-env  # replace "python3" with path to python executable
   source ovm-env/bin/activate
   pip install --upgrade pip
   ```

3. From the *openviewmin/* directory, install the package as:

   ```
   pip install .
   ```

4. Finally, to use *open_viewmin* in a Jupyter notebook, make the *ovm-env* virtual environment available as a notebook kernel:

   ```
   python3 -m ipykernel install --user --name ovm-env
   hash -r  # so "ipython" command uses this Python version in future
   ```

## Basic usage

A user can open the GUI directly from the command line as `./viewmin.py`.

However, more control is gained by combining the GUI and Python interfaces, so it is recommended to run the GUI in the background of a Python interpreter or Jupyter notebook.

```
import open_viewmin as ovm  # this might take a few minutes the first time it runs 
my_plot = ovm.NematicPlot(dims=(10, 10, 10))  # empty 10x10x10 grid

# or

my_plot = ovm.NematicPlot('my_file_x0y0z0.txt')  # open-Qmin data
``` 

Treat `my_plot` like a dictionary in terms of accessing its meshes. To create a new mesh with a `PyVista` filter, use `add_filter`:

```
my_plot["sliced"] = my_plot["fullmesh"].add_filter("slice", normal=(0,0,1))
```

To change an existing mesh, alter the filter's parameters using `update()`:
```
my_plot["sliced"].update(normal=(1,0,0))
```

Change visualization options for the mesh using `set()`:
```
my_plot["sliced"].set(color="green")
```



### Terminology


* A `mesh` is the set of coordinates along with data defined on them.
* A *PyVista* `filter` maps a parent `mesh` to a child `mesh`.
* An `actor` is an object visualizing a mesh in the plot, and is created by calling `pyvista.Plotter.add_mesh()` with the `mesh` as the first argument.
* When a `mesh` is visualized as an `actor`, we give the same name to the `mesh` and the `actor`.
* If the `mesh` will have no child `mesh` of its own (e.g. for the `glyph` filter), then we will often speak of the `filter` as mapping the parent `mesh` directly to the child mesh's `actor`.

### Importing data

You can import files through the *File -> Open files(s)...* menu option within the GUI, or by providing the file paths as arguments when calling *open-ViewMin* from the command line:

    ./viewmin.py file1
    # or
    ./viewmin.py file1 file2 file3...

or from a Python interpreter or Jupyter notebook:

    import open_viewmin as ovm
    my_plot = ovm.NematicPlot(file1)
    # or
    my_plot = ovm.NematicPlot([file1, file2, file3, ...])

Files from the same MPI run are assumed to be labeled as "*run\_name\_x\#y\#z\#.txt*" (with the *#*s replaced by some integers). All such files from the same run will be combined into a single file "*run\_name.txt*".

For importing files from legacy-*Qmin*/*nematicvXX*, you need to provide the "Qtensor... .dat" file or files, and make sure the corresponding "Qmatrix... .dat" file is in the same folder.


### Usage notes for the `NematicPlot` GUI

Once you've imported at least one file, *open-ViewMin* automatically creates:

1. a view of the director field along a slice "widget" that you can move using the mouse,
2. surface(s) for the boundary(ies),
3. isosurface of (uniaxial) nematic order to visualize defects.

Controls for these visualization elements ("actors") will be on the left. For each actor, these controls include a visibility toggle checkbox and a menu of customization options.

You can add other visualizations of computed fields from the "Add" menu.  

If you import more than one file, *open-ViewMin* will attempt to put them in order of timestamp inferred from the filenames. You can then move through the sequence of timesteps using the buttons at the top of the left toolbar area.

### Non-GUI usage

If you prefer that the GUI not open, such as if you're generating snapshots from batch runs, you can pass `off_screen=True` to `NematicPlot`:
```
my_plot = ovm.NematicPlot(file1, off_screen=True)
```
Then, other options to visualize the results include:
* save a screenshot: `my_plot.screenshot('my_image.png')`
* render interactively in Jupyter notebook using *pythreejs* backend: `my_plot.to_pythreejs()`
* render interactively in Jupyter notebook using another backend, such as *panel": `my_plot.extract_actors(jupyter_backend='panel').show()`


### Computing other field arrays

You can use Python+*NumPy* to compute or import field arrays other than the ones provided. This is best done in a Python interpreter or Jupyter notebook, after calling `my_plot = open_viewmin.NematicPlot()` from there. The plotter runs in the background, so you can execute Python commands without closing the plot.

You can view a list of the existing arrays using

    my_plot['fullmesh'].array_names

You can add an array to a mesh by treating it like a Python dictionary and assigning a "key" to a *NumPy* array. This can be derived from existing arrays, e.g.

```
mesh = my_plot['fullmesh']
mesh["splay_plus_twist"] = mesh["LdG_K1"] + mesh["LdG_K2"]
```

or created by any other means, as long as the length of the array's first axis equals the number of points in the mesh, e.g.

    import numpy as np
    my_plot['fullmesh']["my_silly_array"] = (
      np.random.random(my_plot['fullmesh'].mesh.n_points)
    )

If the new array is a "scalar field" i.e. it has one value for each point in the mesh, then it will automatically appear in the appropriate submenus in the "Add" menu.

Important: In order for the new array to be inherited by the other meshes descended from "fullmesh", you should press the Refresh button (circle of two blue arrows) in the top menu area.



## License

*open-ViewMin* is released open-source under the [MIT license](LICENSE). However, *open-ViewMin* depends on *PyVistaQt*, which depends on Qt bindings to Python that have their own licenses. See https://qtdocs.pyvista.org/ under "License".

## Roadmap

### TODO:

* hedgehog charge of loop defects

## Authors
Created by Daniel Beller, 2021-2022.

Jones matrix calculations written by Sophie Ettinger, Yvonne Zagzag, and Daniel Beller.
