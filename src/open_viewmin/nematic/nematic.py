""" Create plotter classes specific to nematic liquid crystals visualization
"""

import numpy as np
import qtpy.QtWidgets as qw
from . import jones
from ..filter_tree_plot.filter_tree_plot import FilterTreePlot
from ..filter_tree_plot_Qt.filter_tree_plot_Qt import FilterTreePlotQt
from ..filter_tree_plot.widgets import plane_widgets
from ..filter_tree_plot.utilities import calculations, ovm_utilities
from ..filter_tree_plot.utilities.calculations import levi_civita, safe_inverse
from ..filter_tree_plot.filters import isosurfaces, glyphs


class NematicPlotNoQt(FilterTreePlot):
    """ Plotter class specific to nematic liquid crystals visualization """

    def __init__(
        self, filenames=None, q0=0, user_settings=None,
        data_format="open-Qmin", sep="\t", reader=ovm_utilities.fast_import,
        theme=None, do_auto_setup=True, **kwargs
    ):
        self.pre_init(
            q0, data_format, do_auto_setup, reader, sep
        )
        super().__init__(
            filenames, user_settings, theme, reader=reader, **kwargs
        )
        self.post_init()

    def pre_init(self, q0, data_format, do_auto_setup, reader, sep):
        self.q0 = q0
        self.do_auto_setup = do_auto_setup
        if reader is None:
            if data_format == "open-Qmin":
                reader = ovm_utilities.fast_import
            elif data_format == "Qmin":
                reader = self.legacy_Qmin_import
            elif data_format == "director":
                def reader_callback(*args, **kwargs):
                    return self.director_data_import(*args, sep=sep, **kwargs)
                reader = reader_callback
        return reader

    def post_init(self):
        self.settings = {
            **self.settings,
            **dict(
                director_color=self.settings["actor_color"],
                boundaries_color=self.oVM_theme["darkblue"],
                scale_mesh_name="nematic_sites",
                defects_S=0.3,
                Jones_arrows_scale=5,
                Jones_arrows_placement_dist=6,
                Jones_p_arrows_color="white",
                Jones_a_arrows_color="yellow",
                Jones_arrows_font_size=16,
                Jones_arrows_label_separation=1
            )
        }

        if len(self.data) > 0 and self.do_auto_setup:
            self.auto_setup()

    def legacy_Qmin_import(self, Qtensor_filenames):
        if type(Qtensor_filenames) is str:
            Qtensor_filenames = [Qtensor_filenames]
        Qtensor_filenames = [
            filename for filename in Qtensor_filenames
            if "Qtensor" in filename
        ]
        for qfi, Qtensor_filename in enumerate(Qtensor_filenames):
            print(
                f"Beginning data import. If the program hangs here, "
                f"try quitting the Python/PyVista viewer application.",
                end="\r"
            )
            print(
                f"Loading {Qtensor_filename} "
                f"({qfi+1} of {len(Qtensor_filenames)})                       "
                f"                                                     ",
                end="\r"
            )
            qtensor = ovm_utilities.fast_import(Qtensor_filename, sep=" ")
            qmatrix = ovm_utilities.fast_import(
                Qtensor_filename.replace('Qtensor', 'Qmatrix'),
                sep=" "
            )
            # calculate nematic order here, not using file, because that info isn't
            # saved for animations
            S = np.linalg.eigvalsh(self.Q33_from_Q5(qmatrix))[:, -1]

            site_types = qtensor[:, -1]
            # infer Lx, Ly, Lz from filename
            self.Lx, self.Ly, self.Lz = np.asarray(
                (
                    ' '.join(
                        Qtensor_filename.split('/')[-1].split(
                            '.dat'
                        )[0].split('x')
                    )
                ).split('_')[1].split(),
                dtype=int
            )
            self.data_stride = int(
                ((self.Lx * self.Ly * self.Lz) / len(qmatrix))**(1/3)
            )
            Z, Y, X = np.meshgrid(
                range(0, self.Lz, self.data_stride),
                range(0, self.Ly, self.data_stride),
                range(0, self.Lx, self.data_stride)
            )
            open_Qmin_style_data = np.concatenate((
                np.array([X.flatten(), Y.flatten(), Z.flatten()]).T,
                qmatrix,
                np.array((site_types, S)).T
            ), axis=-1)

            return open_Qmin_style_data

    # default visualizations

    def setup_defects(self):
        """ Isosurface (with single value) of uniaxial order to visualize defects """
        max_S_value = np.max(self.fullmesh["order_uniaxial"])
        min_S_value = np.min(self.fullmesh["order_uniaxial"])
        # plot defects, unless range of S is very small

        if (max_S_value - min_S_value) / max_S_value > 1e-4:
            self.add_filter_formula(
                "defects",
                filter_callable="contour",
                mesh_kwargs=self.settings["isosurface_kwargs"].copy(),
                scalars="order_uniaxial"
            )

    def setup_boundaries(self, separate_meshes=False):
        """ Isosurface for all boundaries collectively, as well as an
        individual isosurface (invisible by default)
        for each boundary """
        boundary_vis_kwargs = dict()
        ovm_utilities.copy_from_dict_recursively(
            self.settings['isosurface_kwargs'],
            boundary_vis_kwargs
        )
        boundary_vis_kwargs["color"] = self.settings["boundaries_color"]

        if separate_meshes:
            for i in range(1, self.num_boundaries+1):
                actor_name = f"boundary_{i}"
                isosurfaces.update_isosurface(
                    self, actor_name, dataset_name=actor_name,
                    contour_values=[0.5], **boundary_vis_kwargs
                )
        else:
            actor_name = "boundaries"
            try:
                isosurfaces.update_isosurface(
                    self,
                    actor_name, dataset_name="nematic_sites",
                    contour_values=[0.5], **boundary_vis_kwargs
                )
            except (KeyError, ValueError):
                pass

    def setup_director(self):
        """ Glyph representation of director on a plane controlled by a plane widget """
        slice_name = "director_plane"
        widget_name = slice_name + "_widget"
        plane_widgets.setup_slice_widget(
            self,
            widget_name=widget_name,
            slice_name=slice_name,
            mesh_to_slice_name="fullmesh"
        )
        self[slice_name].set(opacity=0.1, color=self.settings['slice_color'])
        glyph_viz_kwargs = dict()
        ovm_utilities.copy_from_dict_recursively(
            self.settings["default_mesh_kwargs"],
            glyph_viz_kwargs
        )
        glyphs_name = "director"
        glyphs.add_glyphs_to_mesh(
            self,
            glyphs_name,
            mesh_name=slice_name,
            glyph_stride=2,
            orient="director",
            scale="nematic_sites",
            **glyph_viz_kwargs
        )
        return slice_name, glyphs_name, widget_name

    def set_director(self, director_data, mesh_name=None):
        mesh = self.get_mesh(mesh_name)
        mesh["director"] = director_data

    def set_Q(self, Q_data, mesh_name=None):
        mesh = self.get_mesh(mesh_name)
        self.Q33 = self.Q33_from_Q5(Q_data)  # 3x3 Q matrix
        mesh["Q"] = self.Q33.reshape(-1, 9)
        mesh["director"] = self.n_from_Q(self.Q33)

    def create_fullmesh(self, dat=None, mesh_name="fullmesh"):
        """ Calculation of nematic dataset information """
        root_mesh_name = super().create_fullmesh(dat, mesh_name)
        if dat is None:
            return

        self.fullmesh["site_types"] = dat[:, 8]
        site_types = self.fullmesh["site_types"]

        Q_data = dat[:, 3:8]

        # Don't bother calculating eigenvectors inside objects
        Q_data[site_types > 0] = 0.

        self.set_Q(Q_data, mesh_name=root_mesh_name)

        # boundaries:

        self.fullmesh["nematic_sites"] = 1 * (site_types <= 0)
        nsites = self.fullmesh["nematic_sites"]
        nsites3 = np.stack((nsites,)*3, axis=-1)
        self.num_boundaries = int(np.max(site_types))
        self.Qij = calculations.list_to_xyzmat(self.Q33, self.array_dims)
        self.diQjk = calculations.di(
            self.Qij, self.array_dims, data_stride=self.data_stride
        )
        # self.dijQkl = calculations.dij(
        #     self.Qij, self.array_dims, data_stride=self.data_stride
        # )  # !! slow

        # returns eigenvalues in ascending order at each site
        self.Q_eigenvalues = np.linalg.eigvalsh(self.Q33)

        self.fullmesh["order_uniaxial"] = self.Q_eigenvalues[..., 2]

        site_types = self.fullmesh["site_types"]
        self.fullmesh["order_uniaxial"][site_types > 0] = np.max(
            self.fullmesh["order_uniaxial"]
        )  # fake values to avoid plotting defects behind boundaries

        self.fullmesh["inverse_order"] = (
            safe_inverse(self.fullmesh["order_uniaxial"])
            * nsites
        )

        self.fullmesh["order_biaxial"] = np.abs(
            self.Q_eigenvalues[..., 1] - self.Q_eigenvalues[..., 0]
        ) * nsites
        self.n = calculations.list_to_xyzmat(
            self.fullmesh["director"], self.array_dims
        )

        self.dinj = np.empty(self.n.shape+(3,))

        def dot_prod_sgn(arr1, arr2):
            return np.sign(np.sum(arr1*arr2, axis=-1))

        def dot_prod_sgn_correct(arr_to_correct, ref_arr):
            dps = dot_prod_sgn(arr_to_correct, ref_arr)
            for i in range(3):
                arr_to_correct[..., i] *= dps

        for i in range(3):
            arr_up = np.roll(self.n, -1, axis=i)
            dot_prod_sgn_correct(arr_up, self.n)
            arr_dn = np.roll(self.n, 1, axis=i)
            dot_prod_sgn_correct(arr_dn, self.n)
            self.dinj[..., i, :] = 0.5 / self.data_stride * (arr_up - arr_dn)

        S_inv = self.fullmesh["inverse_order"]

        self.fullmesh["active_force_Q"] = (
            -calculations.einstein_sum("iij", self.diQjk)
        ) * nsites3

        # energy:
        self.fullmesh["LdG_L1"] = nsites * calculations.einstein_sum(
            "ijk, ijk", self.diQjk, self.diQjk
        )
        self.fullmesh["LdG_L2"] = nsites * calculations.einstein_sum(
            "jij, kik", self.diQjk, self.diQjk
        )
        self.fullmesh["LdG_L3"] = nsites * calculations.einstein_sum(
            "jik, kij", self.diQjk, self.diQjk
        )
        self.fullmesh["LdG_L6"] = nsites * calculations.einstein_sum(
            "ij,ikl,jkl", self.Qij, self.diQjk, self.diQjk
        )
        self.fullmesh["LdG_L4"] = nsites * calculations.einstein_sum(
            "lik, lj, kij", levi_civita, self.Qij, self.diQjk
        )
        L1 = self.fullmesh["LdG_L1"]
        L2 = self.fullmesh["LdG_L2"]
        L3 = self.fullmesh["LdG_L3"]
        L4 = self.fullmesh["LdG_L4"]
        L6 = self.fullmesh["LdG_L6"]

        self.fullmesh["LdG_L24"] = L2 - L3

        # self.fullmesh["splay-bend"] = (
        #     calculations.einstein_sum("ijij", self.dijQkl)
        # ) * nsites

        prefactor = (2/9)*S_inv**2
        tmp = 2/3 * S_inv * L6
        self.fullmesh["LdG_K1"] = prefactor * (-L1/3 + 2*L2 - tmp)
        self.fullmesh["twist_Q"] = -2 * prefactor * L4
        self.set_q0(self.q0)  # <- calculate LdG_K2 here
        self.fullmesh["LdG_K3"] = prefactor * (L1/3 + tmp)
        self.fullmesh["saddle-splay_Q"] = (
            2 * prefactor
            * self.fullmesh["LdG_L24"]
        )

        self.fullmesh["cholestericity"] = (
            self.fullmesh["twist_Q"]**2 - 2 * self.fullmesh["saddle-splay_Q"]
        )

        glyphs.calculate_tangent(
            self.dims,
            self.fullmesh, S_inv, threshold=1/0.4,
            field_name='defect_tangent'
        )

    def set_q0(self, q0):
        """ set energetically preferred helicity and (re)calculate associated data fields """
        self.q0 = q0
        nsites = self.fullmesh["nematic_sites"]
        self.fullmesh["LdG_K2"] = (
            (self.fullmesh["twist_Q"] + self.q0)**2
        ) * nsites

    def calculate_Frank_energy_comps(self):
        nsites = self.fullmesh["nematic_sites"]
        nsites3 = np.stack((nsites,)*3, axis=-1)

        self.fullmesh["splay"] = calculations.einstein_sum("ii", self.dinj) * nsites
        self.fullmesh["splay_vec"] = calculations.einstein_sum(
            "i, jj", self.n, self.dinj
        ) * nsites3

        self.fullmesh["curl_n"] = calculations.einstein_sum(
            "ijk,jk", levi_civita, self.dinj
        ) * nsites3
        self.fullmesh["twist_n"] = calculations.einstein_sum(
            "i,ijk,jk", self.n, levi_civita, self.dinj
        ) * nsites
        self.fullmesh["bend"] = (
            calculations.einstein_sum("i, ij", self.n, self.dinj)
        ) * nsites3

        self.fullmesh["rotation_vector_n"] = (
            self.fullmesh["curl_n"]
            - self.fullmesh["director"]*np.stack(
                (self.fullmesh["twist_n"],)*3, axis=-1
            )
        )
        tmp = np.sqrt(
            safe_inverse(
                np.sum(self.fullmesh["rotation_vector_n"]**2, axis=-1)
            )
        )
        self.fullmesh["rotation_vector_n"] *= np.stack((tmp,)*3, axis=-1)

        self.fullmesh["Frank_K1"] = self.fullmesh["splay"]**2 * nsites
        self.fullmesh["Frank_K2"] = (
            (self.fullmesh["twist_n"] + self.q0)**2
        ) * nsites
        self.fullmesh["Frank_K3"] = (
            np.sum(self.fullmesh["bend"]**2, axis=-1)
            * nsites
        )

        self.fullmesh["Frank_K24"] = (
             calculations.einstein_sum("ij,ji", self.dinj, self. dinj)
             - self.fullmesh["Frank_K3"]
        ) * nsites
        self.fullmesh["Frank_oneconst"] = (
            self.fullmesh["Frank_K1"]
            + self.fullmesh["Frank_K2"]
            + self.fullmesh["Frank_K3"]
        )

    def calculate_chi_tensor(self):
        self.chi_Q = calculations.einstein_sum(
            "il, imk, nkl", self.Qij, levi_civita, self.diQjk,
            reshape=False
        )
        tmp = ((2/3*self.fullmesh["inverse_order"])**2).reshape(
            self.Qij.shape[:3]
        )
        for i in range(3):
            for j in range(3):
                self.chi_Q[..., i, j] *= tmp

        self.fullmesh["χ"] = calculations.einstein_sum("ij", self.chi_Q)
        self.chi_Q = calculations.xyzmat_to_list(self.chi_Q)
        self.fullmesh["χᵀχ"] = calculations.transpose_times_matrix(self.chi_Q).reshape(-1, 9)
        self.fullmesh["χχᵀ"] = calculations.matrix_times_transpose(self.chi_Q).reshape(-1, 9)
        self.fullmesh["Ω from χ"] = np.linalg.eigh(
            self.fullmesh["χχᵀ"].reshape(-1, 3, 3)
        )[1][:, :, -1]

    def calculate_D_tensor(self):
        self.D_tensor = np.zeros((self.diQjk.shape[:-1]))
        for g in range(3):
            for i in range(3):
                for m in range(3):
                    for n in range(3):
                        lc1 = levi_civita[g, m, n]
                        if lc1 != 0:
                            for ell in range(3):
                                for k in range(3):
                                    lc2 = levi_civita[i, ell, k]
                                    if lc2 != 0:
                                        self.D_tensor[..., g, i] += (
                                            lc2 * lc2
                                            * np.sum(
                                                self.diQjk[..., ell, m, :]
                                                * self.diQjk[..., k, n, :],
                                                axis=-1
                                            )
                                        )

        self.fullmesh["D_tensor"] = calculations.einstein_sum("ij", self.D_tensor)
        self.D_tensor = calculations.xyzmat_to_list(self.D_tensor)
        self.fullmesh["DᵀD"] = calculations.transpose_times_matrix(self.D_tensor).reshape(-1, 9)
        self.fullmesh["DDᵀ"] = calculations.matrix_times_transpose(self.D_tensor).reshape(-1, 9)

    def calculate_Westin_metrics(self):
        nsites = self.fullmesh["nematic_sites"]
        self.fullmesh["Westin_l"] = (
            self.fullmesh["order_uniaxial"] - self.fullmesh["order_biaxial"]
        ) * nsites
        self.fullmesh["Westin p"] = 4 * self.fullmesh["order_biaxial"] * nsites
        self.fullmesh["Westin_s"] = (
            1 - self.fullmesh["order_uniaxial"]
            - 3*self.fullmesh["order_biaxial"]
        ) * nsites

    @staticmethod
    def Q33_from_Q5(Q5):
        """ Convert 5-component Q-tensor information to 3x3 matrix """
        (Qxx, Qxy, Qxz, Qyy, Qyz) = Q5.T
        Qmat = np.moveaxis(np.array([
                [Qxx, Qxy, Qxz],
                [Qxy, Qyy, Qyz],
                [Qxz, Qyz, -Qxx-Qyy]
                ]), -1, 0)
        return Qmat

    @staticmethod
    def n_from_Q(Qmat):
        """Get director from 3x3-matrix Q-tensor data"""
        _, evecs = np.linalg.eigh(Qmat)
        return evecs[..., -1]

    @staticmethod
    def Q_from_n(director_data):
        """ Create Q-tensor array from director data assuming S_0=1 """
        return 3/2 * (
            np.einsum("...i,...j->...ij", director_data, director_data)
            - np.eye(3) / 3
        )

    @staticmethod
    def director_data_import(filename, sep="\t"):
        director_data = ovm_utilities.fast_import(filename, sep=sep)
        x, y, z, nx, ny, nz = director_data.T
        x -= np.min(x)
        y -= np.min(y)
        z -= np.min(z)
        Qxx = 3/2 * (nx * nx - 1/3)
        Qxy = 3/2 * nx * ny
        Qxz = 3/2 * nx * nz
        Qyy = 3/2 * (ny * ny - 1/3)
        Qyz = 3/2 * ny * nz
        open_Qmin_style_data = np.array([
            x, y, z,
            Qxx, Qxy, Qxz, Qyy, Qyz,
            np.zeros_like(x), np.ones_like(x)
        ]).T
        return open_Qmin_style_data

    def auto_setup(self):
        self.setup_director()
        self.setup_boundaries()
        self.setup_defects()

    def analyze_disclinations(
        self, defects_mesh_name="defects", n_centroids=80, n_samples_circuit=16
    ):
        defects_formula = self.get_filter_formula(defects_mesh_name)
        centroids_name = defects_formula.get_centroids(
            num_centroids=n_centroids
        )
        centroids_formula = self.get_filter_formula(centroids_name)
        self.set_actors_visibility(centroids_name, 0)

        circuits_name = centroids_formula.get_circuits(
            n_samples=n_samples_circuit
        )
        circuits_formula = self.get_filter_formula(circuits_name)
        circuits_formula.do_after_update.append(
            lambda filter_formula: filter_formula.get_eigenvec(
                "Q", new_dataset_name="local_director"
            )
        )
        self.set_actors_visibility(circuits_name, 0)
        circuits_formula.update()

        self['director_circuits'] = circuits_formula.add_glyphs(
            orient="local_director", factor=2, scale="ones"
        )
        self['director_circuits'].set(
            scalars="LdG_L24", clim=[-0.02, 0.02], cmap="turbo_r"
        )
        self.calculate_chi_tensor()

        self.refresh()
        centroids_formula.do_after_update.append(
            lambda filter_formula: filter_formula.get_eigenvec(
                "χχᵀ", new_dataset_name="omega"
            )
        )
        centroids_formula.update()
        self["Ω"] = centroids_formula.add_filter(
            "glyph", orient="omega", factor=4, scale="ones"
        )

        omega_formula = self.get_filter_formula("Ω")
        omega_formula.set_glyph_shape("dbl_headed_arrow")
        omega_formula.set(
            scalars="LdG_L24", clim=[-0.1, 0.1], cmap="turbo_r"
        )
        omega_formula.update()

        self["defects"].set(opacity=0.2)

    def show_jones(self, **kwargs):
        return jones.add_jones_to_plotter(self, **kwargs)


class NematicPlot(FilterTreePlotQt, NematicPlotNoQt):
    def __init__(
        self, filenames=None, q0=0, user_settings=None,
        data_format="open-Qmin", sep="\t", reader=ovm_utilities.fast_import,
        theme=None, do_auto_setup=True, dims=None, data_stride=1, **kwargs
    ):
        NematicPlotNoQt.pre_init(
            self, q0, data_format, do_auto_setup, reader, sep
        )
        FilterTreePlotQt.__init__(
            self, filenames=filenames, user_settings=user_settings,
            theme=theme, reader=reader, **kwargs
        )
        NematicPlotNoQt.post_init(self)
        self.toolbars["widgets"].populate()
        self.the_Add_menu.addSeparator()
        self.the_Add_menu.addAction(
            "Jones matrix planes", self.show_jones
        )
        if filenames is None and dims is not None:
            self.Lx, self.Ly, self.Lz = dims
            self.data_stride = data_stride
            self.create_fullmesh()


    def finish_setup_calculate_menu(self):
        for label, action in (
            ("Frank energy", self.calculate_Frank_energy_comps),
            ("D_tensor", self.calculate_D_tensor),
            ("χ_tensor", self.calculate_chi_tensor),
            ("Westin metrics", self.calculate_Westin_metrics)
        ):
            self.calculate_menu.addAction(label, action)
        self.calculate_menu.addSeparator()

        def q0_callback():
            q0_entry, ok = qw.QInputDialog().getDouble(
                self.app_window,  # parent
                "Energetically preferred chiral wavenumber",  # title
                "q0:",  # label
                self.q0,  # value
                -np.inf,  # minValue
                np.inf,  # maxValue
                3,  # decimals
            )
            if ok:
                self.set_q0(q0_entry)

        self.calculate_menu.addAction("Set q0", q0_callback)

    def auto_setup(self):
        NematicPlotNoQt.auto_setup(self)
        self.finish_setup_calculate_menu()
        self.filters_tree.expandAll()

    def setup_defects(self):
        NematicPlotNoQt.setup_defects(self)
        defects_S_value = self.settings["defects_S"]
        max_S_value = np.max(self.fullmesh["order_uniaxial"])

        toolbar = self.actor_control_toolbars.get("defects")
        if toolbar is not None:
            toolbar.add_isosurface_QSlider(
                min_val=0, max_val=max_S_value, label_txt="S = ",
                init_val_percent=int(defects_S_value / max_S_value * 100),
            )

    def setup_director(self):
        slice_name, _, widget_name = NematicPlotNoQt.setup_director(self)
        self.actor_control_toolbars[slice_name].make_slice_slider_controls(
            widget_name=widget_name
        )

    def _init_procedures_after_data_import(self):
        FilterTreePlotQt._init_procedures_after_data_import(self)

    def show_jones(self, **kwargs):
        return jones.add_jones_toolbar_to_plotter(self, **kwargs)
