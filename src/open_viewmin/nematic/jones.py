""" Jones matrix calculations for transmission of polarized light through the
nematic liquid crystal.

This file was derived, with permission, from code written by Sophie Ettinger
and adapted for open-Qmin data by Yvonne Zagzag.
"""

import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation
import pyvista as pv
import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
from ..filter_tree_plot_Qt.widgets_Qt import utilities_Qt


def n_and_site_types_from_open_Qmin(filename):
    """ Import Q-tensor data in the open-Qmin format """

    data = np.loadtxt(filename, delimiter='\t')
    Qxx, Qxy, Qxz, Qyy, Qyz, site_types = data.T[3:9]

    # take system dimensions from coordinates on last line
    dims = tuple(np.asarray(data[-1][:3] + 1, dtype=int))

    # form traceless, symmetric matrix from indep. Q-tensor components
    Qmat = np.moveaxis(
        np.array([
            [Qxx, Qxy, Qxz],
            [Qxy, Qyy, Qyz],
            [Qxz, Qyz, -Qxx - Qyy]
        ]),
        -1, 0
    )

    # Calculate director as eigevector with leading eigenvalue
    _, evecs = np.linalg.eigh(Qmat)
    n = evecs[:, :, 2]

    # Reshape 1D arrays into system (Lx, Ly, Lz) dimensions
    n = n.reshape(dims + (3,), order='F')
    site_types = site_types.reshape(dims, order='F')

    return n, site_types


def rotation_matrix_2D(angle):
    c = np.cos(angle)
    s = np.sin(angle)
    rot_mat = np.array([
        [c, -s],
        [s,  c]
    ])

    # move newly created axes to last indices
    if isinstance(angle, np.ndarray):
        if len(angle.shape) > 1:
            rot_mat = np.moveaxis(rot_mat, (0, 1), (-2, -1))

    return rot_mat


def rotation_transform(mat, rot_mat):
    """ Calculate M' = R M R^T for given matrix M and rotation matrix R """
    rot_mat_transposed = np.swapaxes(rot_mat, -1, -2)
    return rot_mat @ mat @ rot_mat_transposed


def jones_matrix(n_phi, phi_ext, phi_ord):
    """ Calculate Jones matrix array for one slice at constant z """

    # 2D array of 2x2 rotation matrices
    rot_mat = np.asarray(rotation_matrix_2D(n_phi), dtype=complex)

    # Jones matrix in local coord. syst. where director is along x.
    jones = np.zeros_like(rot_mat)
    jones[..., 0, 0] = np.exp(1j * phi_ext)
    jones[..., 1, 1] = np.exp(1j * phi_ord)

    # rotation transform of Jones matrix into the lab frame
    jones[:] = rotation_transform(jones, rot_mat)

    return jones


def jones_light_propagation_through_sample(
    director,
    site_types,
    wavelength=660e-9,
    res=4.5e-9,
    n_ord=1.55,  # ordinary index of refraction (default value from 5CB)
    n_ext=1.70,  # extraordinary index of refraction (default value from 5CB)
    optical_axis='z'
):
    """ Calculate Jones matrix product for light propagation through nematic
    sample.

    Parameters
    ----------
    director
    site_types
    wavelength
    res
    n_ord
    n_ext
    optical_axis

    Returns
    -------

    """

    director = permute_axes(director, optical_axis)
    site_types = permute_axes(site_types, optical_axis)

    dim_x, dim_y, dim_z = director.shape[:3]

    phi_ord = 2 * np.pi * n_ord * res / wavelength

    # director components
    n_z = director[..., 2]
    n_phi = np.arctan2(director[..., 1], director[..., 0])

    # effective extraordinary refractive index 3D array
    phi_ext_eff_coeff = n_ord * n_ext * 2 * np.pi * res / wavelength
    phi_ext_eff = phi_ext_eff_coeff / np.sqrt(
        (n_ext * n_z) ** 2 + n_ord ** 2 * (1 - n_z ** 2)
    )

    # 3D array of 2x2 Jones matrices
    jones_matrix_array = jones_matrix(n_phi, phi_ext_eff, phi_ord)

    # set Jones matrix to zero at non-nematic sites, i.e. particles are opaque
    jones_matrix_array[site_types > 0, :, :] = 0.

    # iterate over z-values
    J = np.empty_like(jones_matrix_array[:, :, 0])
    J[:] = jones_matrix_array[:, :, 0]  # copy Jones values from 1st layer
    for k in range(1, dim_z):
        J[:] = jones_matrix_array[:, :, k] @ J

    return J


def polarizer_matrix_from_angle(angle):
    """ Calculate projection matrix along a polarizer's direction in the xy plane. """
    rot_mat = rotation_matrix_2D(angle)
    x_projection = np.array([[1, 0], [0, 0]])
    mat = rotation_transform(x_projection, rot_mat)
    return mat


def pom_image_one_wavelength(jones_mat_product, p_angle, a_angle):
    """ Simulated polarized optical microscopy image using a single wavelength of light """
    analyzer = polarizer_matrix_from_angle(a_angle)
    # incoming linearly polarized E field
    E_0 = np.array([np.cos(p_angle), np.sin(p_angle)])
    Exy = analyzer @ jones_mat_product @ E_0
    transmission = np.sum(np.abs(Exy) ** 2, axis=-1)
    return transmission


def pom_image_color(
    jones_mats,
    polarizer_angle=0.,
    analyzer_angle=None,
    brightness_factor=1,  # multiplier for intensity
):
    """ Given Jones matrices for red, green, and blue light propagation,
    generate rgb array (float values in [0, 1]) for a simulated color polarized
    optical microscopy image. """

    if analyzer_angle is None:
        analyzer_angle = polarizer_angle + np.pi / 2

    img_data = np.moveaxis(
        np.array(
            [
                pom_image_one_wavelength(
                    jms, polarizer_angle, analyzer_angle
                )
                for jms in jones_mats
            ],
        ),
        0, -1
    )
    img_data *= brightness_factor
    img_data = np.minimum(img_data, 1)  # cut off values above 1
    return img_data


def permute_axes(arr, axis_name):
    """ Permute coordinate axes so that the optical axis corresponds to
    axis_name

    """

    if axis_name == 'z':
        axes_permut = (0, 1, 2)  # x,y,z -> x,y,z; 0->0, 1->1, 2->2
    elif axis_name == 'x':
        axes_permut = (2, 0, 1)  # x,y,z -> y,z,x; 0->2, 1->0, 2->1
    elif axis_name == 'y':
        axes_permut = (1, 2, 0)  # x,y,z -> z,x,y; 0->1, 1->2, 2->0
    else:
        raise ValueError('axis_name must be \'x\', \'y\', or \'z\'')

    return np.moveaxis(arr, (0, 1, 2), axes_permut)


def jones_matrices_multiple_wavelengths(
    n, site_types,
    wavelengths=(660e-9, 550e-9, 460e-9),  # red, green, blue
    **kwargs
):
    jones_mats = [
        jones_light_propagation_through_sample(
            n, site_types, wavelength=wavelength, **kwargs
        )
        for wavelength in wavelengths
    ]
    return jones_mats


def list_to_xyz_mat(data, dims):
    new_shape = dims + data.shape[1:]
    data_reshaped = data.reshape(new_shape, order="F")
    return data_reshaped


def get_director_data_from_plotter(plotter, n, site_types):
    if n is None:
        n = plotter.default_mesh['director']
    if site_types is None:
        site_types = plotter.default_mesh['site_types']
    elif site_types == 'off':
        site_types = np.zeros(n.shape[0])
    n = list_to_xyz_mat(n, plotter.array_dims)
    site_types = list_to_xyz_mat(site_types, plotter.array_dims)
    return n, site_types


def pom_image_sequence_rotate_polarizers(
    ax, n, site_types,
    num_frames=10,
    brightness_factor=1,
    **kwargs
):
    """ Create image sequence of simulated polarized optical microscopy images
    with polarizer and analyzer rotating linearly in time through total angle pi/2.

    """

    jones_mats = jones_matrices_multiple_wavelengths(
        n, site_types, **kwargs
    )

    ims = []
    for i in range(num_frames):
        polarizer_angle = np.pi / 2 * i / num_frames
        analyzer_angle = polarizer_angle + np.pi / 2
        img_data = pom_image_color(
            jones_mats, polarizer_angle, analyzer_angle,
            brightness_factor=brightness_factor
        )
        img = ax.imshow(img_data, animated=(i != 0))
        ims.append([img])
    return ims


def pom_animation_rotate_polarizers(
    plotter, n=None, site_types='off', interval=50, filename=None, **kwargs
):
    """ Create video of simulated polarized optical microscopy images
    with polarizer and analyzer rotating linearly in time through total angle
    pi/2.

    """

    n, site_types = get_director_data_from_plotter(plotter, n, site_types)

    plt.ioff()  # don't show frames as they're created

    # create figure with correct aspect ratio
    dim_x, dim_y = n.shape[:2]
    fig, ax = plt.subplots(figsize=(10 * dim_y / dim_x, 10))
    ax.axis('off')  # disable axes ticks

    # image sequence
    ims = pom_image_sequence_rotate_polarizers(ax, n, site_types, **kwargs)

    # animation
    anim = ArtistAnimation(fig, ims, interval=interval, blit=True)
    plt.close()  # don't leave figure open

    vid_txt = anim.to_html5_video()

    if filename is None:
        return vid_txt
    else:
        with open(filename, 'w') as f:
            f.write(vid_txt)
        return filename


def rgb_to_texture(rgb_data):
    return pv.numpy_to_texture(np.asarray(256 * rgb_data, dtype=np.uint8))


def add_jones_to_plotter(
    plotter, n=None, site_types=None,
    brightness_factor=1, polarizer_angle=0, analyzer_angle=None, **kwargs
):
    """ Add planes to back of box, colored by RGB Jones matrix transmission
    and normal to x, y, z.

    """

    n, site_types = get_director_data_from_plotter(plotter, n, site_types)

    for i in range(3):
        z_dir_idx = (i + 2) % 3
        optical_axis = ['x', 'y', 'z'][z_dir_idx]
        rgb_data = pom_image_color(
            jones_matrices_multiple_wavelengths(
                n, site_types, optical_axis=optical_axis, **kwargs
            ),
            polarizer_angle=polarizer_angle,
            analyzer_angle=analyzer_angle,
            brightness_factor=brightness_factor
        )

        # corrections for the way imshow flips axes around
        if i in [0, 1]:
            rgb_data = np.moveaxis(rgb_data, 0, 1)
        rgb_data = np.flip(rgb_data, axis=0)

        center = np.array(n.shape[:3]) / 2
        center[z_dir_idx] = 0.01  # some small non-zero value needed
        normal = np.identity(3)[z_dir_idx]

        tex = rgb_to_texture(rgb_data)

        pln = plotter.default_mesh.slice(normal=normal, origin=center)
        pln.texture_map_to_plane(inplace=True)
        actor_name = f"Jones_{optical_axis}"
        plotter.remove_actor(actor_name)
        plotter.add_mesh(pln, texture=tex, name=actor_name)

        jones_arrows_scale = plotter.settings["Jones_arrows_scale"]
        jones_arrows_placement_dist = (
            plotter.settings["Jones_arrows_placement_dist"]
        )
        p_arrows_color = plotter.settings["Jones_p_arrows_color"]
        a_arrows_color = plotter.settings["Jones_a_arrows_color"]
        jones_arrows_font_size = plotter.settings["Jones_arrows_font_size"]
        jones_arrows_label_separation = plotter.settings["Jones_arrows_label_separation"]

        x_idx = i
        y_idx = (i + 1) % 3
        x_dir = np.identity(3)[x_idx]
        y_dir = np.identity(3)[y_idx]
        p_dir = (
            x_dir * np.cos(polarizer_angle)
            + y_dir * np.sin(polarizer_angle)
        )

        arrows_center = (
                center
                + 0.5 * (
                    plotter.dims[x_idx] * x_dir + plotter.dims[y_idx] * y_dir
                )
                + jones_arrows_placement_dist * (x_dir + y_dir) / np.sqrt(2)
        )
        if analyzer_angle is None:
            analyzer_angle = polarizer_angle + np.pi / 2
        a_dir = (
                x_dir * np.cos(analyzer_angle)
                + y_dir * np.sin(analyzer_angle)
        )

        for arrow_dir, arrow_label_txt, arrow_color in zip(
            (p_dir, a_dir),
            ("P", "A"),
            (p_arrows_color, a_arrows_color)
        ):
            arrow_1, arrow_2 = [
                pv.Arrow(
                    arrows_center,
                    jones_arrows_scale * i_sgn * arrow_dir,
                    scale="auto"
                )
                for i_sgn in [1, -1]
            ]
            arrows = arrow_1 + arrow_2
            arrows_actor_name = f"{actor_name}_{arrow_label_txt}"
            plotter.remove_actor(arrows_actor_name)
            plotter.add_mesh(
                arrows, color=arrow_color, name=arrows_actor_name
            )
            plotter.remove_actor(arrows_actor_name + "-labels")
            plotter.add_point_labels(
                [
                    arrows_center
                    + (
                        (jones_arrows_scale + jones_arrows_label_separation)
                        * arrow_dir * (-1 if arrow_dir[2] < 0 else 1)
                    )
                ],
                [arrow_label_txt],
                show_points=False,
                shape=None,
                font_size=jones_arrows_font_size,
                text_color=arrow_color,
                name=arrows_actor_name,
                italic=False,
                bold=False,
            )


def add_jones_toolbar_to_plotter(plotter, n=None, site_types=None, **kwargs):
    plotter.toolbars["Jones"] = utilities_Qt.ControlsToolbar(
        name="Jones", label="<b>Jones</b>", icon_size=plotter.settings["icon_size"]
    )
    jones_toolbar = plotter.toolbars["Jones"]
    jones_toolbar.options_toolbar.setOrientation(2)
    visibility_checkboxes = []

    def checkbox_callback(checkbox, jones_actor_name):
        checked = checkbox.isChecked()
        for actor_name in plotter.renderer.actors.keys():
            if actor_name.startswith(jones_actor_name):
                actor = plotter.renderer.actors[actor_name]
                actor.SetVisibility(checked)

    def jones_callback(_):
        try:
            kwargs["polarizer_angle"] = (
                    polarizer_angle_toolbar.slider.value() / 100 * np.pi / 2
            )
        except NameError:  # ignore if toolbar doesn't exist yet
            return
        try:
            kwargs["brightness_factor"] = (
                    jones_brightness_toolbar.slider.value() / 20
            )
        except NameError:  # ignore if toolbar doesn't exist yet
            return
        add_jones_to_plotter(
            plotter, n=n, site_types=site_types, **kwargs
        )
        if len(visibility_checkboxes) == 3:
            for chkbx, norm_dir_lbl in zip(
                visibility_checkboxes, ['x', 'y', 'z']
            ):
                checkbox_callback(chkbx, f"Jones_{norm_dir_lbl}")

    polarizer_angle_toolbar = utilities_Qt.ControlsSliderToolbar(
        jones_callback,
        label_txt="P angle    ︎",
        min_val=0,
        max_val=100,
        init_val_percent=0,
        name="Jones_polarizer_angle",
        spinbox=False,
    )
    polarizer_angle_toolbar.slider.setFixedWidth(150)
    jones_toolbar.options_toolbar.addWidget(polarizer_angle_toolbar)

    jones_brightness_toolbar = utilities_Qt.ControlsSliderToolbar(
        jones_callback,
        label_txt="brightness",
        min_val=0,
        max_val=100,
        init_val_percent=20,
        name="Jones brightness",
        spinbox=False
    )
    jones_brightness_toolbar.slider.setFixedWidth(150)
    jones_toolbar.options_toolbar.addWidget(jones_brightness_toolbar)

    jones_checkbox_toolbar = qw.QToolBar()

    jones_callback(None)  # create the Jones planes

    def add_jones_checkbox(norm_dir):
        checkbox = qw.QCheckBox()
        checkbox.setText(norm_dir)
        actor_name = "Jones_" + norm_dir
        actor = plotter.renderer.actors[actor_name]
        checkbox.setChecked(actor.GetVisibility())

        checkbox.stateChanged.connect(
            lambda: checkbox_callback(checkbox, actor_name)
        )
        jones_checkbox_toolbar.addWidget(checkbox)
        visibility_checkboxes.append(checkbox)

    for norm_dir in ['x', 'y', 'z']:
        add_jones_checkbox(norm_dir)

    jones_toolbar.options_toolbar.addWidget(jones_checkbox_toolbar)

    plotter.controls_dock_window.addToolBar(
        Qt.Qt.LeftToolBarArea, jones_toolbar
    )
