"""
General calculation utilities.
"""

import numpy as np

levi_civita = np.zeros((3, 3, 3), dtype=int)
levi_civita[0, 1, 2] = levi_civita[1, 2, 0] = levi_civita[2, 0, 1] = 1
levi_civita[0, 2, 1] = levi_civita[2, 1, 0] = levi_civita[1, 0, 2] = -1


def safe_inverse(arr):
    """ take inverse x->1/x but with 0->0 """
    return (
            np.asarray(arr != 0, dtype=float)
            / (arr + (arr == 0))
    )


def matrix_times_transpose(arr):
    return arr @ arr.transpose((0, 2, 1))


def transpose_times_matrix(arr):
    return arr.transpose((0, 2, 1)) @ arr


def list_to_xyzmat(arr, dims):
    return arr.reshape(
        tuple(reversed(dims))
        + arr.shape[1:]
    )


def xyzmat_to_list(arr):
    return arr.reshape(
        (np.prod(arr.shape[:3]),)
        + arr.shape[3:]
    )


def di(arr, array_dims, flatten=False, component=None, data_stride=1):
    """ Gradient """
    if len(arr.shape) < 4:
        arr = list_to_xyzmat(arr, array_dims)

    if component in [0, 1, 2]:
        ans = 0.5 / data_stride * (
                np.roll(arr, -1, axis=component)
                - np.roll(arr, 1, axis=component)
        )
    else:
        ans = np.empty(arr.shape[:3] + (3,) + arr.shape[3:])
        for i in range(3):
            ans[:, :, :, i, ...] = 0.5 / data_stride * (
                    np.roll(arr, -1, axis=i) - np.roll(arr, 1, axis=i)
            )
    if flatten:
        ans = ans.reshape((np.prod(ans.shape[:3]),) + ans.shape[3:])
    return ans


def dij(arr, array_dims, flatten=False, data_stride=1):
    """ Hessian """
    if len(arr.shape) < 4:
        arr = list_to_xyzmat(arr, array_dims)
    ans = np.empty(arr.shape[:3] + (3, 3) + arr.shape[3:])
    for i in range(3):
        ans[:, :, :, i, i, ...] = 1 / (data_stride ** 2) * (
                np.roll(arr, -1, axis=i) - 2 * arr + np.roll(arr, 1, axis=i)
        )
    for i, j in [[0, 1], [1, 2], [0, 2]]:
        ans[:, :, :, i, j, ...] = 0.25 / (data_stride ** 2) * (
                np.roll(np.roll(arr, -1, axis=i), -1, axis=j)
                - np.roll(np.roll(arr, -1, axis=i), 1, axis=j)
                - np.roll(np.roll(arr, 1, axis=i), -1, axis=j)
                + np.roll(np.roll(arr, 1, axis=i), 1, axis=j)
        )
        ans[:, :, :, j, i, ...] = ans[:, :, :, i, j, ...]
    if flatten:
        ans = ans.reshape((np.prod(ans.shape[:3]),) + ans.shape[3:])
    return ans


def einstein_sum(sum_str, *arrays, reshape=True):
    """ Increase flexibility of numpy.einsum, for convenience in calculating
    datasets

    """

    sum_str = "..."+sum_str
    sum_str = (
        sum_str.replace(" ", "").replace(",", ",...").replace("->", "->...")
    )
    answer = np.einsum(sum_str, *arrays)
    if reshape:
        answer = answer.reshape((-1,) + answer.shape[3:])
    return answer


def calculate_from_scalar_field(
    plotter, scalar_field_name, operation_string, mesh_name="fullmesh"
):
    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        sfield = mesh[scalar_field_name]
        result_name = scalar_field_name + '_' + operation_string
        if operation_string == 'gradient':
            mesh[result_name] = di(sfield, plotter.array_dims, flatten=True)
        elif operation_string == 'Laplacian':
            mesh[result_name] = einstein_sum(
                "ii", dij(sfield, plotter.array_dims, flatten=True)
            )
        plotter.update_filter('fullmesh')
    return return_func


def calculate_from_vector_field(
    plotter, vector_field_name, operation_string, mesh_name="fullmesh"
):
    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        vfield = mesh[vector_field_name]
        result_name = vector_field_name + '_' + operation_string
        if operation_string in ['x', 'y', 'z']:
            idx = ['x', 'y', 'z'].index(operation_string)
            mesh[result_name] = vfield[:, idx]
        elif operation_string in ['|x|', '|y|', '|z|']:
            idx = ['|x|', '|y|', '|z|'].index(operation_string)
            mesh[result_name] = np.abs(vfield[:, idx])
        elif operation_string == 'div':
            ans = np.zeros(vfield.shape[0])
            for i in range(3):
                ans += di(
                    vfield[:, i], plotter.array_dims, component=i, flatten=True
                )
            mesh[result_name] = ans
        elif operation_string == 'curl':
            djvi = [di(
                vfield[:, i], plotter.array_dims, flatten=True
            ) for i in range(3)]
            ans = np.zeros(vfield.shape)
            for i in range(3):
                for j in range(3):
                    for k in range(3):
                        eps = plotter.levi_civita[i, j, k]
                        if eps != 0:
                            ans[:, i] += eps * djvi[k][:, j]
            mesh[result_name] = ans
        plotter.update_filter('fullmesh')

    return return_func


def calculate_from_tensor_field(
    plotter, tensor_field_name, operation_string, mesh_name="fullmesh"
):
    def return_func():
        mesh = plotter.get_mesh(mesh_name)
        tfield = mesh[tensor_field_name].reshape(-1, 3, 3)
        if operation_string == 'eigvalsh':
            evals = np.linalg.eigvalsh(tfield)
            for idx, mstr in enumerate(['min', 'mid', 'max']):
                mesh[tensor_field_name + '_eigval_' + mstr] = evals[:, idx]
        elif operation_string == 'eigh':
            evals, evecs = np.linalg.eigh(tfield)
            for idx, mstr in enumerate(['min', 'mid', 'max']):
                mesh[tensor_field_name + '_eigval_' + mstr] = evals[:, idx]
                mesh[tensor_field_name + '_eigvvec_' + mstr] = evecs[:, :, idx]
        plotter.update_filter('fullmesh')
    return return_func
