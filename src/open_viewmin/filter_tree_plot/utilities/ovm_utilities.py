"""

Define utility functions and classes for use throughout project.

"""

from pandas import read_csv
import pyvista as pv
import inspect

pv.set_jupyter_backend('pythreejs')


def fast_import(filename, sep="\t"):
    """ Use pandas.read_csv to import .txt/.dat files quickly """
    ret = read_csv(filename, sep=sep, header=None)
    ret = ret.to_numpy()
    return ret


def available_filters(mesh):
    if isinstance(mesh, pv.MultiBlock):
        avail = pv.MultiBlock
    elif isinstance(mesh, pv.UniformGrid):
        avail = pv.UniformGridFilters
    elif isinstance(mesh, pv.UnstructuredGrid):
        avail = pv.UnstructuredGridFilters
    elif isinstance(mesh, pv.PolyData):
        avail = pv.PolyDataFilters
    elif isinstance(mesh, pv.DataSet):
        avail = pv.DataSetFilters
    else:
        return []
    fltrs = [
        fltr
        for fltr in dir(avail)
        if (
            fltr[0] != "_"
            and fltr not in [
                "boolean_add",
                "boolean_cut"
            ]
            and fltr[:len('plot_')] != 'plot_'
        )
    ]
    return fltrs


def sort_filenames_by_timestamp(filenames):
    timestamps = []
    for filename in filenames:
        filename_trimmed = ''.join(filename.split(".")[:-1])
        filename_suffix = filename_trimmed.split("_")[-1]
        # check for open-Qmin mpi x#y#z# string, to ignore for timestamp
        if (
                "x" in filename_suffix
                and "y" in filename_suffix
                and "z" in filename_suffix
        ):
            check_for_mpi_suffix = filename_suffix.replace("x", "")
            check_for_mpi_suffix = check_for_mpi_suffix.replace("y", "")
            check_for_mpi_suffix = check_for_mpi_suffix.replace("z", "")
            if check_for_mpi_suffix.isdigit():
                filename_suffix = filename_trimmed.split("_")[-2]
        if filename_suffix.isnumeric():
            timestamps.append(int(filename_suffix))
        else:
            # give up on sorting by timestamp
            return filenames

    sorted_filenames = []
    for timestamp in sorted(timestamps):
        sorted_filenames.append(filenames[timestamps.index(timestamp)])
    return sorted_filenames


def name_without_overwriting(try_new_name, dict_of_old_names):
    """
    To avoid overwriting keys in a dictionary,
    modify a new key until it matches no older ones.
    Last argument can be dictionary or a list of keys.
    """
    if isinstance(dict_of_old_names, (list, tuple, type({}.keys()))):
        old_names = dict_of_old_names
    elif isinstance(dict_of_old_names, dict):
        old_names = list(dict_of_old_names.keys())
    else:
        old_names = []
    new_name = try_new_name
    i = 1
    while new_name in old_names:
        i += 1
        new_name = try_new_name + "_" + str(i)
    return new_name


def get_kwargs_and_defaults(func, as_strings=False):
    """ For a given function, try to obtain the names of all keyword arguments
    and their default values

    """

    sig = inspect.signature(func)
    params = sig.parameters
    kwargs = {
        key: (
            params[key].default
            if params[key].default != inspect._empty
            else None
        )
        for key in params.keys()
    }
    last_arg_name = list(params)[-1]
    # for **kwargs case
    if "**" + last_arg_name in str(sig):
        del kwargs[last_arg_name]
        kwargs["**" + last_arg_name] = {}

    if as_strings:
        for key in kwargs.keys():
            if isinstance(kwargs[key], str):
                kwargs[key] = f"\'{kwargs[key]}\'"
            else:
                kwargs[key] = str(kwargs[key])
    return kwargs


def copy_from_dict_recursively(from_dict, to_dict=None):
    if to_dict is None:
        to_dict = dict()
    for key in from_dict.keys():
        val = from_dict[key]
        if isinstance(val, dict):
            if key not in to_dict.keys():
                to_dict[key] = dict()
            copy_from_dict_recursively(val, to_dict[key])
            # if isinstance(to_dict[key], dict):
            #     copy_from_dict_recursively(from_dict[key], to_dict[key])
            # else:
            #     to_dict[key] = val
        else:
            # non-dictionary values are simply copied
            to_dict[key] = val
    return to_dict


def add_if_dict_lacks(to_dict, from_dict):
    """ For any keys absent in a first dictionary but present in a second,
    copy the key-value pairs to the first dictionary
    """
    for key in from_dict.keys():
        if key not in to_dict.keys():
            to_dict[key] = from_dict[key]

def hex_to_RGB(hex_color):
    hex_color = hex_color.lstrip('#')

    # from https://stackoverflow.com/questions/29643352/converting-hex-to-rgb-value-in-python
    RGB_color = tuple(int(hex_color[i:i+2], 16) for i in (0, 2, 4))

    return RGB_color
