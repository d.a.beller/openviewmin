""" Utility to merge data from separate files for a single scene, such as from
an MPI run"""

import numpy as np
import re


def mpi_stitch(in_filenames, out_filename=None):
    """
    Stitches together a set of data text files from an MPI run into a single
    file with the proper ordering of lines for use by openViewMin. First three
    columns must be x y z coords.

    """

    data = np.concatenate([np.loadtxt(f) for f in in_filenames])
    coords = np.asarray(data[:, :3], dtype=int)
    order = np.lexsort(coords.T)
    ordered_data = data[order]
    if out_filename is not None:
        fmt = "%d\t%d\t%d"
        for i in range(data.shape[1] - 3):
            fmt += "\t%f"
        np.savetxt(
            out_filename,
            ordered_data,
            fmt=fmt
        )
        print(in_filenames, "->", out_filename)
    return ordered_data


def mpi_group(filenames):
    """ Check whether some filenames come from the same MPI run
    (formatted as same_prefix_x#y#z#.txt) and if so, stitch them together
    using mpi_stitch()

    """

    fnames_mpi = ([], [])
    fnames_no_mpi = []
    for f in filenames:
        if bool(re.search(r'_x\d+y\d+z\d+.', f)):
            fprefix = '_'.join(f.split('_')[:-1])
            if fprefix in fnames_mpi[0]:
                fnames_mpi[1][fnames_mpi[0].index(fprefix)].append(f)
            else:
                fnames_mpi[0].append(fprefix)
                fnames_mpi[1].append([f])
        else:
            fnames_no_mpi.append(f)
    for fprefix, fnames in zip(fnames_mpi[0], fnames_mpi[1]):
        if len(fnames) > 1:
            outf = fprefix + '.txt'
            mpi_stitch(fnames, out_filename=outf)
        else:
            outf = fnames[0]
        fnames_no_mpi.append(outf)
    return fnames_no_mpi
