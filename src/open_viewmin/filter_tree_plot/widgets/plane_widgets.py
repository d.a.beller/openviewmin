""" Utilities for working with PyVista plane widgets
"""

import numpy as np
from ..filters import isosurfaces
from ..utilities import ovm_utilities, scalar_bars


def new_slice_mesh(
    plotter, slice_name=None, mesh_to_slice_name="fullmesh",
    normal=(1.0, 0.0, 0.0), origin=None, **mesh_kwargs
):
    if origin is None:
        origin = [dim / 2 for dim in plotter.dims]
    if slice_name is None:
        slice_name = mesh_to_slice_name + "_slice"
    use_mesh_kwargs = ovm_utilities.copy_from_dict_recursively(
        plotter.settings['default_slice_kwargs']
    )
    ovm_utilities.copy_from_dict_recursively(mesh_kwargs, use_mesh_kwargs)
    slice_mesh = plotter.add_filter_formula(
        name=slice_name,
        parent_mesh_name=mesh_to_slice_name,
        filter_callable="slice",
        filter_kwargs=dict(normal=normal, origin=origin),
        mesh_kwargs=use_mesh_kwargs
    )
    return slice_mesh



def setup_slice_widget(
    plotter, widget_name=None, slice_name=None, mesh_to_slice_name="fullmesh",
    **mesh_kwargs
):
    if slice_name is None and widget_name is not None:
        slice_name = widget_name.replace('_widget', '')
    if widget_name is None:
        widget_name = slice_name + "_widget"
    normal, origin = default_slice_kwargs(plotter).values()
    if slice_name not in plotter.actor_names:
        new_slice_mesh(
            plotter,
            slice_name=slice_name,
            mesh_to_slice_name=mesh_to_slice_name,
            normal=normal,
            origin=origin,
            **mesh_kwargs
        )
    add_slice_widget_to_slice_mesh(
        plotter,
        slice_name=slice_name,
        widget_name=widget_name
    )


def configure_plane_widget(plotter):
    def return_func(widget):
        if widget is None:
            normal, origin = default_slice_kwargs(plotter).values()
            enabled = True
        else:
            normal = widget.GetNormal()
            origin = widget.GetOrigin()
            enabled = widget.GetEnabled()
        return enabled, (normal, origin)
    return return_func


def default_slice_kwargs(plotter):
    return dict(
        normal=(1.0, 0.0, 0.0),
        origin=[dim / 2 for dim in plotter.dims]
    )


def add_slice_aux(plotter, scalars_name=None):
    def return_function():
        add_slice(plotter, scalars_name=scalars_name)
    return return_function


def add_slice(
    plotter, scalars_name=None, slice_name=None, widget_name=None,
    mesh_to_slice_name="fullmesh"
):
    if scalars_name is None:
        color = plotter.settings["slice_color"]
        slice_name = "new_slice"
    else:
        color = None
        if slice_name is None:
            slice_name = plotter.name_mesh_without_overwriting(
                scalars_name + "_slice"
            )
    if widget_name is None:
        widget_name = ovm_utilities.name_without_overwriting(
            slice_name + "_widget",
            plotter.widgets
        )
    mesh_kwargs = ovm_utilities.copy_from_dict_recursively(
        plotter.settings["default_mesh_kwargs"]
    )
    mesh_kwargs["color"] = color
    mesh_kwargs["scalars"] = scalars_name

    setup_slice_widget(
        plotter,
        widget_name,
        slice_name=slice_name,
        mesh_to_slice_name=mesh_to_slice_name,
        **mesh_kwargs
    )
    filter_formula = plotter.get_filter_formula(slice_name)
    # colorbar = filter_formula.colorbar
    # if colorbar is not None:
    #     scalar_bars.standardize_scalar_bar(plotter, colorbar)
    #     colorbar.SetTitle(scalars_name)

    plotter.set_actors_visibility(slice_name, 1)

    # colorbar = plotter.get_filter_formula(slice_name).colorbar
    # if colorbar is not None:
    #     scalar_bars.standardize_scalar_bar(plotter, colorbar)

    return slice_name, widget_name


def slice_widget_callback(plotter, slice_name):
    def return_func(widget_normal, widget_origin):
        plotter.update_filter(
            slice_name,
            update_actor=True,
            normal=widget_normal,
            origin=widget_origin
        )
    return return_func


def add_slice_widget_to_slice_mesh(
    plotter, slice_name=None, widget_name=None
):
    if slice_name in plotter.mesh_names:
        if widget_name is None:
            widget_name = slice_name + "_widget"
        normal, origin = default_slice_kwargs(plotter).values()

        if not plotter.off_screen:
            plotter.add_widget_formula(
                name=widget_name,
                mesh_name=slice_name,
                creator="add_plane_widget",
                callback=slice_widget_callback,
                configure=configure_plane_widget(plotter),
                factor=plotter.settings["widget_factor"],
                color=plotter.settings["widget_color"],
                tubing=True,
            )
        else:
            slice_widget_callback(plotter, slice_name)(normal, origin)


def alter_plane_widget(
    widget_formula, theta=None, phi=None, origin=None, dtheta=0, dphi=0,
    d_origin=(0, 0, 0)
):
    """ Change normal and/or origin of a plane widget,
    and update associated mesh (and possibly actor(s)) accordingly
    """

    widget = widget_formula.get_widget()
    if widget is None:
        return
    normal_x, normal_y, normal_z = widget.GetNormal()
    slice_origin = widget.GetOrigin()
    Ntheta = np.arccos(normal_z)
    Nphi = np.arctan2(normal_y, normal_x)
    if theta is not None:
        Ntheta = theta
    if phi is not None:
        Nphi = phi
    if origin is not None:
        slice_origin = origin
    Ntheta += dtheta
    Nphi += dphi
    slice_origin = np.array(slice_origin) + np.array(d_origin)
    widget.SetNormal(
        np.sin(Ntheta) * np.cos(Nphi),
        np.sin(Ntheta) * np.sin(Nphi),
        np.cos(Ntheta)
    )
    widget.SetOrigin(slice_origin)
    widget_formula.update()


def add_filter_widget_clip_plane(plotter, mesh_name):
    if mesh_name in plotter.mesh_names:
        new_mesh_name = plotter.name_mesh_without_overwriting(
            mesh_name + "_plane_clipped"
        )

        def generate_clip_plane_callback(plotter, mesh_name):
            def clip_plane_callback(normal, origin):
                filter_info = plotter.get_filter_formula(mesh_name)
                scalars = filter_info.mesh_kwargs.get("scalars")
                if scalars is None:
                    color = filter_info.mesh_kwargs.get(
                        "color", plotter.settings['slice_color']
                    )
                else:
                    color = None
                mesh_kwargs = dict(color=color, scalars=scalars)
                plotter.add_filter_formula(
                    name=new_mesh_name,
                    parent_mesh_name=mesh_name,
                    filter_callable="clip",
                    filter_kwargs=dict(normal=normal, origin=origin),
                    mesh_kwargs=mesh_kwargs
                )
            return clip_plane_callback
        widget_name = plotter.name_widget_without_overwriting(
            mesh_name+'_plane_widget'
        )
        plotter.add_widget_formula(
            name=widget_name,
            mesh_name=mesh_name,
            creator="add_plane_widget",
            callback=generate_clip_plane_callback,
            configure=configure_plane_widget(plotter),
            tubing=True,
            factor=plotter.settings['widget_factor'],
            color=plotter.settings['widget_outline_color']
        )
        # make unclipped mesh invisible immediately
        if mesh_name in plotter.actor_names:
            plotter.set_actors_visibility(mesh_name, 0)

        plotter.refresh()


def add_PT_widget(plotter, vectors_name, apolar=True, name=None):
    """ Add a Pontryagin-Thom surface.

    Finds the surface(s) of points where the orientation lies in the plane
    of directions
    perpendicular to `normal_dir`, and colors the surface by angle in this
    plane. For uses of the Pontryagin-Thom construction in nematic liquid
    crystals, see:
    Chen, Ackerman, Alexander, Kamien, and Smalyukh (2013),
    Generating the Hopf fibration experimentally in nematic liquid crystals.
    Phys. Rev. Lett. 110, 237801.
    https://doi.org/10.1103/PhysRevLett.110.237801

    Čopar, Porenta and Žumer (2013),
    Visualisation methods for complex nematic fields,
    Liquid Crystals, 40:12, 1759-1768.
    https://doi.org/110.1080/02678292.2013.853109.

    Machon and Alexander (2014),
    Knotted Defects in Nematic Liquid Crystals,
    Phys. Rev. Lett. 113, 027801.
    https://doi.org/110.1103/PhysRevLett.113.027801

    """
    actor_name = name
    if actor_name is None:
        actor_name = plotter.name_mesh_without_overwriting(
            vectors_name + "_PT"
        )

    mesh_kwargs = {
        **plotter.settings["isosurface_kwargs"].copy(),
        **dict(
            cmap="hsv",
            preference="cell",
            interpolate_before_map=False  # avoid color jumps at branch cuts
        )
    }

    plotter.add_filter_formula(
        actor_name,
        filter_callable="contour",
        mesh_kwargs=mesh_kwargs,
        ovm_info=dict(apolar=apolar),
        apply_upon_creation=False
    )

    def callback_generator(plotter, PT_surface_name):

        def new_normal_callback(normal, _):
            filter_formula = plotter.get_filter_formula(PT_surface_name)
            parent_mesh_name = filter_formula.parent_mesh_name
            theta_key, phi_key, apolar_phi_key = (
                isosurfaces.calculate_PT_surface(
                    plotter.get_mesh(parent_mesh_name),
                    vectors_name,
                    normal_dir=normal
                )
            )

            filter_formula.update(scalars=theta_key, update_actor=False)
            current_apolar = filter_formula.ovm_info.get("apolar", False)
            filter_formula.update_actor(
                scalars=apolar_phi_key if current_apolar else phi_key,
                clim=[0, (1 if current_apolar else 2) * np.pi]
            )

        return new_normal_callback

    widget_name = ovm_utilities.name_without_overwriting(
        actor_name + "_normal", plotter.widgets
    )

    plotter.add_widget_formula(
        name=widget_name,
        mesh_name=actor_name,
        creator="add_plane_widget",
        callback=callback_generator,
        configure=configure_plane_widget(plotter),
        color=plotter.settings["widget_color"],
        tubing=True,
        factor=plotter.settings["widget_factor"]
    )
    return actor_name
