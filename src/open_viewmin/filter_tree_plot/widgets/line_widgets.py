"""
Utilities for working with PyVista line widgets
"""

from pyvista import Line

def setup_line_widget(
    plotter, widget_name=None, new_mesh_name="line_mesh", parent_mesh_name=None
):
    new_mesh_name = plotter.name_mesh_without_overwriting(new_mesh_name)
    if widget_name is None:
        widget_name = new_mesh_name + "_widget"
        widget_name = plotter.name_widget_without_overwriting(widget_name)

    def configure_widget(widget):
        if widget is None:
            enabled = True
        else:
            enabled = widget.GetEnabled()
        return enabled, ()

    plotter.add_filter_formula(
        name=new_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable="probe",
        filter_kwargs=dict(
            points=Line(
                pointa=(0, plotter.Ly / 2, plotter.Lz / 2),
                pointb=(plotter.Lx, plotter.Ly / 2, plotter.Lz / 2)
            )
        )
    )

    def generate_line_widget_callback(plotter, mesh_name):
        def line_widget_callback(pointa, pointb):
            plotter.get_filter_formula(mesh_name).update(
                points=Line(pointa=pointa, pointb=pointb)
            )

        return line_widget_callback

    if not plotter.off_screen:
        plotter.add_widget_formula(
            name=widget_name,
            mesh_name=new_mesh_name,
            callback=generate_line_widget_callback,
            creator="add_line_widget",
            configure=configure_widget,
            factor=plotter.settings["widget_factor"],
            color=plotter.settings["widget_outline_color"],
            use_vertices=True
        )
