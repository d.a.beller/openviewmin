""" Define classes for reusable PyVista filters and widgets, which can be
re-applied in the Plotter based on changes to other meshes. """


import inspect
import numpy as np
import pyvista as pv
import matplotlib.colors
from .utilities import ovm_utilities, scalar_bars
from .filters import sample, glyphs


class FilterFormula:
    def __init__(
        self, plotter, name=None, parent_mesh_name=None,
        filter_callable=None, filter_kwargs=None, mesh_kwargs=None,
        random=None, has_actor=True, in_place=False, ovm_info=None,
        widget_name=None, apply_upon_creation=True, children=None,
        colorbar=None, **extra_filter_kwargs
    ):

        self.plotter = plotter
        self._allowed_args_to_add_mesh = (
            inspect.getfullargspec(plotter.add_mesh).args
        )
        # parent mesh defaults to first root mesh of plotter
        if parent_mesh_name is None:
            parent_mesh_name = plotter.root_meshes[0]
        if name is None:
            self.name = self.plotter.root_meshes[0]
        else:
            self.name = name
        if filter_kwargs is None:
            filter_kwargs = dict()
        filter_kwargs = {**filter_kwargs, **extra_filter_kwargs}
        if ovm_info is None:
            ovm_info = dict()
        self.ovm_info = ovm_info
        if random is not None:
            self.ovm_info["random"] = random
        self.parent_mesh_name = parent_mesh_name
        if children is None:
            self.children = []
        else:
            self.children = children

        if filter_callable is None:
            self.filter_callable = self.identity_callable
        elif isinstance(filter_callable, str):
            self.filter_callable = self.string_to_filter(filter_callable)
        else:
            self.filter_callable = filter_callable

        self.filter_kwargs = filter_kwargs
        self.has_actor = has_actor
        self.in_place = in_place
        self.colorbar = colorbar
        self.widget_name = widget_name

        # A list of tasks, written as function of filter_formula,
        # to perform after updating self but before updating # children,
        # such as any dataset calculations that should be passed to children.
        self.do_after_update = []
        if mesh_kwargs is None:
            mesh_kwargs = dict()
        if (
            mesh_kwargs.get("scalars") is None
            and mesh_kwargs.get("color") is None
            and self.has_actor
        ):
            mesh_kwargs["color"] = self.get_auto_color()
        self.mesh_kwargs = mesh_kwargs
        if apply_upon_creation and not self.is_filter_tree_root:
            self.update(update_actor=False)
            if self.has_actor:
                self.update_actor()

    def __repr__(self):
        ret = str(self.__class__.__name__) + "\n"
        dict_rep = self.__dict__
        for key in dict_rep.keys():
            if key == 'plotter':
                val = self.plotter.__class__.__name__
            elif key[0] == "_":
                continue
            else:
                val = dict_rep[key]
            ret += f"  {key}: {val}\n"
        return ret

    def __getitem__(self, key):
        return self.mesh.__getitem__(key)

    def __setitem__(self, key, set_to):
        return self.mesh.__setitem__(key, set_to)

    @property
    def points(self):
        return self.mesh.points

    @property
    def array_names(self):
        return self.mesh.array_names

    @staticmethod
    def identity_callable(mesh):
        return lambda: mesh

    @property
    def mesh(self):
        return self.plotter.get_mesh(self.name)

    @property
    def actor(self):
        return self.plotter.get_actor(self.name)

    @property
    def parent_mesh(self):
        return self.plotter.get_mesh(self.parent_mesh_name)

    @property
    def is_filter_tree_root(self):
        return (self.parent_mesh_name == self.name)

    @property
    def random(self):
        return self.ovm_info.get("random", False)

    def add_filter(self, filter_callable, **kwargs):
        def filter_formula_lambda(mesh_name):
            self.plotter.add_filter_formula(
                name=mesh_name,
                parent_mesh_name=self.name,
                filter_callable=filter_callable,
                **kwargs
            )
        return filter_formula_lambda

    def add_glyphs(self, **kwargs):
        return lambda new_mesh_name: glyphs.add_glyphs_to_mesh(
            self.plotter, new_mesh_name, mesh_name=self.name, **kwargs
        )

    def make_sure_parent_knows_about_me(self):
        if not self.is_filter_tree_root:
            parent_filter_formula = self.plotter.filter_formulas[
                self.parent_mesh_name
            ]
            if self.name not in parent_filter_formula.children:
                parent_filter_formula.children.append(self.name)

    def remove_child_relationship_to_all_parents(self):
        """
        Cause any meshes claiming this mesh as a child to forget that
        relationship, a prerequisite for deleting this mesh.

        Parameters
        ----------

        Returns
        -------
        None

        """
        for other_filter_formula in self.plotter.filter_formulas.values():
            if self.name in other_filter_formula.children:
                other_filter_formula.children.remove(self.name)

    @staticmethod
    def handle_filter_or_numpy(filter_result, parent_mesh, filter_name):
        if isinstance(filter_result, np.ndarray):
            filtered_mesh = parent_mesh.copy()
            filtered_mesh[filter_name] = filter_result
        elif isinstance(filter_result, pv.DataSet):
            filtered_mesh = filter_result
        else:
            filtered_mesh = None
        return filtered_mesh

    @staticmethod
    def string_to_filter(filter_string):
        """
        Convert string name of PyVista filter to a callable that produces that
        filter. If the argument is not a string, return the argument (for cases
        where the filter callable is passed rather than its string name).

        Parameters
        ----------
        filter_string : str or object

        Returns
        -------
        callable()
            Function mapping existing mesh to a new mesh via the named filter.

        object
            If `filter_string` is not a string, returns `filter_string`
        """

        if isinstance(filter_string, str):
            return lambda mesh: getattr(mesh, filter_string)
        else:
            return filter_string

    def update(self, update_actor=True, ovm_info=None, **filter_kwargs):
        """ Update a mesh by calling its creator filter """
        self.make_sure_parent_knows_about_me()
        ovm_utilities.copy_from_dict_recursively(
            filter_kwargs, self.filter_kwargs
        )
        if ovm_info is not None:
            ovm_utilities.copy_from_dict_recursively(
                ovm_info, self.ovm_info
            )
        try:
            filter_result = (
                self.filter_callable(self.parent_mesh)(**self.filter_kwargs)
            )
        # ignore calls to PyVista filters with incorrect types for filter_kwargs
        except (TypeError, ValueError, KeyError) as err:
            print(
                f"{err.__class__.__name__} "
                f"while updating mesh \'{self.name}\':"
            )
            print(err)
            if isinstance(err, TypeError):
                print(
                    f"Consider deleting bad keyword from "
                    f"{self.plotter.__class__.__name__}[\"{self.name}\"]"
                    f".filter_kwargs"
                )
            return None
        else:
            filtered_mesh = self.handle_filter_or_numpy(
                filter_result, self.parent_mesh, self.name
            )

            self.plotter.meshes[self.name] = filtered_mesh
            try:
                _ = filtered_mesh.n_points
            except AttributeError:
                pass
            if update_actor and self.has_actor:
                try:
                    self.update_actor()
                except ValueError as err:
                    print(f"{err.__class__.__name__} "
                          f"while updating actor visualizing"
                          f" mesh \'{self.name}\':")
                    print(err)

            for task in self.do_after_update:
                task(self)

            self.update_children()

        return filtered_mesh

    def get_auto_color(self):
        auto_color = self.plotter.actors_color_cycle.pop(0)
        self.plotter.actors_color_cycle.append(auto_color)
        return auto_color

    def _interpret_color_or_scalars(self, mesh_kwargs):
        """Check if mesh coloring should be updated as a solid color or
        color array.
        """

        updated_color = mesh_kwargs.get('color')
        updated_scalars = mesh_kwargs.get('scalars')
        if updated_color is not None:
            mesh_kwargs['scalars'] = None
            if updated_color == "auto":
                mesh_kwargs['color'] = self.get_auto_color()
        elif updated_scalars is not None:
            mesh_kwargs['color'] = None
        do_update_scalars = (mesh_kwargs.get('scalars') is not None)
        do_update_colors = (mesh_kwargs.get('colors') is not None)
        return do_update_colors, do_update_scalars, mesh_kwargs

    def _remove_bad_mesh_kwargs(self, mesh_kwargs):
        good_mesh_kwargs = dict()
        for key in mesh_kwargs.keys():
            if key in self._allowed_args_to_add_mesh:
                good_mesh_kwargs[key] = mesh_kwargs[key]
            else:
                print(
                    f"{key} is not a valid keyword argument to "
                    f"{self.plotter.__class__.__name__}.add_mesh; ignoring."
                )
        return good_mesh_kwargs

    def update_actor(self, **mesh_kwargs):
        """
        Update or create an actor by generating it anew based on its
        parent mesh.
        """
        do_update_color, do_update_scalars, mesh_kwargs = (
            self._interpret_color_or_scalars(mesh_kwargs)
        )
        mesh_kwargs = self._remove_bad_mesh_kwargs(mesh_kwargs)
        ovm_utilities.copy_from_dict_recursively(
            mesh_kwargs, self.mesh_kwargs
        )
        # Maintain actor's current visibility
        if self.name in self.plotter.renderer.actors.keys():
            visibility = self.actor.GetVisibility()
        else:
            # If actor is newly created, make it visible.
            visibility = 1

        actor_mesh_kwargs = self.mesh_kwargs
        # Copy any missing kwargs from defaults
        ovm_utilities.add_if_dict_lacks(
            actor_mesh_kwargs,
            self.plotter.settings["default_mesh_kwargs"]
        )
        scalar_bar_args = actor_mesh_kwargs['scalar_bar_args']

        # Make sure mesh's "name" is the same as actor name
        actor_mesh_kwargs["name"] = self.name

        # Update the color array if new scalars were passed
        if do_update_scalars:
            self._update_actor_color_array()

        # Otherwise, update solid color
        elif do_update_color:
            self._update_actor_solid_color()

        if self.colorbar is not None:
            # Record colorbar's current properties
            scalar_bars.copy_scalar_bar_properties(
                self.colorbar, scalar_bar_args
            )
            self.remove_colorbar()

        # If we will be making a colorbar...
        if (
            actor_mesh_kwargs['scalars'] is not None
            and actor_mesh_kwargs["show_scalar_bar"]
        ):
            do_adjust_scalar_bar = True
            self.give_colorbar_unique_title()
        else:
            do_adjust_scalar_bar = False
            defaults = self.plotter.settings['default_mesh_kwargs']
            scalar_bar_args['title'] = defaults['scalar_bar_args']['title']
        try:
            # Use Pyvista's "add_mesh()" to (re-)assign filtered_mesh,
            # visualized with mesh_kwargs, to actor_info.actor
            self.plotter.renderer.actors[self.name] = self.plotter.add_mesh(
                self.mesh, **actor_mesh_kwargs
            )
        except (TypeError, ValueError) as err:
            print(f"{err.__class__.__name__} while updating actor \'{self.name}\':")
            print(err)
        else:
            self.make_sure_parent_knows_about_me()

            # If we are coloring by scalars and mesh_kwargs['show_scalar_bar']
            # is True, then the scalar bar has been updated.
            if do_adjust_scalar_bar:
                scalar_bar_title = scalar_bar_args['title']
                this_scalar_bar = self.plotter.scalar_bars[scalar_bar_title]
                self.colorbar = this_scalar_bar
                scalar_bars.update_scalar_bar(
                    self.plotter, self.name, this_scalar_bar
                )

            # Set actor visibility as decided above
            self.plotter.set_actors_visibility(self.actor, visibility)
            return self.actor

    def set(self, **mesh_kwargs):
        return self.update_actor(**mesh_kwargs)

    def update_children(self):
        for child_name in self.children:
            if (
                child_name in self.plotter.mesh_names
                and child_name != self.name
            ):
                child_filter_formula = (
                    self.plotter.filter_formulas.get(child_name)
                )
                child_filter_formula.update()

    def remove_colorbar(self):
        if self.colorbar is not None:
            old_scalar_bar = self.colorbar
            try:
                old_scalar_bar_name = old_scalar_bar.GetTitle()
                self.plotter.remove_scalar_bar(old_scalar_bar_name)
            except KeyError:
                pass
            del self.colorbar
            self.colorbar = None

    def give_colorbar_unique_title(self):
        scalar_bar_args = self.mesh_kwargs["scalar_bar_args"]
        dataset_name = self.mesh_kwargs["scalars"]
        if "title" not in scalar_bar_args.keys():
            scalar_bar_args["title"] = ''
        if scalar_bar_args["title"] == '':
            scalar_bar_args["title"] = f"{dataset_name} ({self.name})"
        return scalar_bar_args["title"]

    def _update_actor_solid_color(self):
        actor_mesh_kwargs = self.mesh_kwargs
        actor_mesh_kwargs["scalars"] = None
        actor_mesh_kwargs["show_scalar_bar"] = False
        self.remove_colorbar()

    def _update_actor_color_array(self):
        actor_mesh_kwargs = self.mesh_kwargs
        actor_mesh_kwargs["color"] = None

        # make scalar bar visible whenever we change color to new scalars source
        actor_mesh_kwargs["show_scalar_bar"] = True
        if "scalar_bar_args" not in actor_mesh_kwargs.keys():
            # if we haven't made a scalar bar already for this actor,
            # use default settings
            actor_mesh_kwargs["scalar_bar_args"] = (
                self.plotter.settings["default_mesh_kwargs"]["scalar_bar_args"]
            )

        if self.colorbar is not None:
            old_scalar_bar = self.colorbar
            scalar_bar_args = actor_mesh_kwargs["scalar_bar_args"]

            # record scalar bar's properties, then remove it
            scalar_bars.copy_scalar_bar_properties(
                old_scalar_bar, scalar_bar_args
            )
            self.remove_colorbar()

    @property
    def is_glyphs(self):
        return "orient" in self.filter_kwargs.keys()

    def set_glyph_shape(self, new_shape, **geom_args):
        assert self.is_glyphs
        assert isinstance(new_shape, str)
        assert new_shape in self.plotter.geometric_objects.keys()
        self.filter_kwargs["geom"] = self.plotter.geometric_objects[
            new_shape
        ](**geom_args)
        self.ovm_info["glyph_shape"] = new_shape
        self.update()

    def set_glyphs_stride(self, new_stride):
        assert self.is_glyphs
        parent_filter_formula = self.plotter.get_filter_formula(
            self.parent_mesh_name
        )
        assert isinstance(new_stride, (int, float))
        assert "stride" in parent_filter_formula.filter_kwargs.keys()
        parent_filter_formula.update(stride=new_stride)

    def set_glyphs_scale(self, new_scale):
        assert self.is_glyphs
        assert isinstance(new_scale, (int, float))
        self.update(factor=new_scale)

    def get_centroids(
        self, num_centroids=10, ref_pt_idx=0, mesh_name_to_probe=None
    ):
        return sample.add_centroids_probe_filter_formula(
            self.plotter,
            parent_mesh_name=mesh_name_to_probe,
            find_centroids_of_mesh_name=self.name,
            num_centroids=num_centroids,
            ref_pt_idx=ref_pt_idx,
            sampled_mesh_name=None,
        )

    def get_circuits(
        self, mesh_to_probe_name=None, normals_name=None,
        radius=3, n_samples=100, use_ints=False
    ):
        return sample.add_circuit_probe_filter_formula(
            self.plotter,
            mesh_to_probe_name=mesh_to_probe_name,
            mesh_of_circuit_centers_name=self.name,
            normals_name=normals_name,
            radius=radius,
            n_samples=n_samples,
            use_ints=use_ints
        )

    def get_eigenvec(
        self, tensors_name=None, evec_idx=-1, new_dataset_name=None
    ):
        if tensors_name is None:
            tensors_name = self.plotter.symmetric_tensor_fields(
                mesh_name=self.name
            )[-1]
        if new_dataset_name is None:
            new_dataset_name = tensors_name + "_eigenvec"
        dataset = self.mesh[tensors_name]
        if dataset is not None:
            dataset = dataset.reshape(-1, 3, 3)
            evecs = np.linalg.eigh(dataset)[1]
            self.mesh[new_dataset_name] = evecs[:, :, evec_idx]
            return self.mesh[new_dataset_name]


class WidgetFormula:
    def __init__(
        self, plotter, name=None, mesh_name=None, enabled=True, callback=None,
        creator=None, configure=None, widget_args=None, **kwargs
    ):
        self.plotter = plotter
        if mesh_name is None:
            self.mesh_name = self.plotter.root_meshes[0]
        else:
            self.mesh_name = mesh_name
        if widget_args is None:
            self.widget_args = ()
        else:
            self.widget_args = widget_args
        if name is None:
            self.name = self.mesh_name + "_widget"
        else:
            self.name = name
        self.enabled = enabled

        # Function mapping (plotter, mesh_name) to
        # function(*widget_args, **widget_kwargs)
        # that updates mesh controlled by widget.
        self.callback = callback

        # name of method of self.plotter that creates the widget
        self.creator = creator

        # function that returns arguments of existing widget to use in updated
        # widget, or default arguments if no such widget exists
        self.configure = configure

        # kwargs passed to self.creator function
        self.widget_kwargs = kwargs
        if self.enabled:
            self.apply()
            self.set_color()

    @property
    def widget(self):
        return self.plotter.widgets.get(self.name)

    def get_creator_func(self):
        if isinstance(self.creator, str):
            creator_func = getattr(self.plotter, self.creator)
        else:
            creator_func = self.creator
        assert callable(creator_func)
        return creator_func

    def check_existing_widget_configuration(self):
        self.enabled, self.widget_args = self.configure(self.widget)

    def update(self):
        self.check_existing_widget_configuration()
        return self.callback(self.plotter, self.mesh_name)(*self.widget_args)

    def apply(self):
        creator_func = self.get_creator_func()
        if callable(self.configure):
            self.check_existing_widget_configuration()
        self.plotter.widgets[self.name] = creator_func(
            self.callback(self.plotter, self.mesh_name),
            *self.widget_args, **self.widget_kwargs
        )
        self.widget.SetEnabled(self.enabled)

        self.plotter.get_filter_formula(self.mesh_name).widget_name = self.name

    def get_widget(self):
        return self.plotter.get_widget(self.name)

    def set_color(self, *widget_color):
        widget = self.widget
        plotter_settings = self.plotter.settings
        if len(widget_color) == 0:
            widget_color = (plotter_settings["widget_outline_color"],)
        if len(widget_color) == 1:
            widget_color = widget_color[0]
            if widget_color[0] == "#":
                widget_color = np.array(
                    ovm_utilities.hex_to_RGB(widget_color)
                ) / 256
            elif isinstance(widget_color, str):
                widget_color = matplotlib.colors.to_rgb(widget_color)
        assert len(widget_color) in [3, 4]

        for attr_name in [
            "GetNormalProperty", "GetLineProperty", "GetOutlineProperty",
            "GetHandleProperty"
        ]:
            if hasattr(widget, attr_name):
                attr = getattr(widget, attr_name)
                property = attr()
                property.SetDiffuse(plotter_settings["widget_diffuse"])
                property.SetSpecular(plotter_settings["widget_specular"])
                property.SetSpecularPower(
                    plotter_settings["widget_specular_power"]
                )
                property.SetColor(widget_color)

    def set_outline_visibility(self, visibility):
        """Set visibility of a widget's box outline

        Parameters
        ----------
        visibility : int
            Make outline visible unless `visibility` equals 0.

        Returns
        -------
        None

        """
        attr_name = "GetOutlineProperty"
        if hasattr(self.widget, attr_name):
            property = getattr(self.widget, attr_name)()
            opacity = 0 if visibility == 0 else 1
            property.SetOpacity(opacity)

