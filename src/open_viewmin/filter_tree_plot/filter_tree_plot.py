""" Define class extending `pyvista.Plotter` with filter formulas and a
filters tree """

import pyvista as pv
import numpy as np
from time import sleep
import glob
from . import settings
from .utilities import mpi_stitch, ovm_utilities
from .filters import glyphs
from .filter_formulas import FilterFormula, WidgetFormula

# TODO: extend ViewMinPlot's isosurface sliders to FilterTreePlot as pyvista widgets


class FilterTreePlot(pv.Plotter):
    """
    Extend PyVista Plotter class to organize meshes into a "filter tree" so that
    updates to any mesh are automatically followed by updates to its child
    meshes and actors, recursively.

    Parameters
    ----------
    filenames : list[str], str, or None, optional
        Files to import.
    user_settings : dict or None, optional
        Customizations to default plotter settings.
    theme : ['dark' | 'document' | 'paraview' | None], optional
        PyVista plot theme
    reader : callable()
        Function to import and process data files
    dims : tuple of ints or None, optional
        System dimensions used if no filenames are given.
    data_stride : int, optional
        Spacing between grid sites used if no filenames are given.
    kwargs : dict or None

    """

    def __init__(
        self, filenames=None, user_settings=None, theme=None,
        reader=ovm_utilities.fast_import, dims=None, data_stride=1, **kwargs
    ):


        if filenames is None:
            filenames = []
        elif isinstance(filenames, str):
            filenames=[filenames]
        self.filenames = filenames
        if user_settings is None:
            user_settings = {}
        if 'off_screen' in kwargs.keys():
            if kwargs['off_screen']:
                pv.start_xvfb()
        if theme in ['paraview', 'document', 'dark']:
            pv.set_plot_theme(theme)
        else:
            pv.set_plot_theme('dark')
            # Themes: use ['dark' | 'document' | 'paraview' ]
            # for [black, white, gray] background
        self.initialize_plotter(**kwargs)
        self.is_enabled_eye_dome_lighting = kwargs.get(
            "enable_eye_dome_lighting", False
        )

        self._set_pyvista_plotter_properties()
        self.oVM_theme = settings.Hopkins_theme
        self._make_empty_convenience_arrays()
        self.shadowsEnabled = False
        self.data_stride = data_stride

        self.settings = settings.make_settings_dict(
            user_settings, self.theme, self.oVM_theme, self.off_screen
        )
        self.actors_color_cycle = self.settings["actors_color_cycle"].copy()
        if "background_color" in self.settings.keys():
            kwargs = {}
            if "background_top_color" in self.settings.keys():
                kwargs["top"] = self.settings["background_top_color"]
            self.set_background(self.settings["background_color"], **kwargs)

        if "text_color" in self.settings.keys():
            self.theme['font']['color'] = self.settings["text_color"]

        if len(filenames) > 0:
            self.load(filenames, reader=reader)
        elif dims is not None:
            self.Lx, self.Ly, self.Lz = dims
            self.create_fullmesh()
        # prepare glyph shapes
        self.geometric_objects = glyphs.make_geometric_objects_dict(
            self.settings
        )

    @property
    def root_meshes(self):
        return [
            mesh_name for mesh_name in self.mesh_names
            if self.get_filter_formula(mesh_name).parent_mesh_name == mesh_name
        ]

    @property
    def default_mesh_name(self):
        return self.root_meshes[0]

    @property
    def default_mesh(self):
        return self.get_mesh(self.default_mesh_name)

    def _set_pyvista_plotter_properties(self):
        """
        Initialization choices for PyVista Plotter

        Returns
        -------
        None

        """
        self.theme.antialiasing = True
        self.theme.smooth_shading = True
        self.theme.render_points_as_spheres = True
        self.fly_to_right_click_enabled = False

        # odefault to anaglyph for 3D stereo
        self.ren_win.SetStereoTypeToAnaglyph()

        # don't print VTK warnings
        self.ren_win.SetGlobalWarningDisplay(0)

        self.add_axes()

    def add_axes(self):
        # xyz axes arrows
        if not self.off_screen:
            self.axes = self.renderer.add_axes(
                interactive=(not self.off_screen),
                color=self.theme['font']['color']
            )

    def initialize_plotter(self, **kwargs):
        super().__init__(
            multi_samples=12,
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
            lighting='light kit',
            **kwargs
        )

    def __getitem__(self, mesh_name):
        """
        Emulate a dictionary with mesh names as keys.

        Parameters
        ----------
        mesh_name : str
            Name of mesh.

        Returns
        -------
        MeshObjRepresentation
            Object representing mesh

        """        
        return self.filter_formulas[mesh_name]

    def __setitem__(self, mesh_name, filter_formula_lambda):
        """
        Create a new filter by emulating behavior of a dictionary.

        """

        return filter_formula_lambda(mesh_name)

    def add_filter_formula(self, name=None, **kwargs):
        if name is not None:
            self.filter_formulas[name] = FilterFormula(
                self, name=name, **kwargs
            )
        return self.get_mesh(name)

    def add_widget_formula(self, name=None, **kwargs):
        if name is not None:
            self.widget_formulas[name] = WidgetFormula(
                self, name=name, **kwargs
            )
            return self.widget_formulas[name]

    def __repr__(self):
        """
        Represent plotter as a textual representation of its filters tree.

        Returns
        -------
        str
            Textual representation of filters tree.

        """

        def mesh_type_string(mesh_name):
            return self.meshes[mesh_name].__class__.__name__

        def get_filter_tree_string_component(mesh_name):
            if mesh_name in self.mesh_names:
                ret = f"  {mesh_name} ({mesh_type_string(mesh_name)})\n"
                for child in self.filter_formulas[mesh_name].children:
                    if child != mesh_name:
                        tree_component = get_filter_tree_string_component(child)
                        lines = tree_component.splitlines()
                        ret += "\n".join(["  " + line for line in lines]) + "\n"
                return ret

        ret = self.__class__.__name__ + "\n"
        for root in self.filter_tree_roots:
            ret += get_filter_tree_string_component(root) + "\n"
        return ret

    # file handling

    def load(
        self, filenames, reader=ovm_utilities.fast_import,
        fallback_reader=None, mpi_group=True
    ):
        """ Import a file or files.

        Parameters
        ----------
        filenames : str or list[str]
            Files to load.

        reader : callable, optional
            Function to read each file.

        fallback_reader : callable, optional
            Fallback function for reading each file if `reader` encounters an error.

        mpi_group : bool, optional
            Whether to stitch files together as separate outputs
            from the same timestep from an MPI run.

        """

        if type(filenames) is str:
            filenames = glob.glob(filenames)
        if mpi_group:
            filenames = mpi_stitch.mpi_group(filenames)
        try:
            first_frame_data = reader(filenames[0])
        except ValueError:  # detect whether file is from old Qmin
            if fallback_reader is not None:
                reader = fallback_reader
                first_frame_data = reader(filenames[0])
            else:
                raise ValueError(
                    f"No available reader knows how to import {filenames[0]}"
                )
        self.data.append(first_frame_data)  # reads data files into self.data
        for filename in filenames[1:]:
            self.data.append(reader(filename))
        self.data_stride = int(self.data[0][1, 0] - self.data[0][0, 0])
        self.Lx, self.Ly, self.Lz = [
            int(item+self.data_stride)
            for item in self.data[0][-1, :3]
        ]
        if len(self.data) == 0:
            raise ValueError('No data was imported.')

        # sets self.fullmesh and appends it to self.fullmeshes
        for file_data in self.data:
            self.create_fullmesh(file_data)

    def add_root_mesh(self, mesh_name):
        mesh_name = self.name_mesh_without_overwriting(mesh_name)
        self.add_filter_formula(
            name=mesh_name,
            parent_mesh_name=mesh_name,
            has_actor=False
        )
        return mesh_name

    def create_fullmesh(self, dat=None, mesh_name="fullmesh"):
        """
        Automatic processing of imported data, including creating of root mesh
        "fullmesh" and coordinates array.

        :param dat: Data array, with first three columns holding integer coordinates X, Y, Z.
        :type dat: numpy.ndarray
        """
        root_mesh_name = self.add_root_mesh(mesh_name)
        self.meshes[root_mesh_name] = pv.UniformGrid(
            dims=self.array_dims,
            spacing=(self.data_stride,)*3
        )
        self.fullmesh = self.meshes[root_mesh_name]

        root_mesh = self.get_mesh(root_mesh_name)
        root_mesh["ones"] = np.ones(len(root_mesh.points))
        # name the data columns (should be same in all frames)
        if dat is None:
            self.coords = np.asarray(root_mesh.points, dtype=int)
        else:
            self.coords = np.asarray(dat[:, :3], dtype=int)
        return root_mesh_name

    def update_filter(self, mesh_name, update_actor=False, **kwargs):
        filter_formula = self.filter_formulas.get(mesh_name)
        if filter_formula is not None:
            filter_formula.update(update_actor=update_actor, **kwargs)

    def update_actor(self, actor_name, **kwargs):
        filter_formula = self.filter_formulas.get(actor_name)
        if filter_formula is not None:
            if filter_formula.has_actor:
                filter_formula.set(**kwargs)

    # frame handling / animation

    def load_frame(self, frame_num=0):
        """ Use one of the (already imported) sets of data, as the source for
        all PyVista datasets, e.g. for generating a frame of an animation from
        timeseries data.

        Parameters
        ----------

        frame_num : int, optional
            Index in `self.data` of the timeframe to display.
        """

        camera_dist = self.camera.GetDistance()
        if self.frame_num >= self.num_frames:
            self.frame_num = self.num_frames - 1
        elif self.frame_num < 0:
            self.frame_num = 0

        if self.num_frames > 0:
            self.fullmesh = self.get_mesh(self.root_meshes[self.frame_num])

        for widget_formula in self.widget_formulas.values():
            widget_formula.apply()
        self.camera.SetDistance(camera_dist)

    def next_frame(self):
        """ Load next frame in data timeseries. """
        if self.frame_num < self.num_frames-1:
            self.frame_num += 1
            self.load_frame()

    def previous_frame(self):
        """ Load previous frame in data timeseries. """
        if self.frame_num > 0:
            self.frame_num -= 1
            self.load_frame()

    def first_frame(self):
        """ Load first frame in data timeseries. """
        if self.frame_num > 0:
            self.frame_num = 0
            self.load_frame()

    def last_frame(self):
        """ Load last frame in data timeseries. """
        num_frames = self.num_frames
        if self.frame_num < num_frames-1:
            self.frame_num = num_frames-1
            self.load_frame()

    @property
    def num_frames(self):
        """ Number of frames in timeseries data. """
        return len(self.root_meshes)

    def play(self, pause_time=0.5):
        """ In-GUI animation; will be slow because it loads and renders on the fly """
        while self.frame_num < self.num_frames-1:
            self.next_frame()
            self.update_frame_spinbox()
            self.repaint()
            sleep(pause_time)

    # general utilities

    @property
    def colorbars(self):
        colorbars = dict()
        for mesh_name in self.mesh_names:
            filter_formula = self.get_filter_formula(mesh_name)
            cbar = filter_formula.colorbar
            if cbar is not None:
                colorbars[mesh_name] = cbar
        return colorbars

    @property
    def colorbar_titles(self):
        """
        Get titles of all scalar bars

        Returns
        -------
        list[str]
            List of scalar bar titles
        """

        return [
            scalar_bar.GetTitle()
            for scalar_bar in self.colorbars.values()
        ]

    @property
    def points(self):
        """ Get array of coordinates belonging to root mesh "fullmesh"

        Returns
        -------
        numpy.ndarray
            Array of point coordinates.
        """

        return self.fullmesh.points

    def unique_array_names(self, mesh=None):
        """
        Get array names for datasets attached to a mesh, deleting duplicates.

        Parameters
        ----------
        mesh : str or pyvista.DataSet
            PyVista mesh or name of mesh

        Returns
        -------
        (pyvista.DataSet, list[str])
            Mesh along with the list of unique array names.

        """

        if mesh is None:
            if len(self.root_meshes) > 0:
                mesh_name = self.root_meshes[0]
                mesh = self.get_mesh(mesh_name)
        elif isinstance(mesh, str):
            mesh_name = mesh
            mesh = self.get_mesh(mesh_name)
        if mesh is None:
            return None, []

        # get unique array names
        arr_names = mesh.array_names
        unique_names = list(set(arr_names))  # delete duplicates

        # if we've deleted any duplicates
        if len(unique_names) < len(arr_names):
            for arr_name in mesh.cell_data.keys():
                # check if any cell array has the same name as any point array
                # and rename it if so
                if arr_name in mesh.point_data.keys():
                    mesh.rename_array(
                        arr_name,
                        arr_name + " (cells)",
                        preference="Cells"
                    )
        return mesh, mesh.array_names

    # array/dataset utilities

    def scalar_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with one value at each site.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """

        mesh_name, array_names = self.unique_array_names(mesh=mesh_name)
        ret = []
        for array_name in array_names:
            array_shape = mesh_name[array_name].shape
            if len(array_shape) == 1:
                ret.append(array_name)
        return sorted(ret)

    def vector_fields(self, mesh="fullmesh"):
        """
        List names of datasets with three values at each site.

        Parameters
        ----------
        mesh : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """

        mesh, array_names = self.unique_array_names(mesh=mesh)
        ret = []
        for array_name in array_names:
            array_shape = mesh[array_name].shape
            if len(array_shape) > 1:
                if array_shape[1] == 3:
                    ret.append(array_name)
        return sorted(ret)

    def tensor_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with nine values at each site.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """
        mesh = self.get_mesh(mesh_name)
        _, array_names = self.unique_array_names(mesh=mesh)
        ret = []
        for array_name in array_names:
            array_shape = mesh[array_name].shape
            if len(array_shape) > 1:
                if array_shape[1] == 9:
                    ret.append(array_name)
        return sorted(ret)

    def symmetric_tensor_fields(self, mesh_name="fullmesh"):
        """
        List names of datasets with nine values at each site, whose corresponding
        3x3 matrix is everywhere symmetric.

        Parameters
        ----------
        mesh_name : str or pyvista.DataSet
            PyVista mesh or its name.

        Returns
        -------
        list[str]
            List of array names.

        """
        ret = []
        for field_name in self.tensor_fields(mesh_name=mesh_name):
            field = self.get_mesh(mesh_name)[field_name]
            tmp = field.reshape(-1, 3, 3)
            if np.array_equal(np.swapaxes(tmp, -1, -2), tmp):
                ret.append(field_name)
        return ret

    @property
    def dims(self):
        """ System linear dimensions.

        Returns
        -------
        tuple(int)
            `(Lx, Ly, Lz)`
        """
        return self.Lx, self.Ly, self.Lz

    @property
    def array_dims(self):
        """ System array-size dimensions.

        Returns
        -------
        tuple(int)
            `(array_Lx, array_Ly, array_Lz)`

        """
        return tuple([
            int(item/self.data_stride)
            for item in self.dims
        ])

    # utilities for working with actors and meshes

    @property
    def mesh_names(self):
        """
        Names of all meshes in filters tree.

        Returns
        -------
        list[str]
            List of mesh names.
        """
        return list(self.meshes.keys())

    @property
    def actor_names(self):
        """
        Names of all actors in filters tree.

        Returns
        -------
        list[str]
            List of actor names.

        """

        return [
            mesh_name for mesh_name in self.mesh_names
            if self.filter_formulas[mesh_name].has_actor
        ]

    @property
    def non_filter_tree_actor_names(self):
        """
        List actors that don't belong to open_viewmin's filter tree.

        Returns
        -------
        list[str]
            List of actor names.

        """
        ovm_actor_names = self.actor_names
        return [
            key for key in self.renderer.actors.keys()
            if key not in ovm_actor_names
        ]

    def get_mesh_name(self, mesh):
        """
        Get the name of a given mesh.

        Parameters
        ----------
        mesh : pyvista.DataSet

        Returns
        -------
        str
            name of the mesh

        """

        ans_list = [
            mesh_name
            for (mesh_name, mesh_b) in [
                (mesh_name, self.get_mesh(mesh_name))
                for mesh_name in self.mesh_names
            ]
            if mesh_b is mesh
        ]
        if len(ans_list) > 0:
            return ans_list[0]
        else:
            raise KeyError(f"Cannot find mesh in filter tree")

    def get_grandparent_mesh_name(self, actor_or_mesh_name):
        """
        Get the name of the parent of the parent of a given actor or mesh.

        Parameters
        ----------
        actor_or_mesh_name : str
            Name of actor of mesh

        Returns
        -------
        str
            Name of grandparent mesh.

        """

        filter_formula = self.get_filter_formula(actor_or_mesh_name)
        parent_mesh_name = filter_formula.parent_mesh_name
        parent_mesh_formula = self.get_filter_formula(parent_mesh_name)
        grandparent_mesh_name = parent_mesh_formula.parent_mesh_name

        return grandparent_mesh_name

    def get_actor_name(self, actor):
        """
        Given an actor, return its name.

        Parameters
        ----------
        actor : vtkOpenGLActor
            PyVista actor.

        Returns
        -------
        str
            Name of actor.

        """

        for key in self.renderer.actors.keys():
            if self.renderer.actors[key] is actor:
                return key

    def get_mesh(self, mesh_name):
        """
        Return mesh with a given name, if it exists.
        For non-mesh actors, return parent mesh.

        Parameters
        ----------
        mesh_name : str

        Returns
        -------
        pyvista.DataSet
            PyVista mesh

        """

        if mesh_name is None:
            mesh_name = self.default_mesh_name
        elif isinstance(mesh_name, pv.DataSet):
            # in case the mesh itself was passed, just return that
            return mesh_name
        return self.meshes.get(mesh_name)

    def get_actor(self, actor_name):
        """
        Return actor with a given name, if it exists.

        Parameters
        ----------
        actor_name : str
            Name of actor.

        Returns
        -------
        vtkmodules.vtkRenderingOpenGL2.vtkOpenGLActor
            PyVista actor.

        """

        if isinstance(actor_name, str):
            try:
                return self.renderer.actors[actor_name]
            except KeyError as err:
                print(err)
                return
        else:
            # if passed an actor rather than its name, return the actor
            return actor_name

    def get_widget(self, widget_name):
        return self.widgets.get(widget_name)

    def get_filter_formula(self, actor_name):
        """
        Get the formula needed to re-create an actor from its parent mesh.

        Parameters
        ----------
        actor_name : str
            Name of the actor.

        Returns
        -------
        types.SimpleNamespace
            Actor recipe.
        """
        if actor_name is None:
            actor_name = self.default_mesh_name
        return self.filter_formulas.get(actor_name)

    def get_widget_formula(self, widget_name):
        return self.widget_formulas.get(widget_name)

    def copy_all_actors(self, to_plotter):
        """
        Copy actors to another plotter,
        such as for displaying in Jupyter notebooks.

        Parameters
        ----------
        to_plotter : pyvista.Plotter

        Returns
        -------
        None

        """

        for actor_name in self.renderer.actors.keys():
            actor_is_visible = self.renderer.actors[actor_name].GetVisibility()
            if actor_is_visible:
                if actor_name in self.mesh_names:
                    filter_formula = self.get_filter_formula(actor_name)
                    parent_mesh = self.get_mesh(filter_formula.parent_mesh_name)
                    pvfilter = filter_formula.filter_callable(parent_mesh)
                    filter_kwargs = filter_formula.filter_kwargs
                    mesh_kwargs = filter_formula.mesh_kwargs
                    if to_plotter.theme.jupyter_backend == 'panel':
                        mesh_kwargs['diffuse'] = 1
                    actor = pvfilter(**filter_kwargs)
                    to_plotter.add_mesh(actor, **mesh_kwargs)
                else:
                    actor = self.renderer.actors[actor_name]
                    to_plotter.add_actor(actor)

    def extract_actors(self, jupyter_backend='pythreejs'):
        """
        Set up a new PyVista plotter with a copy of all this plotter's actors.

        Parameters
        ----------
        jupyter_backend : str, optional
            PyVista plotting backend. See https://docs.pyvista.org/api/plotting/_autosummary/pyvista.themes.DefaultTheme.jupyter_backend.html.


        Returns
        -------
        pyvista.Plotter
            New PyVista plotter containing this plotter's actors.

        """
        to_plotter = pv.Plotter()
        to_plotter.theme = self.theme
        to_plotter.set_background(self.background_color)
        to_plotter.theme.jupyter_backend = jupyter_backend
        self.copy_all_actors(to_plotter)
        return to_plotter

    # convenience objects and methods

    def _make_empty_convenience_arrays(self):
        """
        Initialize arrays to hold data and filters tree information.

        Returns
        -------
        None
        """

        self.data = []
        self.meshes = dict()
        self.filter_formulas = dict()
        self.widgets = dict()
        self.widget_formulas = dict()

    # operations on meshes and actors

    def rename_mesh(self, from_name, to_name, do_refresh=True):
        """
        Copy a mesh in the filters tree by copying its recipe.

        Parameters
        ----------
        from_name : str
            Name of mesh to copy.
        to_name : str
            Name of new mesh.
        do_refresh : bool, optional
            Whether to create the mesh anew from its filter after renaming.

        Returns
        -------
        None

        """

        # if mesh has actor currently, remove it
        if from_name in self.actor_names:
            self.remove_actor(from_name)

        filter_formula = self.get_filter_formula(from_name)

        # inform children about change of name
        for child_name in filter_formula.children:
            child_filter_formula = self.get_filter_formula(child_name)
            if child_filter_formula.parent_mesh_name == from_name:
                child_filter_formula.parent_mesh_name = to_name

        # inform parents about change of name
        filter_formula.remove_child_relationship_to_all_parents()

        # rename filter formula
        filter_formula.name = to_name
        self.filter_formulas[to_name] = filter_formula
        del self.filter_formulas[from_name]

        # rename mesh
        self.meshes[to_name] = self.meshes[from_name]
        del self.meshes[from_name]

        # rename widget if applicable
        old_widget_name = filter_formula.widget_name
        if old_widget_name is not None:
            widget = self.get_widget(old_widget_name)

            # remember widget state (enabled/disabled)
            widget_enabled = widget.GetEnabled()

            # disable the widget
            widget.SetEnabled(0)

            widget_formula = self.get_widget_formula(old_widget_name)

            # rename widget formula
            new_widget_name = to_name + "_widget"
            self.widget_formulas[new_widget_name] = widget_formula
            widget_formula.name = new_widget_name
            del self.widget_formulas[old_widget_name]

            # delete old widget from widgets dictionary
            del self.widgets[old_widget_name]

            # widget formula's mesh name should match new mesh name
            widget_formula.mesh_name = to_name

            # re-apply widget formula
            widget_formula.apply()

            # inform filter formula about new widget name
            filter_formula.widget_name = new_widget_name

            # set widget to previous enabled/disabled state
            self.get_widget(new_widget_name).SetEnabled(widget_enabled)

        # make sure change of actor name is recorded in self.renderer.actors
        # if applicable
        if from_name in self.renderer.actors.keys():
            self.renderer.actors[to_name] = self.renderer.actors[from_name]
            del self.renderer.actors[from_name]

        filter_formula.make_sure_parent_knows_about_me()

        if do_refresh:
            self.refresh()

    def remove_mesh_completely(self, mesh_name, remove_children=True):
        """
        Remove a mesh (filter or actor) from the plotter and from the filters
        tree.

        Parameters
        ----------
        mesh_name : str
            Name of mesh to remove.

        remove_children : bool, optional
            Whether to recursively remove all meshes descended from this mesh
            in the filters tree.

        Returns
        -------
        None

        """

        filter_formula = self.filter_formulas[mesh_name]
        colorbar = filter_formula.colorbar
        children = filter_formula.children.copy()
        if colorbar is not None:
            self.remove_scalar_bar(colorbar.GetTitle())
        if mesh_name in self.actor_names:
            self.remove_actor(mesh_name)
        filter_formula.remove_child_relationship_to_all_parents()
        del self.filter_formulas[mesh_name]
        del self.meshes[mesh_name]
        if mesh_name in self.toolbars.keys():
            self.toolbars[mesh_name].hide()
            del self.toolbars[mesh_name]
        if remove_children:
            for child_name in children:
                self.remove_mesh_completely(child_name, remove_children=True)

    # actor/filter creation / update

    def add_filter(
        self, parent_mesh_name="fullmesh", child_name=None, creator_func=None,
        filter_kwargs=None, actor=True, **mesh_kwargs
    ):
        """
        Add a new filter to the filters tree.

        Parameters
        ----------
        parent_mesh_name : str, optional
            Name of mesh to which to apply the filter.
        child_name : str, optional
            Name of mesh to be created by filter.
        creator_func : callable() or str, optional
            Function mapping a parent mesh to its filter callable;
            or the filter name as a string (for filters defined by
            PyVista). Defaults to the identity mapping.
        filter_kwargs : dict or None, optional
            Keyword arguments to the filter defined by `creator_func`
        actor : bool, optional
            Whether to treat the child mesh as an actor with visualization
            properties.
        mesh_kwargs : dict or None, optional
            Keyword arguments to `pyvista.Plotter.add_mesh` for controlling
            visualization properties. Used only if `actor==True`.

        Returns
        -------
        pyvista.Dataset or vtkOpenGLActor
            Mesh or actor object.

        """

        if filter_kwargs is None:
            filter_kwargs = dict()
        if type(creator_func) is str:
            creator_string = creator_func
            creator_func = (lambda mesh: getattr(mesh, creator_string))
        else:
            creator_string = "child"
        if child_name is None:
            child_name = parent_mesh_name + "_" + creator_string
        if creator_func is None:
            def creator_func(mesh):
                return lambda: mesh

        self.add_filter_formula(
            name=child_name,
            parent_mesh_name=parent_mesh_name,
            filter_callable=creator_func,
            filter_kwargs=filter_kwargs,
            mesh_kwargs=mesh_kwargs,
            has_actor=actor
        )

    @property
    def filter_tree_leaves(self):
        leaves = []
        for mesh_name in self.mesh_names:
            mesh_info = self.filter_formulas[mesh_name]
            if len(mesh_info.children) == 0:
                leaves.append(mesh_name)
        return leaves

    @property
    def filter_tree_roots(self):
        roots = []
        for mesh_name in self.mesh_names:
            mesh_info = self.filter_formulas[mesh_name]
            if mesh_info.parent_mesh_name == mesh_name:
                roots.append(mesh_name)
        return roots

    def name_mesh_without_overwriting(self, mesh_name):
        return ovm_utilities.name_without_overwriting(mesh_name, self.meshes)

    def name_widget_without_overwriting(self, widget_name):
        return ovm_utilities.name_without_overwriting(
            widget_name, self.widgets
        )

    def refresh(self):
        for mesh_name in self.filter_tree_roots:
            self.update_filter(mesh_name)

    def set_actors_visibility(self, actor_or_actors, visibility):
        if type(actor_or_actors) is not list:
            actor_or_actors = [actor_or_actors]
        for actor in actor_or_actors:
            if isinstance(actor, str):
                actor_name = actor
                actor = self.get_actor(actor_name)
            else:
                actor_name = self.get_actor_name(actor)
            try:
                actor.SetVisibility(visibility)
            except AttributeError:
                pass
            else:
                filter_formula = self.get_filter_formula(actor_name)
                if filter_formula is not None:
                    colorbar = filter_formula.colorbar
                    if colorbar is not None:
                        if not visibility:
                            colorbar.SetVisibility(0)
                        else:
                            mesh_kwargs = filter_formula.mesh_kwargs
                            show_scalar_bar = mesh_kwargs['show_scalar_bar']
                            colorbar.SetVisibility(int(show_scalar_bar))

    def toggle_filter_tree_actor_visibility(self, actor_name):
        actor = self.get_actor(actor_name)
        current_visibility = actor.GetVisibility()
        new_visibility = 1 - current_visibility
        self.set_actors_visibility(actor_name, new_visibility)

    def enable_eye_dome_lighting(self):
        self.is_enabled_eye_dome_lighting = True
        super().enable_eye_dome_lighting()

    def disable_eye_dome_lighting(self):
        self.is_enabled_eye_dome_lighting = False
        super().disable_eye_dome_lighting()

    def do_orbit(self):
        self.orbit_on_path(
            path=self.orbit_path, threaded=True,
            step=self.orbit_kwargs['step']
        )

    def show_orbit(self):
        self.add_mesh(self.orbit_path.points, name='orbit')

    def hide_orbit(self):
        self.remove_actor("orbit")

    def toggle_orbit_visibility(self, toggle=True):
        if "orbit" in self.renderer.actors.keys():
            vis = self.renderer.actors["orbit"].GetVisibility()
        else:
            vis = False
        if toggle:
            vis = not vis
        if vis:
            self.show_orbit()
        else:
            self.hide_orbit()

    def toggle_last_colorbar(self):
        colorbars = list(self.colorbars.values())
        if len(colorbars) > 0:
            colorbar = colorbars[-1]
            colorbar.SetVisibility(1-colorbar.GetVisibility())

    def set_lights_intensity(self, intensity):
        for light in self.renderer.lights:
            light.SetIntensity(intensity)

    def toggle_floor(self):
        key = 'Floor(-z)'
        if key in self.renderer.actors.keys():
            floor_actor = self.renderer.actors[key]
            floor_actor.SetVisibility(1-floor_actor.GetVisibility())
        else:
            self.add_floor()

    def toggle_stereo_render(self):
        self.ren_win.SetStereoRender(
            1-self.ren_win.GetStereoRender()
        )

    def enable_shadows(self):
        super().enable_shadows()
        self.shadowsEnabled = True

    def disable_shows(self):
        super().disable_shadows()
        self.shadowsEnabled = False

    def toggle_shadows(self):
        if self.shadowsEnabled:
            self.disable_shadows()
        else:
            self.enable_shadows()

    def hide_all_meshes(self):
        for mesh_name in self.mesh_names:
            filter_formula = self.get_filter_formula(mesh_name)
            if filter_formula.has_actor:
                self.set_actors_visibility(mesh_name, 0)
        self.refresh()

    def hide_all_actors(self):
        all_actors =self.renderer.actors
        for actor_name in all_actors.keys():
            if actor_name in self.actor_names:
                self.set_actors_visibility(actor_name, 0)
            else:
                all_actors[actor_name].SetVisibility(0)
        self.refresh()

    def find_actor_for_scalar_bar(self, scalar_bar):
        for actor_name in self.actor_names:
            filter_formula = self.get_filter_formula(actor_name)
            if filter_formula.colorbar is scalar_bar:
                return actor_name

    def toggle_scalar_bar_visibility(self, scalar_bar_actor):
        new_visibility = self.toggle_actor_visibility(scalar_bar_actor)
        parent_actor_name = self.find_actor_for_scalar_bar(scalar_bar_actor)
        filter_formula = self.get_filter_formula(parent_actor_name)
        mesh_kwargs = filter_formula.mesh_kwargs
        mesh_kwargs['show_scalar_bar'] = new_visibility

    @staticmethod
    def toggle_actor_visibility(actor):
        current_visibility = actor.GetVisibility()
        new_visibility = 1 - current_visibility
        actor.SetVisibility(new_visibility)
        return new_visibility
