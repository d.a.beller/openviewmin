""" Define topological calculations for ViewMinPlot meshes """

import numpy as np
import pyvista as pv
from ..utilities import ovm_utilities


def get_connected_sets(plotter, mesh, pbc=False):
    mesh_connectivity = mesh.connectivity()
    region_id = mesh_connectivity.active_scalars  # gives connectivity's RegionID
    if pbc:
        pbc_dist_thresh = 1.1  # threshold distance for joining meshes
        nr = max(region_id) + 1  # number of RegionIDs
        ## Find points within threshold distance of each boundary
        bdy_pts = [[[] for _ in range(3)] for __ in range(nr)]
        for r in range(nr):
            pts = mesh_connectivity.points[region_id == r]
            for i in range(3):
                bdy_pts[r][i] = [
                    pts[pts[:, i] <= plotter.data_stride * pbc_dist_thresh],
                    pts[
                        pts[:, i] >= plotter.dims[i] - (
                                plotter.data_stride * (1 + pbc_dist_thresh)
                        )
                        ]
                ]

        # Check point-pairwise for RegionIDs that should be joined.
        # Begin with trivial plotter-equivalence.
        same_as = [[r] for r in range(nr)]
        for r1 in range(nr):
            for r2 in range(nr):
                if r2 != r1:
                    for i in range(3):
                        pts1_shifted = (
                                bdy_pts[r1][i][0]
                                + np.eye(3)[i] * plotter.dims[i]
                        )
                        pts2 = bdy_pts[r2][i][1]
                        if len(pts1_shifted) > 0 and len(pts2) > 0:
                            if np.min(
                                    [
                                        np.sum(
                                            (pts1_shifted - pt2) ** 2,
                                            axis=-1
                                        )
                                        for pt2 in bdy_pts[r2][i][1]
                                    ]
                            ) <= pbc_dist_thresh ** 2:
                                same_as[r1].append(r2)
                                same_as[r2].append(r1)
                                break

        # Replace each RegionID with the smallest equivalent one
        true_rid = np.arange(nr)
        r = nr - 1
        while r >= 0:
            min_r = min([min(row) for row in same_as if r in row])
            true_rid[true_rid == r] = min_r
            r -= 1
        # Shift remaining RegionID values to be consecutive
        true_rid2 = [list(np.unique(true_rid)).index(i) for i in true_rid]

        # replace RegionID at each point accordingly
        true_rid_array = np.empty_like(np.array(region_id))
        for r1, r2 in enumerate(true_rid2):
            true_rid_array[region_id == r1] = r2
        region_id = true_rid_array

    mesh_connectivity['connected'] = region_id
    return mesh_connectivity



def get_connected_subsets(plotter, mesh, pbc=False):
    """ Use connectivity filter to produce a list of connected subset meshes """
    connectivity_mesh = get_connected_sets(plotter, mesh, pbc=pbc)
    connectivity_array = connectivity_mesh["connected"]

    # mc = mesh.connectivity()
    # mc['connected'] = mc.active_scalars
    return [
        connectivity_mesh.threshold(
            scalars='connected',
            value=[c - 0.1, c + 0.1]
        )
        for c in range(1 + max(connectivity_array))
    ]

def add_connected_sets(plotter, parent_mesh_name, pbc=False):
    child_mesh_name = plotter.name_mesh_without_overwriting(
            parent_mesh_name + "_connected"
        )
    def connected_sets_filter_creator(mesh):
        return lambda: get_connected_sets(plotter, mesh, pbc=pbc)
    mesh_kwargs = dict(
        scalars="connected",
        categories=True,
        cmap=plotter.settings["categories_colormap"],
        scalar_bar_args={
            **plotter.settings["default_mesh_kwargs"]["scalar_bar_args"],
            "interactive":True,
            "fmt":"%.0f"
        }
    )

    plotter.add_filter_formula(
        name=child_mesh_name,
        parent_mesh_name=parent_mesh_name,
        filter_callable=connected_sets_filter_creator,
        mesh_kwargs=mesh_kwargs
    )
    return plotter.get_mesh(child_mesh_name)

def merge_all(list_of_meshes):
    """ Merge several meshes """
    if len(list_of_meshes) > 0:
        ret = list_of_meshes[0]
        for mesh in list_of_meshes[1:]:
            ret = ret.merge(mesh)
        return ret


def contour_periodic_extension(plotter, dataset_name, value):
    """
    Make a contour surface that extends into periodic images of the box
    (in as many directions as are relevant)
    """
    S = plotter.fullmesh[dataset_name].reshape(plotter.dims)
    for i in range(3):
        S = np.concatenate((S, S, S), axis=i)
    # make periodic images of fullmesh containing only the dataset of interest
    fmpbc = pv.UniformGrid(
        dims=[item * 3 for item in plotter.array_dims],
        spacing=(plotter.data_stride,) * 3
    )
    fmpbc['S'] = S.flatten()
    # create contour surface in extended space
    clip_box_bounds = [
        plotter.Lx / 2, 5 * plotter.Lx / 2,
        plotter.Ly / 2, 5 * plotter.Ly / 2,
        plotter.Lz / 2, 5 * plotter.Lz / 2
    ],
    defects_pbc = fmpbc.contour(
        scalars='S', isosurfaces=[value], compute_normals=True
    )
    defects_pbc_clipped = defects_pbc.clip_box(
        bounds=clip_box_bounds, invert=False,
    )

    # re-center
    defects_pbc_clipped.translate(-np.array(plotter.dims))
    defects_pbc_connected_subsets = get_connected_subsets(
        plotter, defects_pbc_clipped
    )
    # keep only connected subsets with center of mass in the original box
    connected_subsets_centers = np.array([
        np.average(item.points, axis=0)
        for item in defects_pbc_connected_subsets
    ])
    keepers = [
        connected_subsets_centers[i]
        for i in np.argwhere(
            np.prod(
                (
                    (connected_subsets_centers < plotter.dims)
                    * (connected_subsets_centers > 0)
                ),
                axis=-1
            )
        ).flatten()
    ]
    return merge_all(keepers)


def calculate_Euler_characteristic(plotter, mesh, keep_chi=None):
    if type(mesh) is str:
        mesh = plotter.get_mesh(mesh)
    mcs = get_connected_subsets(plotter, mesh)
    for mc in mcs:
        chi = mc.n_points - mc.extract_all_edges().n_cells + mc.n_cells
        mc['chi'] = chi * np.ones(mc.n_points, dtype=int)
    if keep_chi is not None:
        if type(keep_chi) is not list:
            keep_chi = [keep_chi]
        mcs = [mc for mc in mcs if mc['chi'][0] in keep_chi]
    return merge_all(mcs)


def add_Euler_characteristic_filter(
        plotter, mesh_name, new_mesh_name=None, keep_chi=None, **mesh_kwargs
):
    if new_mesh_name is None:
        new_mesh_name = mesh_name + "_chi"

    def filter_callback(mesh):
        return lambda: calculate_Euler_characteristic(
            plotter,
            mesh,
            keep_chi=keep_chi
        )

    use_mesh_kwargs = dict(
        categories=True,
        cmap=plotter.settings["categories_colormap"],
        scalars="chi"
    )

    ovm_utilities.copy_from_dict_recursively(mesh_kwargs, use_mesh_kwargs)

    return plotter.add_filter_formula(
        name=new_mesh_name,
        parent_mesh_name=mesh_name,
        filter_callable=filter_callback,
        mesh_kwargs=use_mesh_kwargs,
    )


def add_find_loops_filter(
        plotter, mesh_name, new_mesh_name=None, **mesh_kwargs
):
    if new_mesh_name is None:
        new_mesh_name = mesh_name + "_loops"
    return add_Euler_characteristic_filter(
        plotter,
        mesh_name,
        new_mesh_name,
        keep_chi=0,
        **mesh_kwargs
    )
