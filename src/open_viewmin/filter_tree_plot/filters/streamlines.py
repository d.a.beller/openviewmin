""" Utilities for creating streamlines for orientation data on surfaces """

import numpy as np
import pyvista as pv


def add_surface_streamlines_aux(mesh):
    def return_func(**kwargs):
        src = mesh
        if "n_points" in kwargs.keys():
            n_points = kwargs['n_points']
            if 0 < n_points < mesh.n_points:
                pts = mesh.points
                pts = pts[np.random.choice(
                    len(pts), size=n_points, replace=False
                )]
                src = pv.wrap(pts)

            # don't pass this kwarg to streamlines_from_source
            del kwargs['n_points']

        tube = False
        tube_kwargs = dict()
        if "tube" in kwargs.keys():
            tube = kwargs['tube']
            del kwargs['tube']
        for key in ["radius", "n_sides"]:
            if key in kwargs.keys():
                tube_kwargs[key] = kwargs[key]
                del kwargs[key]
        ret = mesh.streamlines_from_source(src, **kwargs)
        if tube:
            ret = ret.tube(**tube_kwargs)
        return ret

    return return_func


def add_streamlines_to_surface(
    plotter, actor_name, mesh_name,
    vectors_name=None, max_steps=10, n_points=0,
    tube=True, radius=0.1, n_sides=3,
    **mesh_kwargs
):
    if vectors_name is None:
        mesh = plotter.get_mesh(mesh_name)
        vectors_name = mesh.active_vectors_info.name

    filter_kwargs = dict(
        surface_streamlines=True,
        vectors=vectors_name,
        max_steps=max_steps,
        n_points=n_points,
        tube=tube,
        radius=radius,
        n_sides=n_sides
    )
    plotter.add_filter_formula(
        name=actor_name,
        parent_mesh_name=mesh_name,
        filter_callable=add_surface_streamlines_aux,
        filter_kwargs=filter_kwargs,
        mesh_kwargs=mesh_kwargs
    )

