""" Utilities for creating and analyzing glyphs """

import pyvista as pv
import numpy as np
from . import sample


def add_glyphs_to_mesh(
    plotter, actor_name, mesh_name=None, glyph_shape=None,
    glyph_stride=None, glyph_kwargs=None,
    orient=None, scale=None, factor=None, random=True, **mesh_kwargs
):

    ovm_info = dict()
    if glyph_kwargs is None:
        glyph_kwargs = dict()

    if glyph_stride is None:
        glyph_stride = plotter.settings["glyph_stride"]

    if factor is None:
        factor = glyph_stride

    if glyph_shape is None:
        glyph_shape = plotter.settings["glyph_shape"]
    if type(glyph_shape) is str:
        ovm_info["glyph_shape"] = glyph_shape
        if glyph_shape in plotter.geometric_objects.keys():
            glyph_shape = plotter.geometric_objects[glyph_shape]()
    glyph_kwargs["geom"] = glyph_shape

    if orient is not None:
        glyph_kwargs["orient"] = orient

    if scale is not None:
        glyph_kwargs["scale"] = scale

    glyph_kwargs["factor"] = factor

    glyph_kwargs["tolerance"] = None  # forbid interpolation

    sampled_mesh_name = sample.make_sampled_mesh(
        plotter, mesh_name, glyph_stride, random=random
    )

    plotter.add_filter_formula(
        name=actor_name,
        parent_mesh_name=sampled_mesh_name,
        filter_callable="glyph",
        filter_kwargs=glyph_kwargs,
        random=random,
        mesh_kwargs=mesh_kwargs,
        ovm_info=ovm_info
    )


def make_geometric_objects_dict(settings_dict):
    def ellipsoid_func(
            xradius=0.5,
            yradius=0.5 / settings_dict["rod_aspect_ratio"],
            zradius=0.5 / settings_dict["rod_aspect_ratio"],
            u_res=settings_dict["cylinder_resolution"],
            v_res=settings_dict["cylinder_resolution"],
            w_res=settings_dict["cylinder_resolution"]
    ):
        return pv.ParametricEllipsoid(
            xradius=xradius,
            yradius=yradius,
            zradius=zradius,
            u_res=u_res,
            v_res=v_res,
            w_res=w_res
        )

    def cylinder_func(
            center=(0.0, 0.0, 0.0),
            direction=(1.0, 0.0, 0.0),
            radius=0.9 * 0.5 / settings_dict['rod_aspect_ratio'],
            height=0.9,
            resolution=settings_dict['cylinder_resolution'],
            capping=True
    ):
        return pv.Cylinder(
            center=center,
            direction=direction,
            radius=radius,
            height=height,
            resolution=resolution,
            capping=capping
        )

    def dbl_arrow_func(
            direction=(1.0, 0.0, 0.0),
            start=(0.0, 0.0, 0.0),
            tip_length=0.5,
            tip_radius=0.2,
            tip_resolution=20,
            shaft_radius=0.05,
            shaft_resolution=20,
            scale=None
    ):
        kwargs = dict(
            start=start,
            tip_length=tip_length,
            tip_radius=tip_radius,
            tip_resolution=tip_resolution,
            shaft_radius=shaft_radius,
            shaft_resolution=shaft_resolution,
            scale=scale
        )
        arrow1 = pv.Arrow(direction=direction, **kwargs)
        arrow2 = pv.Arrow(direction=-np.array(direction), **kwargs)
        return arrow1.merge(arrow2)

    geometric_objects = dict(
        cylinder=cylinder_func,
        ellipsoid=ellipsoid_func,
        line=pv.Line,
        arrow=pv.Arrow,
        dbl_headed_arrow=dbl_arrow_func,
        sphere=pv.Sphere,
        plane=pv.Plane,
        box=pv.Box,
        cone=pv.Cone,
        polygon=pv.Polygon,
        disc=pv.Disc,
    )

    return geometric_objects


def calculate_tangent(
    dims, mesh, weights=None, threshold=None, sigma=4, field_name='tangent'
):
    """
    Calculate tangent at each point in a mesh, assuming the mesh approximates
    a curve or is parent to an isosurface tube contouring a value related
    to "weights". Parameter "sigma" is the falloff distance for a Gaussian
    weighting function of distance by which each point weighs the direction
    to each other point. Beware: Will show nonsense for regions excluded by
    threshold, since calculation is omitted there.
    """
    hdims = np.array(dims) / 2
    pts = mesh.points
    if weights is None:
        weights = np.ones(len(pts))
    if threshold is not None:
        idxs = np.argwhere(weights >= threshold).flatten()
    else:
        idxs = range(len(pts))
    goodpts = pts[idxs]
    goodweights = weights[idxs]
    tans = np.zeros((len(pts), 3))
    for idx in idxs:
        refpt = pts[idx]
        rvecs = np.mod(goodpts - refpt + hdims, dims) - hdims
        rdistsq = np.sum(rvecs ** 2, axis=-1)
        rdistsq_inv = (rdistsq != 0) / (rdistsq + (rdistsq == 0))
        evecs = np.linalg.eigh(
            np.sum(
                np.einsum(
                    "...i, ...j, ...", rvecs, rvecs,
                    (
                        goodweights
                        * np.exp(-(rdistsq / (sigma ** 2)))
                        * rdistsq_inv
                    )
                ),
                axis=0
            )
        )[1]
        max_evec = evecs[:, -1]
        tans[idx] = max_evec
    if field_name is not None:
        mesh[field_name] = tans
    return tans
