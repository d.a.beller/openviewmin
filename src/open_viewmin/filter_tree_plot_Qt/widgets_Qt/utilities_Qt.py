""" Define classes for dialogs and actor control toolbars """

from qtpy import QtWidgets as qw
import qtpy.QtCore as Qt
import numpy as np


class FormDialog(qw.QDialog):
    def __init__(
        self, title='', message=None, parent=None, apply_button=False, **kwargs
    ):
        super().__init__(**kwargs)
        self.setWindowTitle(title)
        self.buttonBox = qw.QDialogButtonBox(
            qw.QDialogButtonBox.Ok | qw.QDialogButtonBox.Cancel,
            parent=parent
        )
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.layout = qw.QVBoxLayout(self)
        self.formlayout = qw.QFormLayout()
        if message is not None:
            self.layout.addWidget(qw.QLabel(message))
        self.layout.addLayout(self.formlayout)
        self.layout.addWidget(self.buttonBox)
        if apply_button:
            self.apply = lambda: None
            self.add_apply_button()


    def show(self):
        self.exec()

    def add_apply_button(self):
        self.apply_button = self.buttonBox.addButton(qw.QDialogButtonBox.Apply)
        self.apply_button.clicked.connect(self.apply)


class LineEditDialog(FormDialog):
    def __init__(self, ok_btn_action, prompt='', **kwargs):
        super().__init__(**kwargs)
        self.line_edit = qw.QLineEdit()
        self.sublayout = qw.QHBoxLayout()
        self.sublayout.addWidget(self.line_edit)
        self.formlayout.addRow(qw.QLabel(prompt), self.sublayout)
        self.ok_btn_action = ok_btn_action
        self.accepted.connect(self.ok_btn_callback)

    def ok_btn_callback(self):
        self.close()
        self.ok_btn_action(self.line_edit.text())


def add_close_button_to_toolbar(toolbar, closes=None):
    """ Add button to close a widget """
    if closes is None:
        closes = toolbar
    close_button = qw.QToolButton()
    close_button.setAutoRaise(True)
    close_button.setIcon(
        toolbar.style().standardIcon(qw.QStyle.SP_TitleBarCloseButton)
    )
    close_button.released.connect(closes.hide)
    close_button.setToolTip("Hide this toolbar")
    toolbar.addWidget(close_button)
    return close_button


class ControlsToolbar(qw.QToolBar):
    def __init__(
        self, name=None, label=None, icon_size=None, movable=True,
        floatable=False, orientation="horizontal", has_close_button=True
    ):
        if orientation == "vertical":
            orientation_Qt = Qt.Qt.Vertical
        else:
            orientation_Qt = Qt.Qt.Horizontal

        if name is None:
            name = label
        super().__init__(
            name,
            orientation=orientation_Qt,
            movable=movable,
            floatable=floatable
        )

        self.label_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
        self.addWidget(self.label_toolbar)

        self.options_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
        self.addWidget(self.options_toolbar)

        # remove padding between contents
        self.layout().setSpacing(0)
        self.options_toolbar.layout().setSpacing(0)

        self.QLabel = qw.QLabel()
        self.QLabel.setSizePolicy(*(qw.QSizePolicy.Expanding,)*2)
        self.QLabel.setWordWrap(True)
        self.set_label(label)
        self.label_toolbar.addWidget(self.QLabel)
        self.set_icon_size(icon_size)

        if has_close_button:
            self.add_close_button()

        self.rows = [self.label_toolbar, self.options_toolbar]

    def add_close_button(self):
        # """ Add button to close a widget """
        add_close_button_to_toolbar(self.label_toolbar, closes=self)

    def set_label(self, text=None):
        """ Change text in label toolbar """
        if text is not None:
            self.label = text
        if self.label is not None:
            self.QLabel.setText(self.label)

    def set_icon_size(self, icon_size=None):
        if icon_size is not None:
            self.icon_size = icon_size
        if self.icon_size is not None:
            qsize = Qt.QSize(*(self.icon_size,) * 2)
            self.label_toolbar.setIconSize(qsize)
            self.options_toolbar.setIconSize(qsize)


class ControlsSliderToolbar(qw.QToolBar):
    def __init__(
        self, update_method, actor=None,
        num_divs=100, init_val_percent=50, max_val=None, min_val=None,
        scalars=None, label_txt="", name=None, spinbox=True,
        has_close_button=False
    ):
        super().__init__(name, orientation=Qt.Qt.Horizontal)
        if has_close_button:
            self.add_close_button()
        self.update_method = update_method
        self.actor = actor
        self.num_divs = num_divs
        self.min_val = min_val
        self.max_val = max_val
        self.slider = qw.QSlider(Qt.Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(self.num_divs)
        self.slider.setValue(int(init_val_percent * num_divs / 100))

        if max_val is None and scalars is not None:
            self.max_val = np.max(scalars)
        if min_val is None and scalars is not None:
            self.min_val = np.min(scalars)

        self.QLabel = qw.QLabel(label_txt)
        if len(label_txt) > 0:
            self.addWidget(self.QLabel)

        if spinbox:
            self.spinbox = qw.QDoubleSpinBox(
                minimum=self.slider_formula(self.slider.minimum()),
                maximum=self.slider_formula(self.slider.maximum()),
                value=self.slider_formula(init_val_percent),
                singleStep=0.1,
                decimals=3
            )
            self.spinbox.editingFinished.connect(self.spinbox_callback)
            self.addWidget(self.spinbox)
        else:
            self.spinbox = None
        self.slider.valueChanged.connect(self.valuechange_method)
        self.wiggle_slider_to_update()
        self.addWidget(self.slider)

    def add_close_button(self):
        add_close_button_to_toolbar(self)

    def slider_formula(self, slider_value):
        """ Convert from slider integer value to formula float value. """
        return self.min_val + (
                (self.max_val - self.min_val) * slider_value / self.num_divs
        )

    def external_update(self, float_value):
        slider_value = int(
            self.num_divs * (
                (float_value - self.min_val) / (self.max_val - self.min_val)
            )
        )
        self.slider.setValue(slider_value)

    def spinbox_callback(self):
        self.external_update(self.spinbox.value())

    def valuechange_method(self, slider_value):
        float_value = self.slider_formula(slider_value)
        try:
            vis = self.actor.GetVisibility()
        except AttributeError:
            vis = 0
        self.update_method(float_value)
        if self.actor is not None:
            try:
                self.actor.SetVisibility(vis)
            except AttributeError:
                pass
        if self.spinbox is not None:
            self.spinbox.setValue(float_value)

    def wiggle_slider_to_update(self):
        """

        For an actor with some feature controlled by a slider,
        move the slider right and left to force the actor to update

        """
        slider = self.slider
        val = slider.value()
        try:
            for i in [1, -1]:
                slider.setValue(val + i)
                slider.setValue(val)
        except ValueError:
            for i in [-1, 1]:
                slider.setValue(val + i)
                slider.setValue(val)

