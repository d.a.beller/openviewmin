""" Define actions and set up dialogs for actor visualization options accessed
by a dropdown menu in the actor's control toolbar
"""


from matplotlib import colormaps
from qtpy import QtWidgets as qw
from qtpy import QtCore as Qt
from qtpy import QtGui
import numpy as np
from . import utilities_Qt
from ..menus import glyph_modify, streamlines_modify


colormaps_list = colormaps()


def color_picker(plotter, actor_name):
    """ Set solid color of an actor using system color dialog.
    """

    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs
    initial_color = mesh_kwargs.get('color', plotter.settings["actor_color"])
    if isinstance(initial_color, tuple):
        initial_QColor = QtGui.QColor.fromRgbF(*initial_color)
    else:
        initial_QColor = QtGui.QColor(initial_color)
    color_dialog = qw.QColorDialog(initial_QColor, parent=plotter.app_window)

    def color_dialog_callback(chosen_QColor):
        if chosen_QColor.isValid():  # user pressed OK
            chosen_color = chosen_QColor.name()
        else:
            chosen_color = initial_color
        plotter.update_actor(actor_name, color=chosen_color)
    color_dialog.currentColorChanged.connect(color_dialog_callback)
    color_dialog.open()


class QDoubleSpinBoxIgnoreLims(qw.QDoubleSpinBox):
    def __init__(self, *args, num_subdivisions=1000, **kwargs):
        super().__init__(*args, **kwargs)
        self.num_subdivisions = num_subdivisions
        self.high_val = self.maximum()
        self.low_val = self.minimum()
        self.update_single_step()

    @property
    def extent(self):
        return self.high_val - self.low_val

    def set_high_val(self, value):
        self.high_val = value
        if value > 0.5 * (self.maximum() + self.minimum()):
            self.setMaximum(2 * self.maximum() - self.minimum())
        self.update_single_step()

    def set_low_val(self, value):
        self.low_val = value
        if value < 0.5 * (self.minimum() + self.maximum()):
            self.setMinimum(2 * self.minimum() - self.maximum())
        self.update_single_step()

    def update_single_step(self):
        self.setSingleStep(self.extent / self.num_subdivisions)

    def qslider_value(self):
        if self.extent == 0:
            return 0
        else:
            return int(
                (self.value() - self.low_val)
                * self.num_subdivisions / self.extent
            )

    def set_value_from_qslider(self, qslider_value):
        self.setValue(
            self.low_val + qslider_value / self.num_subdivisions * self.extent
        )


def make_color_array_widget(plotter, actor_name):
    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs

    cb_colorby = qw.QComboBox()
    cb_colorby.setFixedWidth(150)
    sb_clim_min = QDoubleSpinBoxIgnoreLims(decimals=3)
    sb_clim_max = QDoubleSpinBoxIgnoreLims(decimals=3)
    sb_clim_min.setFixedWidth(70)
    sb_clim_max.setFixedWidth(sb_clim_min.width())
    qs_clim_min = qw.QSlider(orientation=Qt.Qt.Horizontal)
    qs_clim_max = qw.QSlider(orientation=Qt.Qt.Horizontal)
    cb_cmap = qw.QComboBox()
    cb_cmap.setFixedWidth(cb_colorby.width())
    cb_cmap.addItems(colormaps_list)

    def update_clim_maxmin_display():
        current_cmap = mesh_kwargs.get("cmap", plotter.theme['cmap'])
        if current_cmap in colormaps_list:
            cb_cmap.setCurrentIndex(colormaps_list.index(current_cmap))
        else:
            cb_cmap.setCurrentIndex(-1)
        cb_cmap.currentIndexChanged.connect(
            lambda choice_num: plotter.update_actor(
                actor_name, cmap=colormaps_list[choice_num]
            )
        )
        mesh = plotter.get_mesh(actor_name)
        scalar_field = mesh_kwargs["scalars"]
        if scalar_field in plotter.scalar_fields(mesh_name=mesh):
            if "clim" in mesh_kwargs.keys():
                vmin, vmax = mesh_kwargs["clim"]
            else:
                vmin = np.min(mesh[scalar_field])
                vmax = np.max(mesh[scalar_field])
                plotter.update_actor(
                    actor_name, clim=[vmin, vmax]
                )
            for sb in (sb_clim_min, sb_clim_max):
                sb.set_high_val(vmax)
                sb.set_low_val(vmin)
            sb_clim_min.setValue(vmin)
            sb_clim_max.setValue(vmax)

    def populate_cb_colorby():
        mesh = plotter.get_mesh(actor_name)
        scalar_fields = plotter.scalar_fields(mesh_name=mesh)
        cb_colorby.clear()
        cb_colorby_items = [''] + scalar_fields
        cb_colorby.addItems(cb_colorby_items)
        try:
            current_scalars_array_name = mesh_kwargs['scalars']
        except KeyError:
            current_scalars_array_name = ''
        if current_scalars_array_name in cb_colorby_items:
            cb_colorby.setCurrentIndex(
                cb_colorby_items.index(current_scalars_array_name)
            )

    le_colorbar_title = qw.QLineEdit()
    le_colorbar_title.setText(mesh_kwargs['scalar_bar_args'].get('title', ''))

    def retitle_colorbar():
        plotter.update_actor(
            actor_name, scalar_bar_args=dict(title=le_colorbar_title.text())
        )

    le_colorbar_title.editingFinished.connect(retitle_colorbar)

    def colorby_callback(choice_num):
        if choice_num > 0:
            mesh = plotter.get_mesh(actor_name)
            color_array_name = cb_colorby.itemText(choice_num)
            vmin = np.min(mesh[color_array_name])
            vmax = np.max(mesh[color_array_name])
            plotter.update_actor(
                actor_name,
                scalars=color_array_name,
                clim=[vmin, vmax]
            )
            update_clim_maxmin_display()
            le_colorbar_title.setText(f"{color_array_name} ({actor_name})")

    cb_colorby.currentIndexChanged.connect(colorby_callback)

    def sb_clim_min_callback(value):
        # don't let clim min exceed clim max
        clim_max_val = sb_clim_max.value()
        if value > clim_max_val:
            sb_clim_min.setValue(clim_max_val)

        plotter.update_actor(
            actor_name, clim=[value, clim_max_val]
        )
        try:
            qs_clim_min.setValue(sb_clim_min.qslider_value())
        except ZeroDivisionError:
            pass

    sb_clim_min.valueChanged.connect(sb_clim_min_callback)

    def sb_clim_max_callback(value):
        # don't let clim max drop below clim min
        clim_min_val = sb_clim_min.value()
        if value < clim_min_val:
            sb_clim_max.setValue(clim_min_val)

        plotter.update_actor(actor_name, clim=[clim_min_val, value])
        try:
            qs_clim_max.setValue(sb_clim_max.qslider_value())
        except ZeroDivisionError:
            pass

    sb_clim_max.valueChanged.connect(sb_clim_max_callback)

    for qs, sb in ((qs_clim_min, sb_clim_min), (qs_clim_max, sb_clim_max)):
        qs.valueChanged.connect(sb.set_value_from_qslider)

    color_array_widget = utilities_Qt.FormDialog(
        f'Color for \"{actor_name}\"',
        parent=plotter.app_window
    )
    formlayout = color_array_widget.formlayout
    formlayout.addRow(qw.QLabel('color array'), cb_colorby)
    formlayout.addRow(qw.QLabel('colormap'), cb_cmap)
    sublayout_min = qw.QHBoxLayout()
    sublayout_min.addWidget(sb_clim_min)
    sublayout_min.addWidget(qs_clim_min)
    pb_symmetrize_min = qw.QPushButton('= -max')

    def pb_symmetrize_min_callback():
        if sb_clim_max.value() > 0:
            new_min_val = -sb_clim_max.value()
            if new_min_val < sb_clim_min.low_val:
                sb_clim_min.low_val = new_min_val
            sb_clim_min.setValue(new_min_val)

    pb_symmetrize_min.clicked.connect(pb_symmetrize_min_callback)
    sublayout_min.addWidget(pb_symmetrize_min)
    formlayout.addRow(qw.QLabel('min value'), sublayout_min)
    sublayout_max = qw.QHBoxLayout()
    sublayout_max.addWidget(sb_clim_max)
    sublayout_max.addWidget(qs_clim_max)
    pb_symmetrize_max = qw.QPushButton('= |min|')

    def pb_symmetrize_max_callback():
        if sb_clim_min.value() < 0:
            new_max_val = -sb_clim_min.value()
            if new_max_val > sb_clim_max.high_val:
                sb_clim_max.high_val = new_max_val
            sb_clim_max.setValue(new_max_val)

    pb_symmetrize_max.clicked.connect(pb_symmetrize_max_callback)
    sublayout_max.addWidget(pb_symmetrize_max)
    formlayout.addRow(qw.QLabel('max value'), sublayout_max)

    sublayout_title = qw.QHBoxLayout()
    sublayout_title.addWidget(le_colorbar_title)
    apply_title_button = qw.QPushButton('Apply')
    apply_title_button.clicked.connect(retitle_colorbar)
    sublayout_title.addWidget(apply_title_button)
    formlayout.addRow(qw.QLabel('colorbar title'), sublayout_title)

    populate_cb_colorby()
    update_clim_maxmin_display()
    color_array_widget.show()


def set_opacity_callback(plotter, actor_name):
    filter_formula = plotter.get_filter_formula(actor_name)
    mesh_kwargs = filter_formula.mesh_kwargs
    defaults = plotter.settings['default_mesh_kwargs']
    opacity = mesh_kwargs.get("opacity", 1)

    minwidth = 350

    set_opacity_widget = utilities_Qt.FormDialog(
        title=f'Opacity & lighting options for '
              f'\"{actor_name}\"'
    )
    set_opacity_widget.setMinimumWidth(minwidth)
    opacity_form_layout = set_opacity_widget.formlayout

    sb_set_opacity = qw.QDoubleSpinBox(minimum=0, maximum=1)
    sb_set_opacity.setSingleStep(0.1)
    sb_set_opacity.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, opacity=value)
    )
    opacity_form_layout.addRow(
        qw.QLabel('<b>Opacity</b>'),
    )
    opacity_form_layout.addRow(qw.QLabel('opacity: '), sb_set_opacity)

    opacity_form_layout.addRow(
        qw.QLabel('<b>Physically Based Rendering (PBR)</b>'),
    )
    cb_pbr = qw.QCheckBox()
    cb_pbr.stateChanged.connect(
        lambda: plotter.update_actor(actor_name, pbr=cb_pbr.isChecked())
    )
    opacity_form_layout.addRow(qw.QLabel('PBR:'), cb_pbr)
    sb_metallic = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_metallic.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, metallic=value)
    )
    opacity_form_layout.addRow(qw.QLabel('metallic: '), sb_metallic)

    sb_roughness = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_roughness.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, roughness=value)
    )
    opacity_form_layout.addRow(qw.QLabel('roughness: '), sb_roughness)

    opacity_form_layout.addRow(qw.QLabel('<b>Other options</b>'), )

    sb_diffuse = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_diffuse.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, diffuse=value)
    )
    opacity_form_layout.addRow(qw.QLabel('diffuse: '), sb_diffuse)

    sb_ambient = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_ambient.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, ambient=value)
    )
    opacity_form_layout.addRow(qw.QLabel('ambient: '), sb_ambient)

    sb_specular = qw.QDoubleSpinBox(
        minimum=0, maximum=1, singleStep=0.1, decimals=2
    )
    sb_specular.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, specular=value)
    )
    opacity_form_layout.addRow(qw.QLabel('specular: '), sb_specular)

    sb_specular_power = qw.QSpinBox(
        minimum=1, maximum=100, singleStep=1
    )
    sb_specular_power.valueChanged.connect(
        lambda value: plotter.update_actor(actor_name, specular_power=value)
    )
    opacity_form_layout.addRow(
        qw.QLabel('specular power: '), sb_specular_power
    )
    sb_set_opacity.setValue(opacity)
    current_pbr = mesh_kwargs.get("pbr", defaults['pbr'])
    cb_pbr.setChecked(current_pbr)
    for spinbox, attr_string in (
        (sb_metallic, "metallic"),
        (sb_roughness, "roughness"),
        (sb_diffuse, "diffuse"),
        (sb_ambient, "ambient"),
        (sb_specular, "specular"),
        (sb_specular_power, "specular_power")
    ):
        current_val = mesh_kwargs.get(attr_string, defaults[attr_string])
        spinbox.setValue(current_val)
    set_opacity_widget.show()


def toggle_colorbar_callback(plotter, actor_name):
    colorbar = plotter.get_filter_formula(actor_name).colorbar
    if colorbar is not None:
        plotter.toggle_scalar_bar_visibility(colorbar)


def rename_callback(plotter, actor_name):
    rename_widget = utilities_Qt.LineEditDialog(
        lambda current_text: plotter.rename_mesh(actor_name, current_text),
        title=f"Rename mesh/actor \'{actor_name}\'",
        prompt="Rename as: ",
        parent=plotter.app_window
    )
    rename_widget.show()


def remove_callback(plotter, actor_name):
    plotter.remove_actor_completely(actor_name)


def invert_callback(plotter, actor_name):
    filter_formula = plotter.get_filter_formula(actor_name)
    ovm_info = filter_formula.ovm_info
    current_invert = ovm_info.get("invert", False)
    plotter.update_filter(
        actor_name,
        ovm_info=dict(invert=not current_invert),
        update_actor=True
    )


def apolar_callback(plotter, actor_name):
    filter_formula = plotter.get_filter_formula(actor_name)
    ovm_info = filter_formula.ovm_info
    current_apolar = ovm_info.get("apolar", False)
    ovm_info["apolar"] = not current_apolar
    normal_widget_name = plotter.get_filter_formula(actor_name).widget_name
    plotter.get_widget_formula(normal_widget_name).update()


def streamlines_settings_callback(plotter, actor_name):
    streamlines_settings_widget = (
        streamlines_modify.make_streamlines_settings_widget(
            plotter, actor_name
        )
    )
    streamlines_settings_widget.show()


def glyph_settings_choice_callback(plotter, actor_name):
    glyph_settings_widget = glyph_modify.make_glyph_settings_widget(
        plotter,actor_name
    )
    glyph_settings_widget.show()


def populate_viz_options_menu(plotter, actor_name, viz_options_button=None):
    filter_formula = plotter.get_filter_formula(actor_name)
    if viz_options_button is None:
        toolbar = plotter.toolbars[actor_name]
        viz_options_button = toolbar.visualization_options_button
    viz_options_menu = viz_options_button.menu()
    viz_options_name_action_pairs = [
        ["solid color", color_picker],
        ["color array", make_color_array_widget],
        ["show/hide colorbar", toggle_colorbar_callback],
        ["lighting", set_opacity_callback]
    ]
    if "invert" in filter_formula.filter_kwargs.keys():
        viz_options_name_action_pairs.append(["invert", invert_callback])

    if "orient" in filter_formula.filter_kwargs.keys():
        viz_options_name_action_pairs.append(
            ["glyph settings", glyph_settings_choice_callback]
        )
    if "apolar" in filter_formula.ovm_info.keys():
        viz_options_name_action_pairs.append(
            ["toggle apolar", apolar_callback]
        )
    if "surface_streamlines" in filter_formula.filter_kwargs.keys():
        viz_options_name_action_pairs.append(
            ["streamlines settings", streamlines_settings_callback]
        )
    viz_options_name_action_pairs += [
        ["rename", rename_callback],
        ["remove (!)", remove_callback]
    ]
    viz_options_menu.clear()

    def meta_callback(callback):
        return lambda: callback(plotter, actor_name)

    for label, callback in viz_options_name_action_pairs:
        viz_options_menu.addAction(
            label,
            meta_callback(callback)
        )
    viz_options_button.setToolTip(
        f'Customization options for \"{actor_name}\"'
    )


def add_visualization_options_menu(actor_control_toolbar):
    """ Create dropdown menu of actor customizations """

    plotter = actor_control_toolbar.plotter
    toolbar = actor_control_toolbar.options_toolbar
    actor_name = actor_control_toolbar.actor_name

    viz_options_button = qw.QPushButton(plotter.settings["actor_menu_symbol"])
    viz_options_button.setFixedWidth(55)
    viz_options_menu = qw.QMenu()

    viz_options_button.setMenu(viz_options_menu)
    populate_viz_options_menu(plotter, actor_name, viz_options_button)
    toolbar.addWidget(viz_options_button)
    return viz_options_button