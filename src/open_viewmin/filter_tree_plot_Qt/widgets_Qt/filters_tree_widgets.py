""" Routines to create and modify the in-GUI text representation of the
filters tree
"""

import qtpy.QtWidgets as qw
from qtpy import QtGui
from qtpy import QtCore as Qt
from .. import settings_Qt


def fill_filters_tree_recursively(
    plotter, parent_filter_name, parent_tree_item
):
    """
    For each branch of filters tree, find all child meshes and their child meshes, etc.,
    until reaching actors (leaves of tree)
    """
    for child_name in (
        plotter.get_filter_formula(parent_filter_name).children
    ):
        is_mesh = True
        is_actor = plotter.get_filter_formula(child_name).has_actor
        child_tree_item = format_for_filters_tree(
            plotter,
            child_name,
            is_mesh=is_mesh,
            is_actor=is_actor
        )
        plotter.filters_tree_widget_items[child_name] = child_tree_item
        parent_tree_item.addChild(child_tree_item)
        if (is_mesh and child_name != parent_filter_name):
            # if a child has the same name as its parent, assume this is an
            # actor i.e. a leaf of the tree,
            # so stop recursing; otherwise we would have infinite recursion
            fill_filters_tree_recursively(plotter, child_name, child_tree_item)


def add_filters_tree_top_item(plotter, mesh_name, is_mesh=True, is_actor=False):
    tree_top_item = format_for_filters_tree(
        plotter,
        mesh_name,
        is_mesh=is_mesh,
        is_actor=is_actor
    )
    plotter.filters_tree.addTopLevelItem(tree_top_item)


def format_for_filters_tree(
    plotter, label, is_mesh=None, is_actor=None
):
    if is_mesh is None:
        is_mesh = (label in plotter.mesh_names)
    if is_actor is None:
        is_actor = plotter.get_filter_formula(label).has_actor
    ret = qw.QTreeWidgetItem([label])
    font = QtGui.QFont()
    font.setBold(is_actor)
    font.setItalic(is_mesh)
    ret.setFont(0, font)
    return ret


def create_filters_tree(plotter):
    """ Set up a logical tree of parent-child pairs of meshes and actors,
    to be displayed in the controls area """

    plotter.toolbars['filters tree'] = qw.QToolBar(
        'filters tree',
        orientation=Qt.Qt.Vertical
    )
    plotter.toolbars['filters tree'].addWidget(
        qw.QLabel(
            f"{settings_Qt.mesh_label_style('meshes')} "
            f"and {settings_Qt.actor_label_style('actors')}"
        )
    )
    plotter.filters_tree = qw.QTreeWidget()
    plotter.filters_tree.setColumnCount(1)
    plotter.filters_tree.setWordWrap(True)
    add_filters_tree_top_item(plotter, 'fullmesh')
    plotter.toolbars['filters tree'].addWidget(plotter.filters_tree)


    def filters_tree_callback(item):
        item_name = item.text(0)
        if item_name in plotter.toolbars.keys():
            toolbar = plotter.toolbars[item_name]
            if toolbar.isHidden():
                toolbar.show()


    plotter.filters_tree.itemClicked.connect(filters_tree_callback)
    plotter.controls_dock_window.addToolBar(
        Qt.Qt.LeftToolBarArea, plotter.toolbars['filters tree']
    )

    # replace ellipsis policy with horizontal scrollbar
    plotter.filters_tree.header().setSectionResizeMode(
        qw.QHeaderView.ResizeToContents
    )
    plotter.filters_tree.header().setStretchLastSection(False)
    plotter.filters_tree.setHeaderHidden(True)
    update_filters_tree(plotter)


def update_filters_tree(plotter):
    if hasattr(plotter, "filters_tree"):
        for i in range(plotter.filters_tree.topLevelItemCount()):
            tree_top_item = plotter.filters_tree.topLevelItem(i)
            filter_name = tree_top_item.text(0)
            if filter_name in plotter.mesh_names:
                tree_top_item.takeChildren()
                fill_filters_tree_recursively(
                    plotter, filter_name, tree_top_item
                )
        plotter.filters_tree.expandAll()
