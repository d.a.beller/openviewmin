""" Routines to create actor control toolbars in the Qt GUI """


import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
import numpy as np
from . import utilities_Qt, visualization_options
from .. import settings_Qt
from ..menus import mesh_options
from ...filter_tree_plot.widgets import plane_widgets


def setup_lighting_control(plotter):
    """ Slider to change lighting intensity """
    plotter.toolbars["lighting"] = utilities_Qt.ControlsSliderToolbar(
        lambda value: plotter.set_lights_intensity(
            value * plotter.settings["lighting_rescale_factor"]
        ),
        None,
        label_txt="☀︎",
        min_val=0,
        max_val=20 / plotter.settings["lighting_rescale_factor"],
        init_val_percent=8 / plotter.settings["lighting_rescale_factor"],
        name="lighting",
        spinbox=False,
    )
    plotter.toolbars["lighting"].slider.setFixedWidth(150)
    plotter.controls_dock_window.addToolBar(
        Qt.Qt.LeftToolBarArea,
        plotter.toolbars["lighting"]
    )
    plotter.toolbars["lighting"].setOrientation(1)


class SliceControlsToolBar(qw.QToolBar):
    def __init__(
        self, parent_toolbar, widget_name=None, make_origin_slider=True
    ):
        """ For a plane widget, add an expandable set of rows to its controls
        toolbar to change its normal and origin """
        super().__init__(parent_toolbar, orientation=Qt.Qt.Vertical)
        self.parent_toolbar = parent_toolbar
        self.plotter = self.parent_toolbar.plotter
        self.actor_name = self.parent_toolbar.actor_name
        self.QSliders = dict()
        self.input_boxes = dict()
        self.make_origin_slider = make_origin_slider

        if widget_name is None:
            widget_name = self.actor_name + "_widget"
            assert widget_name in self.plotter.widgets.keys()
        self.widget_name = widget_name
        self.widget = self.plotter.get_widget(self.widget_name)
        if self.widget is None:
            current_normal = [1, 0, 0]
        else:
            current_normal = self.widget.GetNormal()
        current_theta = np.arccos(current_normal[2])
        current_phi = np.arctan2(current_normal[1], current_normal[0])
        self.num_slider_divs = int(2 * np.pi * 1000)

        self.theta_slider, self.theta_input_box = self.setup_slice_control(
            "θ", 0, np.pi, current_theta,
            self.slice_theta_slider_formula,
            self.slice_theta_slider_inv_formula,
            self.slice_theta_slider_value_changed_callback,
        )
        self.phi_slider, self.phi_input_box = self.setup_slice_control(
            "φ", 0, 2 * np.pi, current_phi,
            self.slice_phi_slider_formula,
            self.slice_phi_slider_inv_formula,
            self.slice_phi_slider_value_changed_callback
        )

        max_slice_translate = int(
            np.sqrt(np.sum((np.array(self.plotter.dims) / 2) ** 2))
        )
        if self.make_origin_slider:
            self.origin_slider, self.origin_input_box = self.setup_slice_control(
                "⟂", -max_slice_translate, max_slice_translate, 0,
                lambda val: val,
                lambda val: val,
                self.slice_origin_slider_value_changed_callback,
                use_ints=True,
                slider_minimum=-max_slice_translate,
                slider_maximum=max_slice_translate
            )

        self.parent_toolbar.slice_control_toolbar = self
        self.parent_toolbar.addWidget(self)

        self.setup_orient_along_axes_buttons()

        self.expand_controls_button = qw.QToolButton()
        self.expand_controls_button.setAutoRaise(True)
        self.expand_controls_button.released.connect(
            self.expand_or_collapse_control_toolbar
        )
        self.parent_toolbar.options_toolbar.addWidget(
            self.expand_controls_button
        )
        self.setFixedWidth(
            self.plotter.settings["controls_area_width"]
        )
        self.expanded = False
        self.collapse()  # initialize collapsed

    @property
    def widget_formula(self):
        return self.plotter.get_widget_formula(self.widget_name)

    def slice_theta_slider_formula(self, value):
        return value * np.pi / self.num_slider_divs

    def slice_theta_slider_inv_formula(self, value):
        return int(value / (np.pi / self.num_slider_divs))

    def slice_value_changed_callback(self, key, value_callable):
        def return_func(value):
            widget_formula = self.widget_formula
            if widget_formula is not None:
                return plane_widgets.alter_plane_widget(
                    widget_formula,
                    **{key: value_callable(value)}
            )
        return return_func

    def slice_theta_slider_value_changed_callback(self, value):
        return self.slice_value_changed_callback(
            "theta", self.slice_theta_slider_formula
        )(value)

    def slice_phi_slider_formula(self, value):
        return value * 2 * np.pi / self.num_slider_divs

    def slice_phi_slider_inv_formula(self, value):
        return int(value / (2 * np.pi / self.num_slider_divs))

    def slice_phi_slider_value_changed_callback(self, value):
        return self.slice_value_changed_callback(
            "phi", self.slice_phi_slider_formula
        )(value)

    def slice_origin_slider_value_changed_callback(self, value):
        return self.slice_value_changed_callback(
            "origin",
            lambda value: (
                0.5 * np.array(self.plotter.dims)
                + value * np.array(self.widget.GetNormal())
            )
        )(value)

    def setup_slice_control(
        self, label, minimum, maximum, init_val,
        slider_formula, slider_inv_formula, value_changed_callback,
        use_ints=False, slider_minimum=0, slider_maximum=None
    ):
        if slider_maximum is None:
            slider_maximum = self.num_slider_divs
        slider = qw.QSlider(
            minimum=slider_minimum, maximum=slider_maximum,
            orientation=Qt.Qt.Horizontal
        )

        slider.valueChanged.connect(value_changed_callback)
        if use_ints:
            input_box = qw.QSpinBox(minimum=minimum, maximum=maximum)
        else:
            input_box = qw.QDoubleSpinBox(
                minimum=minimum, maximum=maximum, decimals=3
            )
        input_box.editingFinished.connect(
            lambda: slider.setValue(
                slider_inv_formula(input_box.value())
            )
        )

        slider.valueChanged.connect(
            lambda value: input_box.setValue(slider_formula(value))
        )

        input_box.setValue(init_val)
        slider.setValue(slider_inv_formula(input_box.value()))

        toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
        toolbar.addWidget(qw.QLabel(label))
        toolbar.addWidget(input_box)
        toolbar.addWidget(slider)

        self.addWidget(toolbar)
        return slider, input_box

    def create_orientation_button_callback(self, theta, phi):
        def return_func():
            self.theta_slider.setValue(
                self.slice_theta_slider_inv_formula(theta)
            )
            self.phi_slider.setValue(
                self.slice_phi_slider_inv_formula(phi)
            )

        return return_func

    def origin_reset_callback(self):
        self.origin_slider.setValue(0)
        self.widget.SetOrigin(*(dim/2 for dim in self.plotter.dims))

    def setup_orient_along_axes_buttons(self):
        toolbar = qw.QToolBar()
        button_width = 40
        pi = np.pi
        for button_label, button_theta, button_phi in (
                ("+x", pi / 2, 0),
                ("-x", pi / 2, pi),
                ("+y", pi / 2, pi / 2),
                ("-y", pi / 2, 3 * pi / 2),
                ("+z", 0, 0),
                ("-z", pi, 0)
        ):
            orientation_button = qw.QToolButton()
            orientation_button.setText(button_label)
            orientation_button.setMaximumWidth(button_width)
            orientation_button.clicked.connect(
                self.create_orientation_button_callback(
                    button_theta, button_phi
                )
            )
            toolbar.addWidget(orientation_button)

        if self.make_origin_slider:
            origin_reset_button = qw.QToolButton()
            origin_reset_button.setText("center")
            origin_reset_button.clicked.connect(self.origin_reset_callback)
            toolbar.addWidget(origin_reset_button)

        toolbar.setStyleSheet("QToolBar{spacing:0px;}")
        self.addWidget(toolbar)

    def expand(self):
        height_setting = self.plotter.settings["slice_control_toolbar_height"]
        self.setFixedHeight(height_setting)
        self.expand_controls_button.setIcon(
            self.plotter.style().standardIcon(
                qw.QStyle.SP_TitleBarShadeButton
            )
        )
        self.setFixedWidth(
            self.plotter.settings["controls_area_width"]
        )
        self.expanded = True

    def collapse(self):
        self.setFixedHeight(0)
        self.expand_controls_button.setIcon(
            self.plotter.style().standardIcon(
                qw.QStyle.SP_TitleBarUnshadeButton
            )
        )
        self.expanded = False

    def expand_or_collapse_control_toolbar(self):
        if self.expanded:
            self.collapse()
        else:
            self.expand()


class ControlsCheckboxToolBar(utilities_Qt.ControlsToolbar):
    def __init__(
        self, plotter, name=None, label=None, icon_size=None, movable=True,
        floatable=True, items_dict_reference=None,
        init_state_callback=None, state_changed_callback=None,
        do_color_buttons=False
    ):
        if icon_size is None:
            icon_size = plotter.settings.get("icon_size")
        if name is None:
            name = label
        self.name = name
        super().__init__(
            name=name, label=label, icon_size=icon_size, movable=movable,
            floatable=floatable, has_close_button=False
        )
        self.plotter = plotter
        self.plotter.toolbars[self.name] = self

        # set checkbox toolbar orientation to vertical
        self.options_toolbar.setOrientation(2)

        add_toolbar_to_controls_dock_widget(self.plotter, self.name)
        self.items = dict()
        self.checkboxes = dict()
        self.checkbox_toolbars = dict()
        self.items_dict_reference = items_dict_reference
        self.init_state_callback = init_state_callback
        self.state_changed_callback = state_changed_callback
        self.color_button_enabled=do_color_buttons

        if self.items_dict_reference is not None:
            refresh_button = qw.QToolButton()
            refresh_button.setIcon(
                plotter.style().standardIcon(qw.QStyle.SP_BrowserReload)
            )
            refresh_button.setToolTip('Update list of items')
            refresh_button.clicked.connect(self.populate)
            self.label_toolbar.addWidget(refresh_button)
            self.populate()
        self.add_close_button()

    def add_item(
        self, item_name, init_state_callback, state_changed_callback
    ):
        self.items[item_name] = (init_state_callback, state_changed_callback)
        self.add_checkbox(item_name)
        if self.color_button_enabled:
            self.make_label_a_color_button(item_name)
        self.refresh(item_name)

    def populate(self):
        if self.items_dict_reference is not None:
            self.options_toolbar.clear()
            self.items.clear()
            for item_name in self.items_dict_reference.keys():
                obj = self.items_dict_reference[item_name]
                self.add_item(
                    item_name,
                    self.init_state_callback(obj),
                    self.state_changed_callback(obj)
                )

    def add_checkbox(self, item_name):
        checkboxes_toolbar = self.options_toolbar
        if item_name in self.items.keys():
            init_state_callback, state_changed_callback = self.items[item_name]
            this_checkbox_toolbar = qw.QToolBar()
            self.checkbox_toolbars[item_name] = this_checkbox_toolbar
            this_checkbox_toolbar.setOrientation(1)
            this_checkbox_toolbar.setFixedWidth(
                int(0.9 * self.plotter.settings["controls_area_width"])
            )
            self.checkboxes[item_name] = qw.QCheckBox(item_name)
            checkbox = self.checkboxes[item_name]
            self.refresh(item_name=item_name)
            checkbox.stateChanged.connect(
                lambda: state_changed_callback(checkbox.isChecked())
            )
            this_checkbox_toolbar.addWidget(checkbox)
            this_checkbox_toolbar.setIconSize(
               Qt.QSize(*((self.plotter.settings["icon_size"],) * 2))
            )
            checkboxes_toolbar.addWidget(this_checkbox_toolbar)

    def make_label_a_color_button(self, item_name):
        """ Replace label of checkbox with a separate button that activates
        a color picker dialog for the widget
        """
        if item_name in self.checkbox_toolbars.keys():
            if item_name in self.checkboxes.keys():
                self.checkboxes[item_name].setText("")

            color_dialog = qw.QColorDialog(parent=self.plotter.app_window)

            def color_dialog_callback(chosen_QColor):
                chosen_color = chosen_QColor.name()
                widget_formula = self.plotter.get_widget_formula(item_name)
                widget_formula.set_color(chosen_color)

            color_dialog.currentColorChanged.connect(color_dialog_callback)

            max_len_display_name = 24
            if len(item_name) > max_len_display_name:
                display_name = item_name[:max_len_display_name] + " •••"
            else:
                display_name = item_name

            action = self.checkbox_toolbars[item_name].addAction(
                display_name,
                color_dialog.open
            )
            action.setToolTip(f"Set color for {item_name}")



    def refresh(self, item_name=None):
        if item_name is None:
            self.refresh_all()
        else:
            init_state_callback, _ = self.items[item_name]
            self.checkboxes[item_name].setChecked(init_state_callback())

    def refresh_all(self):
        for item_name in self.items.keys():
            self.refresh(item_name=item_name)


def setup_widget_control(plotter):
    """
    Add toolbar to controls dock containing checkboxes for enabling/disabling
    PyVista widgets.

    Parameters
    ----------
    plotter

    Returns
    -------

    """
    plotter.toolbars["widgets"] = ControlsCheckboxToolBar(
        plotter, label="widgets",
        items_dict_reference=plotter.widgets,
        init_state_callback=lambda widget: widget.GetEnabled,
        state_changed_callback=lambda widget: widget.SetEnabled,
        do_color_buttons=True
    )


def add_toolbar_to_controls_dock_widget(
    plotter, toolbar_name, orientation_int=2  # vertical by default
):
    toolbar = plotter.toolbars[toolbar_name]
    toolbar.setOrientation(orientation_int)
    plotter.controls_dock_window.addToolBar(Qt.Qt.LeftToolBarArea, toolbar)


class ActorControlToolbar(utilities_Qt.ControlsToolbar):
    def __init__(self, plotter, actor_name, label=None):
        self.plotter = plotter
        self.actor_name = actor_name
        super().__init__(
            name=self.actor_name,
            label=self.format_label_for_filters_tree(label),
            icon_size=self.plotter.settings['icon_size']
        )
        self.add_visibility_checkbox()
        self.visualization_options_button = (
            visualization_options.add_visualization_options_menu(self)
        )
        self.mesh_options_button = mesh_options.add_mesh_options_menu(self)
        self.QSlider = None
        self.slider_toolbar = None
        self.QSlider_target_type = ""
        self.slice_controls = None

    def set_label(self, text=None):
        return super().set_label(text=self.format_label_for_filters_tree(text))

    def format_label_for_filters_tree(self, label):
        # plotter = self.plotter
        if label is None:
            label = self.actor_name
        # label = settings_Qt.mesh_label_style(label)
        # if self.actor_name in plotter.mesh_names:
        #     label = settings_Qt.mesh_label_style(label)
        # if self.actor_name in plotter.actor_names:
        #     label = settings_Qt.actor_label_style(label)
        return label

    def add_to_plotter(self):
        self.plotter.controls_dock_window.addToolBar(
            Qt.Qt.LeftToolBarArea, self
        )

    def add_visibility_checkbox(self):
        """ Add a checkbox that toggles an actor's visibility """
        self.visibility_checkbox = qw.QCheckBox(
            self.plotter.settings["visible_symbol"]
        )
        self.assign_checkbox_to_actor_visibility()
        self.options_toolbar.addWidget(self.visibility_checkbox)

    def assign_checkbox_to_actor_visibility(self, actor_name=None):
        plotter = self.plotter
        checkbox = self.visibility_checkbox
        if actor_name is not None:
            self.actor_name = actor_name

        if self.actor_name in plotter.renderer.actors.keys():
            actor = plotter.get_actor(self.actor_name)
            try:
                checkbox.toggled.disconnect()
            except TypeError:
                pass
            checkbox.setChecked(actor.GetVisibility())
            checkbox.toggled.connect(
                lambda: plotter.toggle_filter_tree_actor_visibility(
                    self.actor_name
                )
            )
            checkbox.setToolTip(
                'toggle visibility of \"' + self.actor_name + '\"'
            )

    def add_QSlider(
        self, update_method, num_divs=100,
        init_val_percent=50, min_val=None, max_val=None, scalars=None,
        label_txt=""
    ):
        actor = self.plotter.get_actor(self.actor_name)
        if label_txt is None:
            actor_info = self.plotter.get_filter_formula(self.actor_name)
            label_txt = actor_info.filter_kwargs['scalars']
            label_txt = label_txt[:3]
        cst = utilities_Qt.ControlsSliderToolbar(
            update_method,
            actor,
            num_divs=num_divs,
            init_val_percent=init_val_percent,
            min_val=min_val,
            max_val=max_val,
            scalars=scalars,
            label_txt=label_txt
        )

        self.addWidget(cst)

        self.slider_toolbar = cst
        self.QSlider = cst.slider
        return cst

    def add_QSlider_type(self, type_string, **kwargs):
        if type_string == "isosurface":
            return self.add_isosurface_QSlider(**kwargs)
        elif type_string == "threshold":
            return self.add_threshold_QSlider(**kwargs)
        else:
            raise ValueError(
                f"Unrecognized QSlider target type string {type_string}"
            )

    def get_min_max_scalars(self, min_val=None, max_val=None):
        plotter = self.plotter
        actor_name = self.actor_name
        actor_info = plotter.get_filter_formula(actor_name)
        dataset_name = actor_info.filter_kwargs['scalars']
        parent_mesh = plotter.get_mesh(actor_info.parent_mesh_name)
        dataset = parent_mesh[dataset_name]
        if min_val is None:
            min_val = np.min(dataset)
        if max_val is None:
            max_val = np.max(dataset)
        return dict(
            min_val=min_val,
            max_val=max_val,
            scalars=dataset_name
        )

    def isosurface_slider_callback(self, contour_value):
        self.plotter.update_filter(
            self.actor_name,
            update_actor=True,
            isosurfaces=[contour_value]
        )

    def add_isosurface_QSlider(
        self, min_val=None, max_val=None, **kwargs
    ):
        self.QSlider_target_type = "isosurface"
        return self.add_QSlider(
            self.isosurface_slider_callback,
            **self.get_min_max_scalars(
                min_val=min_val, max_val=max_val
            ),
            **kwargs
        )

    def threshold_slider_callback(self, value_lims):
        self.plotter.update_filter(
            self.actor_name,
            update_actor=True,
            value=value_lims
        )

    def add_threshold_QSlider(
        self, min_val=None, max_val=None, **kwargs
    ):
        self.QSlider_target_type = "threshold"
        return self.add_QSlider(
            self.threshold_slider_callback,
            **self.get_min_max_scalars(
                min_val=min_val, max_val=max_val
            ),
            **kwargs
        )

    def wiggle_slider_to_update(self):
        """ For an actor with some feature controlled by a slider,
        move the slider right and left to force the actor to update
        """
        slider = self.QSlider
        if slider is not None:
            val = slider.value()
            try:
                for i in [1, -1]:
                    slider.setValue(val + i)
                    slider.setValue(val)
            except ValueError:
                for i in [-1, 1]:
                    slider.setValue(val + i)
                    slider.setValue(val)

    def make_slice_slider_controls(
            self, widget_name=None, make_origin_slider=True
    ):
        self.slice_controls = SliceControlsToolBar(
            self,
            widget_name=widget_name,
            make_origin_slider=make_origin_slider
        )

    def add_threshold_text_controls(self, label=""):
        plotter = self.plotter

        def threshold_set_val(val_idx, spinbox):
            def return_func():
                filter_formula = plotter.get_filter_formula(self.actor_name)
                filter_kwargs = filter_formula.filter_kwargs
                current_min, current_max = filter_kwargs["value"]
                new_value = spinbox.value()
                if (
                    (val_idx == 1 and new_value > current_min)
                    or (val_idx == 0 and new_value < current_max)
                ):
                    filter_kwargs["value"][val_idx] = spinbox.value()
                    filter_formula.update(update_actor=True)
                else:
                    # if bad value entered in spinbox, reset to previous value
                    spinbox.setValue(filter_kwargs["value"][val_idx])

            return return_func

        spinboxes_toolbar = qw.QToolBar()
        spinboxes_toolbar.addWidget(qw.QLabel(label + "  "))
        for val_idx in range(2):
            val_lbl = ["min", "max"][val_idx]
            spinboxes_toolbar.addWidget(qw.QLabel(val_lbl))
            spinbox = qw.QDoubleSpinBox()

            filter_formula = plotter.get_filter_formula(self.actor_name)
            filter_kwargs = filter_formula.filter_kwargs
            if "value" not in filter_kwargs.keys():
                filter_kwargs["value"] = [0, 1]
            spinbox.setValue(filter_kwargs["value"][val_idx])
            spinbox.editingFinished.connect(threshold_set_val(val_idx, spinbox))
            spinboxes_toolbar.addWidget(spinbox)
        self.addWidget(spinboxes_toolbar)


def add_viscolor_toolbar(plotter, actor_name, label=None, slider_for=None):
    """ Standard controls toolbar for each actor's rendering options """
    if slider_for is None:
        toolbar = ActorControlToolbar(plotter, actor_name)
    elif slider_for in ("isosurface", "threshold"):
        toolbar = ActorControlToolbar(plotter, actor_name)
        toolbar.add_QSlider_type(slider_for, label=label)
    elif slider_for == "slice":
        toolbar = ActorControlToolbar(plotter, actor_name)
        toolbar.make_slice_slider_controls()
    else:
        raise KeyError(
            "Invalid value for keyword argumentr \'slider_for\'."
        )
    plotter.toolbars[actor_name] = toolbar
    toolbar.add_to_plotter()


def get_lights_intensity_from_slider(plotter):
    plotter.set_lights_intensity(
        plotter.toolbars['lighting'].slider.value()
        * plotter.settings['lighting_rescale_factor']
    )


def setup_animation_buttons(plotter):
    """ Buttons for animations toolbar to change the frame number """
    btn_width = 20
    plotter.toolbars["animation"] = qw.QToolBar(
        "Animation",
        orientation=Qt.Qt.Vertical
    )
    tb = plotter.toolbars["animation"]
    tb.setStyleSheet("QToolBar{spacing:0px;}")
    toolbar_row = qw.QToolBar(orientation=Qt.Qt.Horizontal)
    utilities_Qt.add_close_button_to_toolbar(toolbar_row, closes=tb)
    toolbar_row.setStyleSheet("QToolBar{spacing:0px;}")

    btn = qw.QToolButton()
    btn.setAutoRaise(True)
    btn.setIcon(plotter.style().standardIcon(qw.QStyle.SP_MediaPlay))
    btn.released.connect(plotter.play)
    btn.released.connect(plotter.update_frame_spinbox)
    toolbar_row.addWidget(btn)
    sb = qw.QSpinBox(minimum=1)
    sb.setValue(plotter.frame_num + 1)
    sb.setButtonSymbols(2)  # remove up/down arrows

    def frame_spinbox_callback():
        new_frame_num = sb.value()-1
        if new_frame_num != plotter.frame_num and (
            0 <= new_frame_num < len(plotter.root_meshes)
        ):
            plotter.load_frame_num(new_frame_num)

    sb.editingFinished.connect(frame_spinbox_callback)
    sb.setPrefix(' ')  # to make sure number is fully visible
    plotter.frame_spinbox = sb
    toolbar_row.addWidget(sb)

    plotter.anim_toolbar_nframes = qw.QLabel('')
    # filled in update_frame_spinbox()

    toolbar_row.addWidget(plotter.anim_toolbar_nframes)
    for icon_name, action in zip(
        [
            'SP_MediaSkipBackward', 'SP_MediaSeekBackward',
            'SP_MediaSeekForward', 'SP_MediaSkipForward'
        ],
        [
            plotter.first_frame, plotter.previous_frame,
            plotter.next_frame, plotter.last_frame
        ]
    ):
        btn = qw.QToolButton()
        btn.setAutoRaise(True)
        btn.setIcon(plotter.style().standardIcon(getattr(qw.QStyle, icon_name)))
        btn.released.connect(action)
        btn.released.connect(plotter.update_frame_spinbox)
        toolbar_row.addWidget(btn)
    toolbar_row.setIconSize(Qt.QSize(btn_width, btn_width))
    tb.addWidget(toolbar_row)
    plotter.controls_dock_window.addToolBar(Qt.Qt.LeftToolBarArea, tb)


def relink_visibility_checkbox(plotter, actor_name):
    """ Make sure visibility checkbox state matches actor visibility,
    otherwise checkbox becomes unusable """

    if actor_name not in plotter.actor_control_toolbars.keys():
        add_viscolor_toolbar(plotter, actor_name)
    toolbar = plotter.actor_control_toolbars[actor_name]
    toolbar.assign_checkbox_to_actor_visibility()
