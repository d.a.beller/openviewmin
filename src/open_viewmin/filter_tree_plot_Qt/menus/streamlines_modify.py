""" Dialog for controlling appearance of existing streamlines """


from qtpy import QtWidgets as qw
from ..widgets_Qt import utilities_Qt


def make_streamlines_settings_widget(plotter, actor_name):
    minwidth = 300
    streamlines_settings_widget = utilities_Qt.FormDialog(
        title=f'Surface streamlines settings for \"{actor_name}\"'
    )
    streamlines_settings_widget.setMinimumWidth(minwidth)
    streamlines_settings_formlayout = streamlines_settings_widget.formlayout

    filter_formula = plotter.get_filter_formula(actor_name)
    filter_kwargs = filter_formula.filter_kwargs

    sb_max_steps = qw.QSpinBox(
        minimum=1,
        maximum=1000
    )
    sb_max_steps.setValue(filter_kwargs['max_steps'])
    sb_max_steps.setSingleStep(1)
    sb_max_steps.setToolTip('Press Enter to apply.')

    def max_steps_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(max_steps=sb_max_steps.value())
        )

    sb_max_steps.editingFinished.connect(max_steps_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('max steps'),
        sb_max_steps
    )

    sb_max_step_length = qw.QDoubleSpinBox(
        minimum=0, maximum=1
    )
    sb_max_step_length.setValue(
        filter_kwargs['max_step_length']
        if 'max_step_length' in filter_kwargs.keys()
        else 1.0
    )
    sb_max_step_length.setDecimals(3)
    sb_max_step_length.setSingleStep(0.001)
    sb_max_step_length.setToolTip('Press Enter to apply.')

    def max_step_length_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(max_step_length=sb_max_step_length.value())
        )

    sb_max_step_length.editingFinished.connect(max_step_length_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('max step length'), sb_max_step_length
    )

    sb_n_points = qw.QSpinBox(
        minimum=0,
        maximum=plotter.get_mesh(filter_formula.parent_mesh_name).n_points
    )
    sb_n_points.setValue(filter_kwargs['n_points'])
    sb_n_points.setSingleStep(1)
    sb_n_points.setToolTip('Press Enter to apply.')

    def n_points_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(n_points=sb_n_points.value())
        )

    sb_n_points.editingFinished.connect(n_points_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('num points'), sb_n_points
    )

    cb_tube = qw.QCheckBox()
    cb_tube.setChecked(
        filter_kwargs['tube']
        if 'tube' in filter_kwargs.keys()
        else False
    )

    def tube_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(tube=cb_tube.isChecked())
        )

    cb_tube.stateChanged.connect(tube_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('tube'), cb_tube
    )

    sb_n_sides = qw.QSpinBox(
        minimum=3, maximum=40
    )
    sb_n_sides.setValue(filter_kwargs['n_sides'])
    sb_n_sides.setSingleStep(1)
    sb_n_sides.setToolTip('Press Enter to apply.')

    def n_sides_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(n_sides=sb_n_sides.value())
        )

    sb_n_sides.editingFinished.connect(n_sides_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('tube num sides'), sb_n_sides
    )

    sb_tube_radius = qw.QDoubleSpinBox(
        minimum=0, maximum=10
    )
    sb_tube_radius.setValue(
        filter_kwargs['radius']
        if 'radius' in filter_kwargs.keys()
        else 0.1
    )
    sb_tube_radius.setSingleStep(0.01)
    sb_tube_radius.setToolTip('Press Enter to apply.')

    def tube_radius_callback():
        plotter.update_actor(
            actor_name,
            filter_kwargs=dict(radius=sb_tube_radius.value())
        )

    sb_tube_radius.editingFinished.connect(tube_radius_callback)
    streamlines_settings_formlayout.addRow(
        qw.QLabel('tube radius'), sb_tube_radius
    )
    return streamlines_settings_widget