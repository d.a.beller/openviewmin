""" Dialog to set circular orbit for camera """

import qtpy.QtWidgets as qw
from open_viewmin.filter_tree_plot.utilities import ovm_utilities
from ..widgets_Qt import utilities_Qt


def set_orbit(plotter, do_dlg=True):
    if "orbit" in plotter.renderer.actors.keys():
        old_orbit_actor = plotter.renderer.actors["orbit"]
        vis = old_orbit_actor.GetVisibility()
        plotter.remove_actor("orbit")
    else:
        vis = False
    if not hasattr(plotter, 'orbit_kwargs'):
        plotter.orbit_kwargs = dict()
    ovm_utilities.add_if_dict_lacks(
        plotter.orbit_kwargs,
        dict(
            factor=3.0, n_points=20, viewup=[0, 0, 1], shift=0.0, step=0.1
        )
    )
    orbit_kwargs = plotter.orbit_kwargs

    def accept_callback():
        plotter.orbit_path = plotter.generate_orbital_path(
            factor=orbit_kwargs['factor'],
            n_points=orbit_kwargs['n_points'],
            viewup=orbit_kwargs['viewup'],
            shift=orbit_kwargs['shift']
        )

    if not do_dlg:
        accept_callback()
    else:
        orbit_dlg = utilities_Qt.FormDialog(title='Set orbit path')
        formlayout = orbit_dlg.formlayout
        old_orbit_kwargs = plotter.orbit_kwargs.copy()

        def set_factor():
            orbit_kwargs['factor'] = sb_factor.value()

        sb_factor = qw.QDoubleSpinBox(minimum=0)
        sb_factor.setValue(orbit_kwargs['factor'])
        sb_factor.editingFinished.connect(set_factor)
        formlayout.addRow(qw.QLabel('factor: '), sb_factor)

        def set_n_points():
            orbit_kwargs['n_points'] = sb_n_points.value()

        sb_n_points = qw.QSpinBox(minimum=1, maximum=1000)
        sb_n_points.setValue(orbit_kwargs['n_points'])
        sb_n_points.editingFinished.connect(set_n_points)
        formlayout.addRow(qw.QLabel('n_points: '), sb_n_points)
        le_viewup = qw.QLineEdit()
        le_viewup.setText(str(orbit_kwargs['viewup']))

        def set_viewup():
            txt = le_viewup.text()
            try:
                orbit_kwargs['viewup'] = list(eval(txt))
            except (TypeError, ValueError):  # reject invalid entry, revert
                le_viewup.setText(orbit_kwargs['viewup'])

        le_viewup.editingFinished.connect(set_viewup)
        formlayout.addRow(qw.QLabel('viewup: '), le_viewup)
        sb_shift = qw.QDoubleSpinBox(
            minimum=-max(plotter.dims),
            maximum=max(plotter.dims)
        )
        sb_shift.setValue(orbit_kwargs['shift'])

        def set_shift():
            orbit_kwargs['shift'] = sb_shift.value()

        sb_shift.editingFinished.connect(set_shift)
        formlayout.addRow(qw.QLabel('shift: '), sb_shift)

        sb_fps = qw.QDoubleSpinBox(minimum=0.1)
        sb_fps.setValue(1. / orbit_kwargs['step'])

        def set_fps():
            orbit_kwargs['step'] = 1. / sb_fps.value()

        sb_fps.editingFinished.connect(set_fps)
        formlayout.addRow(qw.QLabel('frames per second: '), sb_fps)

        def reject_callback():
            for key in old_orbit_kwargs.keys():
                plotter.orbit_kwargs[key] = old_orbit_kwargs[key]

        orbit_dlg.rejected.connect(reject_callback)
        orbit_dlg.accepted.connect(accept_callback)
        orbit_dlg.exec()
    if vis:
        plotter.show_orbit()
