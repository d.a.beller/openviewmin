""" Main menu bar menus and actions, extending those of PyVistaQt """

import qtpy.QtWidgets as qw
import qtpy.QtCore as Qt
import numpy as np
from . import add_actors_widgets, menu_utilities, orbit
from ..widgets_Qt import utilities_Qt
from ...filter_tree_plot.widgets import plane_widgets, line_widgets
from ...filter_tree_plot.utilities import calculations, ovm_utilities


def setup_main_menus(plotter):
    plotter.main_menu.setFixedHeight(24)
    setup_file_menu(plotter)
    setup_view_menu(plotter)
    setup_add_menu(plotter)
    setup_theme_menu(plotter)
    setup_calculate_menu(plotter)
    # setup_filter_menu(plotter)
    setup_refresh_button(plotter)
    setup_non_ovm_actors_toggle(plotter)
    setup_tools_menu(plotter)


def add_fields_filter_menu_update(
    plotter, menu, callfunc, list_before=None, field_names_source=None
):
    if list_before is None:
        list_before = []
    if field_names_source is None:
        field_names_source = plotter.scalar_fields
    menu.clear()
    for item_name, item_func in list_before:
        menu_action = menu.addAction(
            item_name,
            item_func()
        )
        menu_action.setCheckable(False)
    menu.addSeparator()
    for array_name in sorted(field_names_source()):
        menu_action = menu.addAction(
            array_name,
            callfunc(array_name)
        )
        menu_action.setCheckable(False)


def setup_tools_menu(plotter):
    plotter.the_Tools_menu.clear()
    picking_menu = plotter.the_Tools_menu.addMenu("Enable Picking...")

    for label, action in (
        ("Cell (through)", lambda: plotter.enable_cell_picking(through=True)),
        ("Cell (visible)", lambda: plotter.enable_cell_picking(through=False)),
        ("Geodesic", plotter.enable_geodesic_picking),
        ("Horizon", plotter.enable_horizon_picking),
        ("Mesh", plotter.enable_mesh_picking),
        ("Path", plotter.enable_path_picking),
        ("Point", plotter.enable_point_picking),
        ("Surface", plotter.enable_surface_picking)
    ):
        picking_menu.addAction(label, action)

    plotter.interaction_style_menu = plotter.the_Tools_menu.addMenu(
        "Mouse Interaction Mode",
    )

    stylename_style_pairs = (
        ("Image Style", plotter.enable_image_style),
        ("Joystick Style", plotter.enable_joystick_style),
        ("Rubber Band 2D Style", plotter.enable_rubber_band_2d_style),
        ("Rubber Band Style (default)", plotter.enable_rubber_band_style),
        ("Terrain Style", plotter.enable_terrain_style),
        ("Zoom Style", plotter.enable_zoom_style),
        (
            "Joystick Style Actor Manipulation",
            plotter.enable_joystick_actor_style
        ),
        (
            "Trackball Style Actor Manipulation",
            plotter.enable_trackball_actor_style
        )
    )

    for style_name, style in stylename_style_pairs:
        interaction_style_action = plotter.interaction_style_menu.addAction(
            style_name,
            style
        )
        interaction_style_action.setToolTip(style.__doc__)

    plotter.interaction_style_menu.addSeparator()

    def enable_fly_to_right_click_callback():
        plotter.enable_fly_to_right_click()
        plotter.fly_to_right_click_enabled = True

    enable_fly_to_right_click_action = (
        plotter.interaction_style_menu.addAction(
            "Enable Fly to Right Click",
            enable_fly_to_right_click_callback
        )
    )
    enable_fly_to_right_click_action.setCheckable(True)

    def set_check_enable_fly_to_right_click_action():
        enable_fly_to_right_click_action.setChecked(
            plotter.fly_to_right_click_enabled
        )

    plotter.interaction_style_menu.aboutToShow.connect(
        set_check_enable_fly_to_right_click_action
    )


def setup_add_menu(plotter):
    plotter.the_Add_menu.setToolTip(
        'Add new visualization element and/or widget'
    )
    the_add_slice_menu = plotter.the_Add_menu.addMenu("Slice")

    def add_slice_menu_update():
        add_fields_filter_menu_update(
            plotter,
            the_add_slice_menu,
            plotter.add_slice_with_controls,
            list_before=[
                ("solid_color", lambda: plotter.add_slice_with_controls(None))
            ]
        )

    the_add_slice_menu.aboutToShow.connect(add_slice_menu_update)
    the_add_isosurface_menu = plotter.the_Add_menu.addMenu("Isosurface")

    def add_isosurface_action(scalars_name):
        def return_func():
            dataset = plotter.default_mesh[scalars_name]
            new_mesh_name = scalars_name + "_isosurface"
            plotter.add_filter_formula(
                new_mesh_name,
                filter_callable="contour",
                scalars=scalars_name,
                isosurfaces=[np.average(dataset)]
            )
            toolbar = plotter.actor_control_toolbars[new_mesh_name]
            toolbar.add_isosurface_QSlider()
        return return_func

    def add_isosurface_menu_update():
        add_fields_filter_menu_update(
            plotter,
            the_add_isosurface_menu,
            add_isosurface_action
        )

    the_add_isosurface_menu.aboutToShow.connect(
        add_isosurface_menu_update
    )

    the_add_threshold_menu = plotter.the_Add_menu.addMenu("Threshold")

    def add_threshold_action(scalars_name):
        def return_func():
            new_mesh_name = scalars_name + "_threshold"
            dataset = plotter.default_mesh[scalars_name]
            plotter.add_filter_formula(
                new_mesh_name,
                filter_callable="threshold",
                scalars=scalars_name,
                value=[np.min(dataset), np.max(dataset)],
                invert=False
            )
            toolbar = plotter.actor_control_toolbars[new_mesh_name]
            toolbar.add_threshold_text_controls()
        return return_func

    def add_threshold_menu_update():
        add_fields_filter_menu_update(
            plotter,
            the_add_threshold_menu,
            add_threshold_action
        )

    the_add_threshold_menu.aboutToShow.connect(add_threshold_menu_update)
    the_add_PT_menu = plotter.the_Add_menu.addMenu("Pontryagin-Thom surface")

    def add_PT_callback(vectors_name):
        def return_func():
            actor_name = plane_widgets.add_PT_widget(plotter, vectors_name)
            toolbar = plotter.actor_control_toolbars[actor_name]
            toolbar.add_isosurface_QSlider(
                label_txt='|∠(n,N)|', num_divs=1000, init_val_percent=90
            )
            widget_name = plotter.get_filter_formula(actor_name).widget_name
            toolbar.make_slice_slider_controls(
                widget_name=widget_name,
                make_origin_slider=False
            )
        return return_func

    def add_PT_surface_update():
        add_fields_filter_menu_update(
            plotter,
            the_add_PT_menu,
            add_PT_callback,
            field_names_source=plotter.vector_fields
        )

    the_add_PT_menu.aboutToShow.connect(add_PT_surface_update)

    plotter.the_Add_menu.addAction(
        "Glyphs",
        lambda: add_actors_widgets.make_add_glyphs_widget(plotter)
    )
    plotter.the_add_surface_streamlines_menu = plotter.the_Add_menu.addAction(
        "Surface streamlines",
        lambda: add_actors_widgets.make_add_surface_streamlines_widget(plotter)
    )

    plotter.the_Add_menu.addSeparator()
    plotter.add_orientation_widget_menu = plotter.the_Add_menu.addMenu(
        "Orientation widget"
    )

    def add_orientation_widget_callback(actor_name):
        def return_func():
            widget_name = plotter.name_widget_without_overwriting(
                actor_name + "_orientation_widget"
            )
            plotter.widgets[widget_name] = (
                plotter.add_orientation_widget(
                    plotter.get_mesh(actor_name),
                    interactive=True
                )
            )
            plotter.toolbars["widgets"].populate()
            plotter.toolbars["widgets"].refresh(widget_name)
        return return_func

    def add_camera_orientation_widget_callback():
        plotter.add_camera_orientation_widget()
        widget_name = plotter.name_widget_without_overwriting("Camera")
        plotter.widgets[widget_name] = plotter._camera_widgets[-1]
        plotter.toolbars["widgets"].populate()
        plotter.toolbars["widgets"].refresh(widget_name)

    def populate_add_orientation_widget_menu():
        menu = plotter.add_orientation_widget_menu
        menu.clear()
        menu.addAction("Camera axes", add_camera_orientation_widget_callback)
        for actor_name in plotter.actor_names:
            menu.addAction(
                actor_name,
                add_orientation_widget_callback(actor_name)
            )

    plotter.add_orientation_widget_menu.aboutToShow.connect(
        populate_add_orientation_widget_menu
    )
    plotter.the_Add_menu.addAction(
        "Line widget",
        lambda: line_widgets.setup_line_widget(plotter)
    )


def setup_theme_menu(plotter):
    plotter.theme_menu.setToolTip('Global colors')


def create_filter_submenu(plotter, mesh_name):
    fltrs = ovm_utilities.available_filters(plotter.get_mesh(mesh_name))
    if len(fltrs) > 0:
        submenu = plotter.filter_menu.addMenu(mesh_name)
        for fltr in fltrs:
            submenu.addAction(
                fltr, open_filter_dialog_aux(plotter, mesh_name, fltr)
            )


def open_filter_dialog_aux(plotter, mesh_name, filter_name):
    def return_function():
        return open_filter_dialog(plotter, mesh_name, filter_name)

    return return_function


def open_filter_dialog(plotter, mesh_name, filter_name):
    mesh = plotter.get_mesh(mesh_name)
    options_dict = ovm_utilities.get_kwargs_and_defaults(
        getattr(mesh, filter_name),
        as_strings=True
    )
    filter_dialog = utilities_Qt.FormDialog(
        f'New {filter_name} filter for \"{mesh_name}\"'
    )
    formlayout = filter_dialog.formlayout
    options_dialog_dict = {}
    # list of keywords that refer only to PyVista datasets
    other_mesh_synonyms = ["other_mesh", "dataset", "surface", "target"]
    for key in options_dict.keys():
        if key == "inplace":
            continue  # 'inplace' not supported
        if key == "vectors" and filter_name != "decimate":
            val = qw.QComboBox()
            val.addItems(plotter.vector_fields(mesh=mesh))
        elif key in other_mesh_synonyms:
            val = qw.QComboBox()
            val.addItems([
                other_mesh_name
                for other_mesh_name in plotter.mesh_names
                if other_mesh_name != mesh_name
            ])
        elif key == "scalars" and filter_name != "decimate":
            val = qw.QComboBox()
            val.addItems(
                ["None"] + plotter.scalar_fields(mesh_name=mesh)
            )
        else:
            val = qw.QLineEdit(text=options_dict[key])
        options_dialog_dict[key] = val

    le_name = qw.QLineEdit(text=f"{mesh_name}_{filter_name}")
    formlayout.addRow(
        qw.QLabel('name'),
        le_name
    )
    for key in options_dialog_dict.keys():
        formlayout.addRow(
            qw.QLabel(key),
            options_dialog_dict[key]
        )

    def ok_btn_callback():
        new_mesh_name = plotter.name_mesh_without_overwriting(le_name.text())
        kwargs = {}
        for key in options_dialog_dict.keys():
            line = options_dialog_dict[key]
            if key[:2] == "**":
                kdict = eval(line.text())
                for kdkey in kdict.keys():
                    kwargs[kdkey] = kdict[kdkey]
            elif key in other_mesh_synonyms:
                kwargs[key] = plotter.get_mesh(line.currentText())
            elif key == "scalars":
                scalars_name = line.currentText()
                if scalars_name == "None":
                    kwargs[key] = None
                else:
                    kwargs[key] = scalars_name
            elif isinstance(line, qw.QComboBox):
                kwargs[key] = line.currentText()
            elif isinstance(line, qw.QLineEdit):
                try:
                    kwargs[key] = eval(line.text())
                except NameError:
                    print(
                        f"Error while evaluating input \'{line.text()}\'. "
                        f"Did you forget to put quotes around a string?"
                    )
                else:
                    if isinstance(kwargs[key], list):
                        kwargs[key] = np.array(kwargs[key])

        try:
            plotter.add_filter(
                mesh_name,
                new_mesh_name,
                filter_name,
                kwargs
            )
        except (ValueError, TypeError, KeyError) as err:
            print(
                f"Error {type(err)} while attempting to apply "
                f"filter \'{filter_name}\' to mesh \'{mesh_name}\':"
            )
            print(err)

        # if new mesh has no points, remove it
        else:
            new_mesh = plotter.get_mesh(new_mesh_name)
            if hasattr(new_mesh, "n_points"):
                if plotter.get_mesh(new_mesh_name).n_points == 0:
                    plotter.remove_filter_completely(new_mesh_name)
                    empty_mesh_warning = (
                        "Filter produced empty mesh; not adding."
                    )
                    print(empty_mesh_warning)
                    warning_dlg = qw.QMessageBox()
                    warning_dlg.setText(empty_mesh_warning)
                    warning_dlg.exec()
        filter_dialog.close()

    filter_dialog.accepted.connect(ok_btn_callback)
    filter_dialog.show()


def update_filter_menu(plotter):
    plotter.filter_menu.clear()
    for mesh_name in plotter.mesh_names:
        create_filter_submenu(plotter, mesh_name)


def setup_filter_menu(plotter):
    """ Experimental! For each mesh in the filters tree, create a submenu
    of all PyVista filters for that mesh type, which will launch a dialog
    for the filter's arguments.

    Parameters
    ----------
    plotter

    Returns
    -------

    """

    plotter.filter_menu = plotter.main_menu.addMenu('Filter')
    plotter.filter_menu.setToolTip(
        'Choose an existing mesh and a filter to apply to it'
    )
    plotter.filter_menu.aboutToShow.connect(
        lambda: update_filter_menu(plotter)
    )


def setup_calculate_menu(plotter):
    plotter.calculate_menu.setToolTip(
        'Add field arrays calculated from existing field arrays'
    )

    plotter.calculate_from_scalars_menu = plotter.calculate_menu.addMenu(
        "Scalars"
    )

    def calculate_from_scalars_menu_update():
        plotter.calculate_from_scalars_menu.clear()
        for field in plotter.scalar_fields():
            menu = plotter.calculate_from_scalars_menu
            this_scalar_component_menu = menu.addMenu(field)
            for op in ['gradient', 'Laplacian']:
                this_scalar_component_menu.addAction(
                    op,
                    calculations.calculate_from_scalar_field(plotter, field, op)
                )

    plotter.calculate_from_scalars_menu.aboutToShow.connect(
        calculate_from_scalars_menu_update
    )

    plotter.calculate_from_vectors_menu = plotter.calculate_menu.addMenu(
        "Vectors"
    )

    def calculate_from_vectors_menu_update():
        vectors_menu = plotter.calculate_from_vectors_menu
        vectors_menu.clear()
        for field in plotter.vector_fields():
            this_vector_component_menu = vectors_menu.addMenu(field)
            for var in ['x', 'y', 'z', '|x|', '|y|', '|z|', 'div', 'curl']:
                this_vector_component_menu.addAction(
                    var,
                    calculations.calculate_from_vector_field(
                        plotter, field, var
                    )
                )

    plotter.calculate_from_vectors_menu.aboutToShow.connect(
        calculate_from_vectors_menu_update
    )
    plotter.tensors_menu = plotter.calculate_menu.addMenu(
        "Symmetric rank-2 tensors"
    )

    def tensors_menu_update():
        plotter.tensors_menu.clear()
        for field in plotter.symmetric_tensor_fields():
            this_tensor_menu = plotter.tensors_menu.addMenu(field)
            this_tensor_menu.addAction(
                'eigenvalues',
                calculations.calculate_from_tensor_field(
                    plotter, field, 'eigvalsh')
            )
            this_tensor_menu.addAction(
                'eigenvectors',
                calculations.calculate_from_tensor_field(
                    plotter, field, 'eigh'
                )
            )

    plotter.tensors_menu.aboutToShow.connect(
        tensors_menu_update
    )
    plotter.calculate_menu.addSeparator()


def open_edit_theme_widget(plotter):
    plotter.edit_theme_widget = utilities_Qt.FormDialog(
        'Global theme colors'
    )
    formlayout = plotter.edit_theme_widget.formlayout
    pb_background = qw.QPushButton('Choose')

    def change_background():
        plotter.set_background(
            qw.QColorDialog.getColor(parent=plotter.app_window).name()
        )

    pb_background.clicked.connect(change_background)
    formlayout.addRow(qw.QLabel('background color: '), pb_background)

    pb_background_top = qw.QPushButton('Choose')

    def change_background_top():
        plotter.set_background(
            plotter.background_color,
            top=qw.QColorDialog.getColor(parent=plotter.app_window).name()
        )

    pb_background_top.clicked.connect(change_background_top)
    formlayout.addRow('background color top: ', pb_background_top)

    def change_theme_color(some_dict, key):
        def return_func():
            some_dict[key] = qw.QColorDialog.getColor(
                parent=plotter.app_window
            ).name()
            # change font color for axes widget by re-creating it
            axes_currently_visible = plotter.axes.GetVisibility()
            plotter.add_axes()
            # if axes were hidden, keep them hidden
            if not axes_currently_visible:
                plotter.hide_axes()
        return return_func

    pb_font_color = qw.QPushButton('Choose')
    pb_font_color.clicked.connect(
        change_theme_color(plotter.theme['font'], 'color')
    )
    formlayout.addRow(qw.QLabel('font color: '), pb_font_color)

    pb_floor_color = qw.QPushButton('Choose')

    def pb_floor_callback():
        change_theme_color(plotter.theme, 'floor_color')()
        for i in range(2):
            plotter.toggle_floor()

    pb_floor_color.clicked.connect(pb_floor_callback)
    formlayout.addRow(qw.QLabel('floor color: '), pb_floor_color)
    plotter.edit_theme_widget.show()


def setup_refresh_button(plotter):
    plotter.the_refresh_toolbar.setWindowTitle('Refresh')
    plotter.the_refresh_button = qw.QToolButton()
    plotter.the_refresh_button.setIcon(
        plotter.style().standardIcon(qw.QStyle.SP_BrowserReload)
    )
    plotter.the_refresh_button.setToolTip(
        'Update all meshes and actors in scene'
    )

    def refresh_callback():
        for mesh_name in plotter.root_meshes:
            plotter.update_filter(mesh_name)
    plotter.the_refresh_button.clicked.connect(refresh_callback)
    plotter.the_refresh_toolbar.addWidget(plotter.the_refresh_button)
    icon_size = plotter.settings['icon_size']
    plotter.the_refresh_toolbar.setIconSize((Qt.QSize(icon_size, icon_size)))
    plotter.app_window.addToolBar(
        Qt.Qt.TopToolBarArea,
        plotter.the_refresh_toolbar
    )


def repopulate_non_ovm_actors_toggle_menu(plotter):
    menu = plotter.non_ovm_actors_toggle_menu
    menu.clear()
    non_filter_tree_actor_names = plotter.non_filter_tree_actor_names

    def add_toggle_action(actor_name):
        actor = plotter.renderer.actors[actor_name]
        current_vis = actor.GetVisibility()
        if actor in plotter.scalar_bars.values():
            toggle_func = plotter.toggle_scalar_bar_visibility
        else:
            toggle_func = lambda: actor.SetVisibility(1-current_vis)
        action = menu.addAction(actor_name, toggle_func)
        action.setCheckable(True)
        action.setChecked(current_vis)
        return action

    for element_name in non_filter_tree_actor_names:
        add_toggle_action(element_name)


def setup_non_ovm_actors_toggle(plotter):
    plotter.non_ovm_actors_toggle_menu.setToolTip('Show/hide elements')
    plotter.non_ovm_actors_toggle_menu.aboutToShow.connect(
        lambda: repopulate_non_ovm_actors_toggle_menu(plotter)
    )


def setup_file_menu(plotter):
    def insert_action_into_file_menu(*action_args, insert_idx=0):
        action = plotter.the_File_menu.addAction(*action_args)
        plotter.the_File_menu.insertAction(
            plotter.the_File_menu.actions()[insert_idx],
            action
        )
        return action

    def insert_menu_into_file_menu(*menu_args, insert_idx=0):
        menu = plotter.the_File_menu.addMenu(*menu_args)
        plotter.the_File_menu.insertMenu(
            plotter.the_File_menu.actions()[insert_idx],
            menu
        )
        return menu

    insert_action_into_file_menu("Export as html", plotter.save_HTML)
    insert_action_into_file_menu("Open file(s)", plotter.open_files_dialog)

    plotter.save_mesh_menu = insert_menu_into_file_menu(
        "Export mesh",
        insert_idx=-3
    )

    def repopulate_save_mesh_menu():
        plotter.save_mesh_menu.clear()
        for mesh_name in plotter.mesh_names:
            plotter.save_mesh_menu.addAction(
                mesh_name,
                lambda: plotter.save_mesh(mesh_name)
            )

    plotter.save_mesh_menu.aboutToShow.connect(repopulate_save_mesh_menu)


def setup_view_menu(plotter):

    # remove pyvistaqt's "Clear All" action
    menu_utilities.remove_menu_action(plotter.the_View_menu, "Clear All")

    plotter.floor_toggle = plotter.the_View_menu.addAction(
        'Toggle Floor',
        plotter.toggle_floor
    )
    plotter.floor_toggle.setCheckable(True)

    def update_floor_toggle_check():
        key = 'Floor(-z)'
        if key in plotter.renderer.actors.keys():
            floor_visible = plotter.renderer.actors[key].GetVisibility()
        else:
            floor_visible = False
        plotter.floor_toggle.setChecked(floor_visible)
    plotter.the_View_menu.aboutToShow.connect(update_floor_toggle_check)

    plotter.lighting_type_menu = plotter.the_View_menu.addMenu("Lighting Type")
    plotter.lighting_type_menu.addAction(
        "Light kit", plotter.choose_lightkit
    )
    plotter.lighting_type_menu.addAction(
        "3 lights", plotter.choose_3_lights
    )
    # move to top of menu
    plotter.the_View_menu.insertMenu(
        plotter.the_View_menu.actions()[0], plotter.lighting_type_menu
    )

    plotter.the_View_menu.addSeparator()
    plotter.the_View_menu.addAction(
        'Set Orbit', lambda: orbit.set_orbit(plotter)
    )
    plotter.the_View_menu.addAction(
        'Show/Hide Orbit', plotter.toggle_orbit_visibility
    )
    plotter.the_View_menu.addAction('Orbit', plotter.do_orbit)

    plotter.the_View_menu.addSeparator()
    toggle_stereo_render_action = plotter.the_View_menu.addAction(
        'Toggle Stereo 3D',
        plotter.toggle_stereo_render
    )
    toggle_stereo_render_action.setCheckable(True)
    toggle_stereo_render_action.setChecked(False)
    toggle_shadows_action = plotter.the_View_menu.addAction(
        'Toggle Shadows', plotter.toggle_shadows
    )
    toggle_shadows_action.setCheckable(True)
    toggle_shadows_action.setChecked(False)

    if plotter.shape[0] > 1 or plotter.shape[1] > 1:
        # multiplotter mode
        plotter.the_View_menu.addSeparator()
        plotter.the_View_menu.addAction('Link views', plotter.link_views)
        plotter.the_View_menu.addAction('Unlink views', plotter.unlink_views)
        plotter.subplot_menu = plotter.the_View_menu.addMenu("Subplot")

        def subplot_callback(i, j):
            def return_func():
                plotter.subplot(i, j)

            return return_func

        for i in range(plotter.shape[0]):
            for j in range(plotter.shape[1]):
                plotter.subplot_menu.addAction(
                    str((i, j)),
                    subplot_callback(i, j)
                )
