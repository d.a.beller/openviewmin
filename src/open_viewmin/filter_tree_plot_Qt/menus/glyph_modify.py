""" Dialog for controlling appearance of existing glyphs """

import qtpy.QtWidgets as qw
import numpy as np
from open_viewmin.filter_tree_plot.utilities import ovm_utilities
from open_viewmin.filter_tree_plot.filters import sample
from ..widgets_Qt import utilities_Qt


def make_glyph_settings_widget(plotter, actor_name):
    """ Options for modifying existing glyphs, called from the customization
    dropdown menu in an actor's controls toolbar """
    minwidth = 300

    # lines after this many in the widget's "form layout" will be deleted
    # when the glyph shape is changed
    num_form_lines_to_keep = 6

    glyph_settings_widget = utilities_Qt.FormDialog(
        title=f'Glyph settings for \"{actor_name}\"'
    )
    glyph_settings_widget.setMinimumWidth(minwidth)
    glyph_settings_formlayout = glyph_settings_widget.formlayout

    filter_formula = plotter.get_filter_formula(actor_name)
    ovm_info = plotter.get_filter_formula(actor_name).ovm_info
    filter_kwargs = filter_formula.filter_kwargs
    parent_mesh_name = filter_formula.parent_mesh_name

    cb_glyph_shape = qw.QComboBox()
    items = [""] + list(plotter.geometric_objects.keys())
    cb_glyph_shape.addItems(items)
    cb_glyph_shape.setCurrentIndex(0)

    def glyph_shape_callback(choice_num):
        if choice_num > 0:
            new_glyph_shape_name = list(
                plotter.geometric_objects.keys()
            )[choice_num - 1]

            filter_formula.set_glyph_shape(new_glyph_shape_name)
            try:
                while glyph_settings_formlayout.rowCount() > num_form_lines_to_keep:
                    glyph_settings_formlayout.removeRow(
                        num_form_lines_to_keep
                    )
            except NameError:
                pass
            else:
                new_glyph_shape_func = plotter.geometric_objects[
                    new_glyph_shape_name
                ]
                default_kwargs_dict = ovm_utilities.get_kwargs_and_defaults(
                    new_glyph_shape_func
                )

                def linedit_callback(linedit, key, default_val):
                    txt = str(linedit.text())
                    try:
                        val = eval(txt)
                    except NameError:
                        val = txt  # leave entry as string
                    kwargs_dict[key] = val
                    try:
                        filter_formula.set_glyph_shape(
                            new_glyph_shape_name, **kwargs_dict
                        )
                    except TypeError:
                        linedit.setText(str(default_val))
                        pass  # ignore bad entries, reset text to default

                kwargs_dict = dict()
                for key in default_kwargs_dict.keys():
                    kwargs_dict[key] = default_kwargs_dict[key]

                def add_linedit_line(key):
                    default_val = kwargs_dict[key]
                    new_linedit = qw.QLineEdit()
                    new_linedit.setText(str(default_val))
                    new_linedit.editingFinished.connect(
                        lambda: linedit_callback(new_linedit, key, default_val)
                    )
                    glyph_settings_formlayout.addRow(
                        qw.QLabel(key),
                        new_linedit
                    )

                for key in kwargs_dict.keys():
                    add_linedit_line(key)

    sb_glyph_stride = qw.QDoubleSpinBox(
        minimum=1,
        maximum=int(
            max(1, max(np.array([plotter.Lx, plotter.Ly, plotter.Lz]) / 6))
        )
    )
    current_glyph_stride = filter_kwargs.get(
        "factor",
        plotter.settings["glyph_stride"]
    )
    sb_glyph_stride.setValue(current_glyph_stride)
    sb_glyph_stride.setSingleStep(1)
    sb_glyph_stride.setToolTip('Press Enter to apply.')
    sb_glyphs_factor = qw.QDoubleSpinBox()

    def glyph_stride_callback():
        grandparent_mesh_name = plotter.get_grandparent_mesh_name(actor_name)
        glyph_stride = sb_glyph_stride.value()
        sample.make_sampled_mesh(
            plotter,
            grandparent_mesh_name, glyph_stride
        )
        plotter.update_filter(
            actor_name,
            factor=glyph_stride,
            update_actor=True
        )
        sb_glyphs_factor.setValue(glyph_stride)

    sb_glyph_stride.editingFinished.connect(glyph_stride_callback)
    cb_glyph_random = qw.QCheckBox()
    parent_filter_info = plotter.get_filter_formula(parent_mesh_name)
    is_currently_random = parent_filter_info.ovm_info.get('random', False)
    grandparent_mesh_name = plotter.get_grandparent_mesh_name(actor_name)

    cb_glyph_random.setChecked(is_currently_random)

    def cb_glyph_random_callback():
        random = cb_glyph_random.isChecked()
        plotter.get_filter_formula(actor_name).ovm_info['random'] = random
        grandparent_mesh_name = plotter.get_grandparent_mesh_name(actor_name)

        plotter.get_filter_formula(actor_name).parent_mesh_name = (
            sample.make_sampled_mesh(
                plotter,
                grandparent_mesh_name,
                sb_glyph_stride.value(),
                random=random
            )
        )
        plotter.update_filter(actor_name, update_actor=True)

    cb_glyph_random.stateChanged.connect(cb_glyph_random_callback)

    vector_fields = plotter.vector_fields(mesh=grandparent_mesh_name)

    cb_glyphs_orient = qw.QComboBox()
    cb_glyphs_orient.addItems([''] + plotter.vector_fields(
        mesh=grandparent_mesh_name)
                              )
    def modify_glyphs_orient_callback(choice_num):
        if choice_num > 0:
            plotter.update_filter(
                actor_name,
                orient=cb_glyphs_orient.currentText(),
                update_actor=True
            )

    cb_glyphs_orient.setCurrentIndex(0)
    if 'orient' in filter_kwargs.keys():
        current_orientation_array = filter_kwargs['orient']
        if current_orientation_array in vector_fields:
            cb_glyphs_orient.setCurrentIndex(
                vector_fields.index(current_orientation_array) + 1
            )

    cb_glyphs_orient.currentIndexChanged.connect(
        modify_glyphs_orient_callback
    )

    cb_glyphs_scale = qw.QComboBox()
    cb_glyphs_scale.addItems([''] + plotter.scalar_fields())

    def modify_glyphs_scale_callback(choice_num):
        plotter.update_filter(
            actor_name,
            scale=plotter.scalar_fields()[choice_num - 1],
            update_actor=True
        )

    cb_glyphs_scale.currentIndexChanged.connect(
        modify_glyphs_scale_callback
    )

    cb_glyphs_scale.setCurrentIndex(
        plotter.scalar_fields().index(plotter.settings['scale_mesh_name']) + 1
    )
    if 'scale' in filter_kwargs.keys():
        current_scale_array = filter_kwargs['scale']
        if current_scale_array in plotter.scalar_fields():
            cb_glyphs_scale.setCurrentIndex(
                plotter.scalar_fields().index(current_scale_array) + 1
            )

    sb_glyphs_factor = qw.QDoubleSpinBox()
    sb_glyphs_factor.setMinimum(0)
    sb_glyphs_factor.setSingleStep(0.5)
    if 'factor' in filter_kwargs.keys():
        sb_glyphs_factor.setValue(filter_kwargs['factor'])
    else:
        sb_glyphs_factor.setValue(1)
    sb_glyphs_factor.editingFinished.connect(
        lambda: plotter.update_filter(
            actor_name,
            update_actor=True,
            factor=sb_glyphs_factor.value()
        )
    )
    label_callback_pairs = (
        ('shape type', cb_glyph_shape),
        ('glyphs stride', sb_glyph_stride),
        ('random locations', cb_glyph_random),
        ('orientations:', cb_glyphs_orient),
        ('scale array:', cb_glyphs_scale),
        ('global scale:', sb_glyphs_factor)
    )
    for label, callback in label_callback_pairs:
        glyph_settings_formlayout.addRow(qw.QLabel(label), callback)

    cb_glyph_shape.currentIndexChanged.connect(glyph_shape_callback)
    if 'glyph_shape' in ovm_info.keys():
        old_glyph_shape_name = ovm_info['glyph_shape']
        if old_glyph_shape_name in items[1:]:
            cb_glyph_shape.setCurrentIndex(
                items.index(old_glyph_shape_name)
            )
    return glyph_settings_widget
