""" Define actions for creating other meshes by filtering an existing mesh,
accessed from a dropdown menu in the corresponding actor's control toolbar.

"""

from qtpy import QtWidgets as qw
import pyvista as pv
from . import add_actors_widgets
from ...filter_tree_plot.widgets import box_widgets, plane_widgets
from ...filter_tree_plot.filters import topology

def populate_mesh_options_menu(plotter, actor_name, mesh_options_button=None):
    if mesh_options_button is None:
        toolbar = plotter.toolbars[actor_name]
        mesh_options_button = toolbar.mesh_options_button
    mesh_options_menu = mesh_options_button.menu()

    def add_threshold_callback():
        new_mesh_name = actor_name + "_threshold"
        plotter.add_filter_formula(
            new_mesh_name,
            parent_mesh_name=actor_name,
            filter_callable="threshold",
            value=[0, 0.99]
        )
        plotter.actor_control_toolbars[new_mesh_name].add_threshold_text_controls()

    mesh_options_name_action_pairs = (
        (
            "add glyphs",
            lambda: add_actors_widgets.make_add_glyphs_widget(
                plotter, mesh_name=actor_name
            )
        ),
        (
            "add streamlines",
            lambda: add_actors_widgets.make_add_surface_streamlines_widget(
                plotter, mesh_name=actor_name
            )
        ),
        ("add threshold (on active scalars)", add_threshold_callback),
        (
            "clip box",
            lambda: box_widgets.add_filter_widget_clip_box(
                plotter, actor_name
            )
        ),
        (
            "clip plane",
            lambda: plane_widgets.add_filter_widget_clip_plane(
                plotter, actor_name
            )
        ),
        (
            "connectivity",
            lambda: topology.add_connected_sets(plotter, actor_name, pbc=False)
        ),
        (
            "connectivity (periodic boundaries)",
            lambda: topology.add_connected_sets(plotter, actor_name, pbc=True)
        )
    )
    if isinstance(
        plotter.get_mesh(actor_name),
        pv.PolyData
    ):
        mesh_options_name_action_pairs += (
            (
                "calculate Euler characteristic",
                lambda: topology.add_Euler_characteristic_filter(
                    plotter, actor_name
                )
            ),
            (
                "find loops",
                lambda: topology.add_find_loops_filter(plotter, actor_name)
            )
        )

    def save_this_mesh_callback():
        try:
            plotter.save_mesh(actor_name)
        except ValueError as err:
            print(err)

    mesh_options_name_action_pairs += (
        (
            "save mesh",
            save_this_mesh_callback
        ),
    )

    mesh_options_menu.clear()
    for name, action in mesh_options_name_action_pairs:
        mesh_options_menu.addAction(name, action)

    mesh_options_button.setToolTip(
        "Create meshes derived from " + actor_name
    )


def add_mesh_options_menu(actor_control_toolbar):
    actor_name = actor_control_toolbar.actor_name
    plotter = actor_control_toolbar.plotter
    toolbar = actor_control_toolbar.options_toolbar
    mesh_options_button = qw.QPushButton(plotter.settings["mesh_menu_symbol"])
    mesh_options_menu = qw.QMenu()
    mesh_options_button.setMenu(mesh_options_menu)
    mesh_options_button.setFixedWidth(55)
    populate_mesh_options_menu(
        plotter, actor_name, mesh_options_button=mesh_options_button
    )
    toolbar.addWidget(mesh_options_button)
    return mesh_options_button


def save_mesh(plotter, mesh_name, mesh):
    dialog = qw.QFileDialog(
        caption=f"Save mesh {mesh_name} as...",
        parent=plotter
    )
    dialog.setAcceptMode(qw.QFileDialog.AcceptSave)
    dialog.exec()
    def accept_callback():
        filename, _ = dialog.getSaveFileName()
        mesh.save(filename)
    dialog.accepted.connect(accept_callback)
