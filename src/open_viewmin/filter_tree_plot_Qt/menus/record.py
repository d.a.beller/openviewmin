""" Create buttons and dialog for recording and exporting videos of the
scene
"""

import qtpy.QtCore as Qt
import qtpy.QtWidgets as qw
import imageio
from ..widgets_Qt import utilities_Qt


def setup_record_toolbar(plotter):
    plotter.movie_record_mode = 0
    plotter.the_Record_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
    plotter.the_Record_toolbar.setWindowTitle('Record movie')
    plotter.app_window.addToolBar(
        Qt.Qt.TopToolBarArea, plotter.the_Record_toolbar
    )
    toolbar = plotter.the_Record_toolbar

    btn_mp4 = qw.QPushButton()
    btn_mp4.setText('◉ mp4')
    btn_mp4.setToolTip('Begin mp4 recording')
    btn_mp4.setFixedWidth(70)
    btn_mp4.clicked.connect(lambda: record_movie(plotter))
    toolbar.addWidget(btn_mp4)

    btn_gif = qw.QPushButton()
    btn_gif.setText('◉ gif')
    btn_gif.setToolTip('Begin GIF recording')
    btn_gif.setFixedWidth(60)
    btn_gif.clicked.connect(lambda: record_gif(plotter))
    toolbar.addWidget(btn_gif)

    plotter.write_frame_button = qw.QPushButton()
    plotter.write_frame_button.released.connect(
        lambda: plotter.movie.append_data(plotter.screenshot())
    )
    toolbar.addWidget(plotter.write_frame_button)
    plotter.write_frame_button.setText('➲')
    plotter.write_frame_button.setToolTip(
        f'Save screenshot as next movie frame (shortcut: {plotter.settings["write_frame_key"]})')
    plotter.write_frame_button.setEnabled(False)
    plotter.stop_recording_button = qw.QPushButton()
    plotter.stop_recording_button.setToolTip(f'Stop recording (shortcut: {plotter.settings["stop_recording_key"]})')

    def stop_recording_callback():
        plotter.movie.close()
        if hasattr(plotter, "movie_timer"):
            plotter.movie_timer.stop()
        plotter.write_frame_button.setEnabled(False)
        plotter.stop_recording_button.setEnabled(False)
        try:
            plotter.clear_events_for_key(plotter.settings['write_frame_key'])
        except KeyError:
            pass
        try:
            plotter.clear_events_for_key(plotter.settings['stop_recording_key'])
        except KeyError:
            pass

    plotter.stop_recording_button.released.connect(stop_recording_callback)
    plotter.stop_recording_button.setText('◼︎')
    plotter.stop_recording_button.setEnabled(False)
    toolbar.addWidget(plotter.stop_recording_button)


def record_movie(plotter, filename='.mp4', fps=5, quality=5, movie_format='mp4'):
    plotter.movie_filename = filename
    plotter.movie_fps = fps
    plotter.movie_quality = quality
    plotter.movie_timer = Qt.QTimer()
    plotter.record_movie_widget = utilities_Qt.FormDialog(title='Record movie')
    formlayout = plotter.record_movie_widget.formlayout
    movie_filename_lineedit = qw.QLineEdit()
    pb_movie_filename = qw.QToolButton()
    pb_movie_filename.setIcon(
        plotter.style().standardIcon(
            qw.QStyle.SP_DirIcon
        )
    )
    if movie_format == 'gif':
        pyvista_filter = 'GIF files (*.gif)'
        suffix = '.gif'
    else:
        pyvista_filter = 'MPEG-4 files (*.mp4)'
        suffix = '.mp4'

    def movie_filename_dialog():
        dlg = qw.QFileDialog(
            parent=plotter.app_window,
            filter=pyvista_filter
        )
        movie_filename = dlg.getSaveFileName()[0]
        if suffix in movie_filename:
            movie_filename = ''.join(movie_filename.split(suffix)[:-1])
        movie_filename_lineedit.setText(movie_filename)

    pb_movie_filename.released.connect(movie_filename_dialog)
    sublayout = qw.QHBoxLayout()
    sublayout.addWidget(pb_movie_filename)
    sublayout.addWidget(movie_filename_lineedit)
    sublayout.addWidget(qw.QLabel(suffix))
    formlayout.addRow(
        qw.QLabel('Save movie to:'),
        sublayout
    )
    sb_fps = qw.QSpinBox(minimum=0, maximum=100, singleStep=1)
    sb_fps.setValue(plotter.movie_fps)
    formlayout.addRow(
        qw.QLabel('Frames per second:'),
        sb_fps
    )
    if suffix == '.mp4':
        sb_quality = qw.QSpinBox(minimum=1, maximum=9, singleStep=1)
        sb_quality.setValue(plotter.movie_quality)
        formlayout.addRow(qw.QLabel('Quality:'), sb_quality)

    cb_record_mode = qw.QComboBox()
    cb_record_mode.addItems([
        'Click ' + plotter.write_frame_button.text() + ' each frame',
        'Live recording',
        'Cycle through data time-frames',
        'Orbit'
    ])

    def record_mode_callback(choice_num):
        plotter.movie_record_mode = choice_num

    cb_record_mode.currentIndexChanged.connect(record_mode_callback)
    cb_record_mode.setCurrentIndex(0)
    plotter.movie_record_mode = 0
    formlayout.addRow(qw.QLabel('Recording mode:'), cb_record_mode)

    def ok_btn_callback():
        plotter.record_movie_widget.close()
        plotter.movie_filename = movie_filename_lineedit.text() + suffix
        plotter.movie_fps = sb_fps.value()
        writer_kwargs = dict(fps=plotter.movie_fps)
        if suffix == '.mp4':
            plotter.movie_quality = sb_quality.value()
            writer_kwargs['quality'] = plotter.movie_quality
        plotter.movie = imageio.get_writer(
            plotter.movie_filename, **writer_kwargs
        )
        if plotter.movie_record_mode == 0:
            plotter.write_frame_button.setEnabled(True)
            plotter.stop_recording_button.setEnabled(True)
            if not plotter.off_screen:
                plotter.add_key_event(
                    plotter.settings['write_frame_key'],
                    plotter.write_frame_button.click
                )
        elif plotter.movie_record_mode == 1:
            plotter.movie_timer.setInterval(1000 / plotter.movie_fps)
            plotter.movie_timer.timeout.connect(
                lambda: plotter.movie.append_data(plotter.screenshot())
            )
            plotter.movie_timer.start()
            plotter.stop_recording_button.setEnabled(True)
        elif plotter.movie_record_mode == 2:
            plotter.first_frame()
            plotter.movie_timer.setInterval(1000 / plotter.movie_fps)

            def timeout_callback():
                plotter.movie.append_data(plotter.screenshot())
                if plotter.frame_num < plotter.num_frames - 1:
                    plotter.next_frame()
                else:
                    plotter.movie_timer.stop()
                    plotter.movie.close()

            plotter.movie_timer.timeout.connect(timeout_callback)
            plotter.movie_timer.start()
        elif plotter.movie_record_mode == 3:
            plotter.movie.close()  # use PyVista export instead

            def do_orbit():
                plotter.orbit_on_path(
                    path=plotter.orbit_path,
                    step=1 / plotter.movie_fps,
                    write_frames=True,
                    threaded=True  # render on-screen while recording
                )

            if suffix == '.gif':
                plotter.open_gif(plotter.movie_filename)
                do_orbit()
            else:
                plotter.open_movie(plotter.movie_filename)
                dlg = utilities_Qt.FormDialog(
                    title='Warning',
                    message="mp4 orbit recording will not be viewable until this plot is closed."
                )
                dlg.accepted.connect(do_orbit)
                dlg.exec()

        if not plotter.off_screen:
            plotter.add_key_event(
                plotter.settings['stop_recording_key'],
                plotter.stop_recording_button.click
            )

    plotter.record_movie_widget.accepted.connect(ok_btn_callback)
    plotter.record_movie_widget.show()


def record_gif(plotter, movie_filename=None):
    record_movie(plotter, movie_format='gif', filename=movie_filename)
