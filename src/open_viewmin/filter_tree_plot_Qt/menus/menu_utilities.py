""" Utilities for using and editing Qt menus """

from qtpy import QtWidgets as qw


def get_menus(plotter):
    return [
        child for child in plotter.app_window.menuBar().children()
        if isinstance(child, qw.QMenu)
    ]


def get_menu(parent, title):
    for item in parent.children():
        if isinstance(item, qw.QMenu):
            if item.title() == title:
                return item
    raise KeyError(f"Cannot find menu named {title} in {parent}")


def get_menu_action(parent, label):
    for item in parent.children():
        if hasattr(item, "text"):
            if item.text() == label:
                return item
    raise KeyError(f"Cannot find action named {label} in {parent}")


def remove_menu_action(parent, label):
    menu_action = get_menu_action(parent, label)
    parent.removeAction(menu_action)
