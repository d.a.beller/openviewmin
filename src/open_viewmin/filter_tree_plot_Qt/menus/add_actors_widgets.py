""" Dialogues for creation of certain data visualization types """


import qtpy.QtWidgets as qw
from ..widgets_Qt import utilities_Qt
from open_viewmin.filter_tree_plot.filters import glyphs, streamlines


def make_add_glyphs_widget(plotter, mesh_name="fullmesh"):
    plotter.add_glyphs_widget = utilities_Qt.FormDialog(title='New glyphs')
    formlayout = plotter.add_glyphs_widget.formlayout
    cb_glyphs_orient = qw.QComboBox()
    cb_glyphs_orient.addItems([''] + plotter.vector_fields(mesh=mesh_name))
    add_glyphs_kwargs = dict(
        mesh=None,
        orient=None,
        glyph_shape=None,
        scale=None
    )

    def add_glyphs_orient_callback(_):
        add_glyphs_kwargs['orient'] = cb_glyphs_orient.currentText()

    cb_glyphs_orient.currentIndexChanged.connect(add_glyphs_orient_callback)
    cb_glyphs_orient.setCurrentIndex(0)

    cb_glyphs_mesh = qw.QComboBox()
    mesh_names = plotter.mesh_names
    cb_glyphs_mesh.addItems([''] + mesh_names)

    def add_glyphs_mesh_callback(_):
        add_glyphs_kwargs['mesh'] = cb_glyphs_mesh.currentText()
        cb_glyphs_orient.clear()
        cb_glyphs_orient.addItems(
            [''] + plotter.vector_fields(
                mesh=plotter.get_mesh(add_glyphs_kwargs['mesh'])
            )
        )

    cb_glyphs_mesh.currentIndexChanged.connect(add_glyphs_mesh_callback)
    if mesh_name in mesh_names:
        cb_glyphs_mesh.setCurrentIndex(1 + mesh_names.index(mesh_name))
    else:
        cb_glyphs_mesh.setCurrentIndex(0)
    formlayout.addRow(qw.QLabel('locations:'), cb_glyphs_mesh)
    formlayout.addRow(qw.QLabel('orientations:'), cb_glyphs_orient)

    cb_glyphs_shape = qw.QComboBox()
    shape_names = [''] + list(plotter.geometric_objects.keys())
    cb_glyphs_shape.addItems(shape_names)

    def add_glyphs_shape_callback(choice_num):
        add_glyphs_kwargs['glyph_shape'] = list(
            plotter.geometric_objects.keys()
        )[choice_num - 1]

    cb_glyphs_shape.currentIndexChanged.connect(add_glyphs_shape_callback)
    cb_glyphs_shape.setCurrentIndex(shape_names.index('arrow'))
    formlayout.addRow(qw.QLabel('shape:'), cb_glyphs_shape)

    cb_glyphs_scale = qw.QComboBox()
    cb_glyphs_scale.addItems([''] + plotter.scalar_fields())

    def add_glyphs_scale_callback(choice_num):
        add_glyphs_kwargs['scale'] = plotter.scalar_fields()[choice_num - 1]

    cb_glyphs_scale.currentIndexChanged.connect(add_glyphs_scale_callback)
    cb_glyphs_scale.setCurrentIndex(
        plotter.scalar_fields().index(plotter.settings['scale_mesh_name']) + 1
    )
    formlayout.addRow(qw.QLabel('scale:'), cb_glyphs_scale)

    def ok_btn_callback():
        glyphs.add_glyphs_to_mesh(
            plotter,
            plotter.name_mesh_without_overwriting(add_glyphs_kwargs['orient']),
            mesh_name=add_glyphs_kwargs['mesh'],
            glyph_shape=add_glyphs_kwargs['glyph_shape'],
            scale=add_glyphs_kwargs['scale'],
            orient=add_glyphs_kwargs['orient']
        )

    plotter.add_glyphs_widget.accepted.connect(ok_btn_callback)
    plotter.add_glyphs_widget.show()


def make_add_surface_streamlines_widget(plotter, mesh_name="fullmesh"):
    plotter.add_surface_streamlines_widget = utilities_Qt.FormDialog(
        title='Add surface streamlines'
    )
    formlayout = plotter.add_surface_streamlines_widget.formlayout
    cb_streamlines_vectors = qw.QComboBox()
    cb_streamlines_vectors.addItems(
        [''] + plotter.vector_fields(mesh=mesh_name)
    )
    add_streamlines_kwargs = dict(
        n_points=0,  # default to all points
        vectors_name=None,
        max_steps=10,
        mesh_name=None
    )

    def add_streamlines_vectors_callback(_):
        add_streamlines_kwargs['vectors_name'] = (
            cb_streamlines_vectors.currentText()
        )

    cb_streamlines_vectors.currentIndexChanged.connect(
        add_streamlines_vectors_callback
    )
    cb_streamlines_vectors.setCurrentIndex(0)

    cb_streamlines_mesh = qw.QComboBox()
    mesh_names = plotter.mesh_names
    cb_streamlines_mesh.addItems([''] + mesh_names)

    def add_streamlines_mesh_callback(_):
        mesh_name = cb_streamlines_mesh.currentText()
        add_streamlines_kwargs['mesh_name'] = mesh_name
        cb_streamlines_vectors.clear()
        cb_streamlines_vectors.addItems(
            [''] + plotter.vector_fields(mesh=mesh_name)
        )

    cb_streamlines_mesh.currentIndexChanged.connect(
        add_streamlines_mesh_callback
    )
    if mesh_name in mesh_names:
        cb_streamlines_mesh.setCurrentIndex(1 + mesh_names.index(mesh_name))
    else:
        cb_streamlines_mesh.setCurrentIndex(0)

    def ok_btn_callback():
        if cb_streamlines_vectors.currentIndex() > 0 and cb_streamlines_mesh.currentIndex() > 0:
            # noinspection PyUnresolvedReferences
            streamlines.add_streamlines_to_surface(
                plotter,
                plotter.name_mesh_without_overwriting(
                    add_streamlines_kwargs['vectors_name'] + "_streamlines"
                ),
                add_streamlines_kwargs['mesh_name'],
                vectors_name=add_streamlines_kwargs['vectors_name'],
                max_steps=add_streamlines_kwargs['max_steps'],
                n_points=add_streamlines_kwargs['n_points']
            )

    formlayout.addRow(qw.QLabel('surface:'), cb_streamlines_mesh)
    formlayout.addRow(qw.QLabel('vectors:'), cb_streamlines_vectors)
    plotter.add_surface_streamlines_widget.accepted.connect(ok_btn_callback)
    plotter.add_surface_streamlines_widget.exec()
