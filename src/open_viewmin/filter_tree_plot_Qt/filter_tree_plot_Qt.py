"""
Extend `pyvistaqt.BackgroundPlotter` functionality to include auto-updating
filter trees and more complete UI.
"""

from qtpy import QtWidgets as qw
from qtpy import QtCore as Qt
from pyvistaqt import BackgroundPlotter
from ..filter_tree_plot.utilities import ovm_utilities, scalar_bars
from ..filter_tree_plot.filter_tree_plot import FilterTreePlot
from ..filter_tree_plot.utilities.ovm_utilities import sort_filenames_by_timestamp
from ..filter_tree_plot.widgets import plane_widgets
from . import settings_Qt
from .widgets_Qt import controls, filters_tree_widgets, utilities_Qt, visualization_options
from .menus import main_menus, orbit, menu_utilities, record, mesh_options


class FilterTreePlotQt(FilterTreePlot, BackgroundPlotter):
    """
    Implement some PyVista functionality, with filter trees, in the PyVistaQt
    GUI
    """
    def __init__(
        self, filenames=None, user_settings=None, theme=None,
        reader=ovm_utilities.fast_import, **kwargs
    ):
        self.finished_setup = False
        FilterTreePlot.__init__(
            self,
            filenames=filenames,
            user_settings=user_settings,
            theme=theme,
            reader=reader,
            **kwargs
        )
        ovm_utilities.copy_from_dict_recursively(
            settings_Qt.settings_Qt_dict, self.settings
        )

        self.the_File_menu = self._get_menu(self.main_menu, 'File')

        if not self.off_screen:
            self._assign_keypress_events()

        self.app_window.setWindowTitle(
            "open-ViewMin: Visualization Environment for 3D Orientation Field Data"
        )

        orbit.set_orbit(self, do_dlg=False)
        self.frame_num = 0

        self.the_View_menu = self._get_menu(self.main_menu, 'View')
        self.the_Tools_menu = self._get_menu(self.main_menu, 'Tools')
        self.the_Add_menu = self.main_menu.addMenu("Add")
        self.theme_menu = self.main_menu.addAction(
            'Theme', lambda: main_menus.open_edit_theme_widget(self)
        )
        self.calculate_menu = self.main_menu.addMenu("Calculate")
        self.the_refresh_toolbar = qw.QToolBar(orientation=Qt.Qt.Horizontal)
        self.non_ovm_actors_toggle_menu = self.the_View_menu.addMenu("Toggle")
        self.the_View_menu.addAction("Hide all actors", self.hide_all_actors)
        main_menus.setup_main_menus(self)

        self.controls_dock_widget = qw.QDockWidget()
        self.controls_dock_window = qw.QMainWindow()
        self.controls_dock_scroll_area = qw.QScrollArea()
        self.customize_controls_dock()
        self.app_window.addDockWidget(
            Qt.Qt.RightDockWidgetArea,
            self.controls_dock_widget,
            Qt.Qt.Vertical
        )

        controls.setup_animation_buttons(self)
        controls.setup_lighting_control(self)
        controls.setup_widget_control(self)

        self.filters_tree_widget_items = dict()
        filters_tree_widgets.create_filters_tree(self)
        self.filters_tree.expandAll()

        self.controls_dock_scroll_area.setWidget(self.controls_dock_window)
        self.controls_dock_widget.setWidget(self.controls_dock_scroll_area)
        self.controls_dock_widget.setMinimumWidth(
            self.settings["controls_area_width"] + 25
        )

        record.setup_record_toolbar(self)

        if len(self.filenames) > 0:
            self._init_procedures_after_data_import()
        if len(self.filenames) < 2:
            self.toolbars["animation"].hide()

        self.toolbars["widgets"].populate()

    @staticmethod
    def _get_menu(parent, title):
        for item in parent.children():
            if isinstance(item, qw.QMenu):
                if item.title() == title:
                    return item
        raise KeyError(f"Cannot find menu named {title} in {parent}")

    def initialize_plotter(self, **kwargs):
        """
        Override parent class's plotter initialization to use
        pyvistaqt's Backgroundplotter instead of pyvista's Plotter
        """
        BackgroundPlotter.__init__(
            self,
            multi_samples=12,
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
            lighting='light kit',  # | 'three lights',
            allow_quit_keypress=False,
            **kwargs
        )

    def _assign_keypress_events(self):
        """
        Assign keyboard keys to actions.
        """
        self.add_key_event('c', self.toggle_last_colorbar)
        self.add_key_event('o', self.toggle_orbit_visibility)
        self.add_key_event('O', self.do_orbit)
        self.add_key_event('f', self.toggle_floor)
        self.add_key_event('E', self.show_editor)
        self.add_key_event('H', self.hide_all_actors)

    def refresh(self):
        FilterTreePlot.refresh(self)
        filters_tree_widgets.update_filters_tree(self)
        self.toolbars["widgets"].populate()

    def set_actors_visibility(self, actor_or_actors, visibility):
        FilterTreePlot.set_actors_visibility(self, actor_or_actors, visibility)
        if not isinstance(actor_or_actors, list):
            actor_or_actors = [actor_or_actors]
        for actor in actor_or_actors:
            actor_name = self.get_actor_name(actor)
            if actor_name in self.filter_formulas.keys():
                controls.relink_visibility_checkbox(self, actor_name)

    def customize_controls_dock(self):
        self.controls_dock_widget.setFeatures(
            self.controls_dock_widget.DockWidgetFloatable
            | self.controls_dock_widget.DockWidgetMovable
        )
        self.controls_dock_scroll_area.setSizeAdjustPolicy(
            qw.QScrollArea.AdjustToContents
        )
        self.controls_dock_window.setMinimumHeight(
            self.settings["controls_area_height"]
        )
        self.controls_dock_window.setSizePolicy(
            qw.QSizePolicy.Expanding,
            qw.QSizePolicy.Expanding
        )

    def show_editor(self):
        action = menu_utilities.get_menu_action(self.main_menu, 'Editor')
        action.activate(action.Trigger)

    # initialization routines

    def open_files_dialog(self, sort=True):
        """ Use GUI to select files to import """
        dlg = qw.QFileDialog()
        dlg.setFileMode(qw.QFileDialog.ExistingFiles)
        dlg.show()
        filenames = dlg.getOpenFileNames()[0]
        if sort:
            filenames = sort_filenames_by_timestamp(filenames)
        dlg.accepted.connect(
            lambda: self.load(filenames)
        )


    def _make_empty_convenience_arrays(self):
        FilterTreePlot._make_empty_convenience_arrays(self)
        self.dock_widgets = {}
        self.toolbars = {}

    @property
    def actor_control_toolbars(self):
        ret = dict()
        for toolbar_name in self.toolbars.keys():
            toolbar = self.toolbars[toolbar_name]
            if isinstance(toolbar, controls.ActorControlToolbar):
                ret[toolbar_name] = toolbar
        return ret

    def _init_procedures_after_data_import(self):
        """ Initialization steps that can be carried out only after some data
        is imported """

        if not self.finished_setup:

            # bounding box
            self.add_mesh(
                self.fullmesh.outline(),
                color=self.settings['bounding_box_color'],
                name="outline"
            )

            self.finished_setup = True
            self.renderer.actors["outline"].SetVisibility(0)

    def add_filter_formula(self, name=None, **kwargs):
        FilterTreePlot.add_filter_formula(self, name=name, **kwargs)
        if self.filter_formulas[name].has_actor:
            if name not in self.actor_control_toolbars.keys():
                controls.add_viscolor_toolbar(self, name)
        filters_tree_widgets.update_filters_tree(self)

    def remove_mesh_completely(self, mesh_name, remove_children=True):
        FilterTreePlot.remove_mesh_completely(
            self, mesh_name, remove_children=remove_children
        )
        filters_tree_widgets.update_filters_tree(self)

    def rename_mesh(self, from_name, to_name):
        if from_name in self.actor_control_toolbars.keys():
            self.toolbars[to_name] = self.toolbars[from_name]
            del self.toolbars[from_name]
        FilterTreePlot.rename_mesh(self, from_name, to_name, do_refresh=False)
        if to_name in self.actor_control_toolbars.keys():
            toolbar = self.toolbars[to_name]
            toolbar.assign_checkbox_to_actor_visibility(actor_name=to_name)
            toolbar.set_label(to_name)
            visualization_options.populate_viz_options_menu(self, to_name)
            mesh_options.populate_mesh_options_menu(self, to_name)
        self.refresh()

    # frame handling / animation

    def load_frame(self, frame_num=0):
        """ Use one of the imported data files as the source for all datasets """

        FilterTreePlot.load_frame(self, frame_num=frame_num)
        self.frame_spinbox.setValue(self.frame_num + 1)


    def update_frame_spinbox(self):
        """ The seemingly circuitous route here to updating the frame-spinbox
        with the current frame number is a hack to make sure the displayed
        number updates cleanly.
        """
        spinbox = self.frame_spinbox
        spinbox.setFocus()  # necessary to update displayed number
        spinbox.setPrefix('  ')  # hack to clear previous displayed number
        spinbox.clearFocus()  # release focus from spinbox so the above takes effect
        spinbox.setFocus()  # return focus to spinbox so that...
        spinbox.setPrefix(' ')  # ... we remove the extra spaces
        spinbox.clearFocus()  # release focus from spinbox so the above takes effect
        self.anim_toolbar_nframes.setText('/' + str(len(self.root_meshes)))

    # lighting

    def choose_lightkit(self):
        self.enable_lightkit()
        controls.get_lights_intensity_from_slider(self)

    def choose_3_lights(self):
        self.enable_3_lights()
        controls.get_lights_intensity_from_slider(self)

    # visibility control

    def update_filter(self, mesh_name, update_actor=False, **kwargs):
        FilterTreePlot.update_filter(
            self, mesh_name, update_actor=update_actor, **kwargs
        )
        if update_actor and mesh_name in self.renderer.actors.keys():
            controls.relink_visibility_checkbox(self, mesh_name)
        return self.get_mesh(mesh_name)

    def update_actor(
        self,
        actor_name,
        **kwargs
    ):
        FilterTreePlot.update_actor(
            self,
            actor_name,
            **kwargs
        )
        if actor_name in self.renderer.actors.keys():
            controls.relink_visibility_checkbox(self, actor_name)
        return self.get_actor(actor_name)

    def save_HTML(self, filename=None):
        if filename is None:
            self.save_HTML_widget = utilities_Qt.FormDialog(title='Save HTML')
            formlayout = self.save_HTML_widget.formlayout
            HTML_filename_lineedit = qw.QLineEdit()
            pushbutton_HTML_filename = qw.QToolButton()
            pushbutton_HTML_filename.setIcon(
                self.style().standardIcon(
                    qw.QStyle.SP_DirIcon
                )
            )

            def HTML_filename_dialog():
                dlg = qw.QFileDialog(
                    parent=self.app_window,
                    filter='HTML files (*.html)'
                )
                HTML_filename = dlg.getSaveFileName()[0]
                if '.html' in HTML_filename:
                    HTML_filename = HTML_filename.split('.html')[0]
                HTML_filename_lineedit.setText(HTML_filename)

            pushbutton_HTML_filename.released.connect(HTML_filename_dialog)
            sublayout = qw.QHBoxLayout()
            sublayout.addWidget(pushbutton_HTML_filename)
            sublayout.addWidget(HTML_filename_lineedit)
            sublayout.addWidget(qw.QLabel('.html'))
            formlayout.addRow(
                qw.QLabel('Save visualization to:'),
                sublayout
            )

            def ok_btn_callback():
                self.save_HTML_widget.close()
                self.HTML_filename = HTML_filename_lineedit.text() + '.html'
                self.export_html(self.HTML_filename)

            self.save_HTML_widget.accepted.connect(ok_btn_callback)
            self.save_HTML_widget.show()
        else:
            self.HTML_filename = filename
            self.export_html(self.HTML_filename)

    def save_mesh(self, mesh_name, mesh_filename=None):
        mesh = self.get_mesh(mesh_name)
        def save_callback(use_this_filename):
            try:
                mesh.save(use_this_filename)
            except ValueError as err:
                print(err)
            else:
                print(f"Mesh \'{mesh_name}\' saved to {use_this_filename}.")

        if mesh_filename is None:
            save_mesh_widget = utilities_Qt.FormDialog(
                title=f"Save mesh \"{mesh_name}\""
            )
            formlayout = save_mesh_widget.formlayout
            mesh_filename_lineedit = qw.QLineEdit()

            pushbutton_mesh_filename = qw.QToolButton()
            pushbutton_mesh_filename.setIcon(
                self.style().standardIcon(
                    qw.QStyle.SP_DirIcon
                )
            )

            def mesh_filename_dialog():
                dlg = qw.QFileDialog(
                    parent=self.app_window,
                    filter='mesh files (*.ply *.stl *.vtk)'
                )
                mesh_filename = dlg.getSaveFileName()[0]
                mesh_filename_lineedit.setText(mesh_filename)

            pushbutton_mesh_filename.released.connect(mesh_filename_dialog)

            sublayout = qw.QHBoxLayout()
            sublayout.addWidget(pushbutton_mesh_filename)
            sublayout.addWidget(mesh_filename_lineedit)
            formlayout.addRow(
                qw.QLabel('Save as:'),
                sublayout
            )

            def ok_btn_callback():
                save_mesh_widget.close()
                mesh_filename = mesh_filename_lineedit.text()
                save_callback(mesh_filename)
            save_mesh_widget.accepted.connect(ok_btn_callback)
            save_mesh_widget.show()
        else:
            save_callback(mesh_filename)

    # TODO: find a way to rescue update_method from getting lost when its source toolbar is destroyed

    def remove_actor_completely(self, actor_name):
        FilterTreePlot.remove_mesh_completely(self, actor_name)
        if actor_name in self.toolbars.keys():
            self.app_window.removeToolBar(self.toolbars[actor_name])
            del self.toolbars[actor_name]
        filters_tree_widgets.update_filters_tree(self)


    def add_slice_with_controls(self, scalars_name):
        def return_func():
            slice_name, widget_name = plane_widgets.add_slice(
                self, scalars_name=scalars_name
            )
            if scalars_name is None:
                self.update_actor(slice_name, color="auto")
            toolbar = self.actor_control_toolbars[slice_name]
            toolbar.make_slice_slider_controls(widget_name=widget_name)

            filter_formula = self.get_filter_formula(slice_name)
            colorbar = filter_formula.colorbar
            if colorbar is not None:
                scalar_bars.standardize_scalar_bar(self, colorbar)
                colorbar.SetTitle(scalars_name)
        return return_func

    def add_widget_formula(self, name=None, **kwargs):
        FilterTreePlot.add_widget_formula(self, name=name, **kwargs)
        self.toolbars['widgets'].populate()
        self.toolbars['widgets'].refresh()
